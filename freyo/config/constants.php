<?php

return [

    /*
	|--------------------------------------------------------------------------
	| Cache Settings
	|--------------------------------------------------------------------------
	|
	| Settings and configuration of the cache function
	|
	*/

    'sett_cache' => true,
    'sett_app_cache' => true,
    'sett_cache_min' => 3, //per minute expire

    /*
	|--------------------------------------------------------------------------
	| Cookie Settings
	|--------------------------------------------------------------------------
	|
	| Settings and configuration of the cookie function
	|
	*/

    'user_cookie_name' => 'freyo_user',


    /*
	|--------------------------------------------------------------------------
	| Location Path
	|--------------------------------------------------------------------------
	|
	| Location of the file where the panel stores the files
	|
	*/

    'url_file' => 'panel/_files',
    'url_file_banner' => 'panel/_files/advertisement',
    'url_file_modbuild' => 'panel/_files/modbuild',
    'url_file_magazine' => 'panel/_files/magazines',
    'url_file_magazine_cover' => 'panel/_files/magazines/cover',
    'url_file_magazine_featured' => 'panel/_files/magazines/featured',
    'url_file_profile_pic' => 'panel/_files/profilepic',
];