<div data-0p-top="background-position:50% 95%"  data-50p-top="background-position:50% 0%" id="follow-topics" class="container-fluid follow-topics" data-scroll-index="2">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 hidden-xs col-md-6 three-topics-container center-block">
<!-- 				<img class="three-topics-1 img-responsive center-block" src="{{asset('assets/image/b_1.png')}}">
				<img style="display:none;" class="three-topics-2 img-responsive center-block" src="{{asset('assets/image/b_2.png')}}">
				<img style="display:none;" class="three-topics-3 img-responsive center-block" src="{{asset('assets/image/b_3.png')}}"> -->
				<img style="" class="center-block" src="{{asset('assets/image/3-screen-screenshot(4-19-16).png')}}">
			</div>
			<div class="col-xs-12 col-md-6">
				<h4>Follow topics that interest you and share these stories. You can customize your reading experience that you will surely love.</h4>
				<div class="row follow-icons">
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/fashion.png')}}"></div>
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/travel.png')}}"></div>
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/celebrities.png')}}"></div>
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/weddings.png')}}"></div>
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/sports.png')}}"></div>
					<div class="col-xs-6 col-sm-4"><img class="center-block" src="{{asset('assets/image/cars.png')}}"></div>
				</div>
			</div>
		</div>
	</div>
</div>