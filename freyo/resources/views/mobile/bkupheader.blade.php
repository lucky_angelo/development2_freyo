<div class="container-fluid f-sidenav" style="display:none;">
	@include('mobile.blocks.sidebar')
</div>

{{-- <div class="freyo-header">
	<div class="container-fluid text-center">
		<span class="left-nav">
			<i class="fa fa-bars"></i>
		</span>		
		<span class="header-center-text">WELCOME</span>
		<span class="right-nav">
			<i class="fa fa-search"></i>
		</span>
	</div>
</div> --}}

<div class="freyo-header" ng-controller="HeaderController">
	<div class="container-fluid text-center">
		<span class="left-nav">
			<i class="fa fa-bars"></i>
		</span>		
		<span class="header-center-text" ng-bind="title"></span>
		<span class="right-nav">
			<i class="fa fa-search"></i>
		</span>
	</div>
</div>