<div class="container-fluid f-su-formpage">
    <div class="fma-vertical-responsive">
        <div class="row fsu-almost-there">
            <h3>Register at Freyo</h3>
            <p>Tell us some details and we’ll get you on your own way</p>
        </div>
        <div class="row f-signup-form">
            <form action="<? echo asset('/api/fblogin') ?>" method="post" class="">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <div class="row fsu-input">
                    <span>Email Address</span>
                    <input id="email_add" class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" name="email" required data-errormessage-value-missing="Email is required!" 
                    data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                    <input type="hidden" name="fb_id" value="{{ \Session::get('fb_id') }}">
                    <input type="hidden" name="first_name" value="{{ $f_name }}">
                    <input type="hidden" name="last_name" value="{{ $l_name }}">
                    <input type="hidden" name="gender" value="{{ $gender }}">
                    <input type="hidden" name="birthdate" value="{{ $birthdate }}">
                    @if(\Session::get('is_silhouette'))
                    <input type="hidden" name="is_silhouette" value="is_silhouette">
                    @endif
                    <input type="hidden" name="__id" value="from_email" checked>
                </div>
                <div class="row fsu-input f-select-address">
                    <div class="col-xs-12 no-gutters">
                        Address
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="province" class="ph-provinces">
                            <option>Province</option>
                        </select>
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="city" class="ph-cities">
                            <option>City/Municipality</option>
                        </select>
                    </div>
                </div>
                <div class="row fsu-input">
                    <span>Mobile Number</span>
                    <input id="mobile_number" class="validate[custom[integer]] fsu-input-box col-xs-12" type="text" name="mobile_number" data-errormessage-custom-error="Number only!" data-prompt-position="topLeft"/>
                </div>
               
                <div class="">
                    <div class="registration_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                </div>
                <input type="submit" class="btn btn_submit_sign_up text-center" value="Continue">
                
            </form>
        </div>
        <!--
        <div class="row text-center below-the-submitbutton">
            <p>Already have an account? <a class="btn_go_sign_in a-su-sign-in" href="#">Sign in</a></p>
        </div>
        -->
    </div>
</div>