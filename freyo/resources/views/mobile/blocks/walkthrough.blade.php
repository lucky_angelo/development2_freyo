<div class="container-fluid f-walkthrough" style="<? echo $displayWalkthrough ?>">

	<div class="">

		<div class="walkthrough-img-container">
			<div class="verticalalign"><img class="img-responsive center-block" src="{{asset('assets/mobile/images/walkthrough/1.png')}}"></div>
			<div class="verticalalign"><img class="img-responsive center-block" src="{{asset('assets/mobile/images/walkthrough/2.png')}}"></div>
			<div class="verticalalign"><img class="img-responsive center-block" src="{{asset('assets/mobile/images/walkthrough/3.png')}}"></div>
			<div class="verticalalign"><img class="img-responsive center-block" src="{{asset('assets/mobile/images/walkthrough/4.png')}}"></div>
			<div class="verticalalign"><img class="img-responsive center-block the-last-walkthrough" src="{{asset('assets/mobile/images/walkthrough/5.png')}}"></div>
		</div>

		<div class="f-footer-walkthrough text-center">

			<div class="walkthrough-nav"></div>

			<div class="walkthrough-next pull-right" style="display:none;">

				<span>NEXT <i class="fa fa-angle-right"></i></span>

			</div>

		</div>

	</div>

</div> 