<div class="container-fluid f-fp-formpage" style="display: none">
    <div class="fma-vertical-responsive">
        <a class="btn_back_to_interests_from_fp" href="#"><i class="fa fa-arrow-left"></i></a>
        <!-- <a class="btn_si_to_welcome" href="#"><i class="fa fa-arrow-left"></i></a> -->
        <div class="row text-center">
            <!-- <h3>Sign in to your Freyo account</h3> -->
            <h3 class="reset-header">RESET PASSWORD</h3> 
        </div>
        <div class="row f-signup-form">
            <form class="reset-password-form"> 
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row fsu-input fsu-active">
                        <span>Email Address</span>
                        <input class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" required name="request_email" data-errormessage-value-missing="Email is required!" data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                    </div>
                    <div class="requestreset_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                    <input type="submit" data-resetted="no" class="btn btn_submit_resetpassword text-center" value="Reset Password">
                </form>
        </div>
    </div>
</div>
<!-- <div id="resetModal" class="modal fade resetpassword-modal" role="dialog" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
                <form class="reset-password-form">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row fsu-input fsu-active">
                        <span>Email Address</span>
                        <input class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" required name="request_email" data-errormessage-value-missing="Email is required!" data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                    </div>
                    <div class="requestreset_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                    <input type="submit" data-resetted="no" class="btn btn_submit_resetpassword text-center" value="Reset Password">
                </form>
            </div>
        </div>
    </div>
</div> -->

