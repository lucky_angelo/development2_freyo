<div ui-content-for='navbarRight'>
    <div class="page-options">
        <i class='fa fa-bookmark dFavoriteAction favorite-action dConfirmSubscribe'></i>
        <i class='fa fa-share-alt share-action dShareAction' where-share='mbrowser'></i>
    </div>
</div>

<div ng-if="!logged_in" ui-content-for="navbarLeft">
</div>

<div ui-content-for="title">
    <a ng-if="!logged_in" href="<?php echo asset('dashboard');?>" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>{{ browserTitle }}
</div>
<!-- <i class="pull-right fa fa-angle-right"></i> -->
<!-- <i class="pull-left fa fa-angle-left"></i> -->
<div class="scrollable">
    <div class="scrollable-content">
        <div class="owlCarousel magazines-browser-slider" data-options='{autoHeight:true,items:1,stagePadding: 25,startPosition:{{activeItem}} }'
        ng-init="magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth) | number:0); magViewUrl = '<?php echo asset('magazine/view'); ?>';">
            <div class="item block-magazine owlCarouselItem" ng-repeat="magazine in browserMagazines">
                <div class="magazine-cover-container" data-magazine-id="{{magazine.magazine_id}}" fmag-title="{{magazine.title}}">
                    <a class="is-loading" href="{{magViewUrl+'/'+magazine.magazine_id}}">
                        <img ng-src='{{magCover+"/"+magazine.magazine_cover}}' class="magazine-cover-photo" alt="">
                    </a>
                </div>
                <div class="magazine-information-container">
                    <div class="magazine-header">
                        <h4 class="magazine-title">{{ magazine.title }}</h4>
                        <!-- <p class="magazine-published-date">
                            {{magazine.issue_date | date:'MMMM y'}}
                        </p> -->
                    </div>
                    <div class="magazine-options-container">
                        <div class="views">
                            <i class='fa fa-eye'></i> <span> {{magazine.views}}</span>
                        </div>
                        <div class="favorites">
                            <i class='fa fa-bookmark-o'></i> <span> {{magazine.favorites}}</span>
                        </div>
                        <div class="rate">
                            <input type="hidden" class="rating"  value="{{magazine.rating}}" readonly="true" /> <span class="review-count">({{magazine.reviews.length+(magazine.reviews.length>1?' Reviews':' Review')}})</span>
                        </div>
                    </div>
                    <div class="magazine-details-container" ng-if="magazine.details != ''">
                        <div class="row">
                            <div class="col-xs-2">
                                <h3 class='label'>Details</h3>
                            </div>
                            <div class="col-xs-10">
                                <!-- <p> Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p> -->
                                <p>{{ magazine.details }}</p>
                            </div>
                        </div>
                    </div>
                    <div ng-if="logged_in && (objectIndexOf(magazine.reviews, user_profile.user_id, 'user_id') == -1) " class="magazine-review-rating-container">
                        <h4 class="header3">
							Write a Review & Ratings
						</h4>
                        <form class="rate-review-form">
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <input type="hidden" name="magazine_id" ng-value="magazine.magazine_id">
                            <div class="review-input-container">
                                <textarea row='3' name="review" class="validate[required]" data-errormessage-value-missing="Please review the magazine!" data-prompt-position="topLeft"></textarea>
                            </div>
                            <div class="review-rate-container">
                                <input type="hidden" class="rating rating-magazine" name="rating" data-step="1"/> <span>Tap to Rate</span>
                                <div class="error-rating-message" style="display:none;">Please rate from 1-5!</div>
                            </div>
                            <button class="submit-review dSubmitRating">
                                Submit
                            </button>
                        </form>
                    </div>
                    <div class="magazine-reviews-container" data-revmagazine="{{ magazine.magazine_id }}" >
                        <div class="review-item" ng-if="magazine.reviews.length > 0" ng-repeat="rateview in magazine.reviews">
                            <div class="review-header" ng-init="revPicUrl = '<?php echo asset('/api/profile_pic');?>';">
                                <img class="reviewer-photo" ng-src="{{rateview.profile_picture!=null?(revPicUrl+'/'+rateview.profile_picture):(revPicUrl+'/default-picture.jpg')}}">
                                <h3 class="reviewer-name">{{rateview.first_name+' '+rateview.last_name}}</h3>
                                <div class="reviewer-rating-container">
                                    <input type="hidden" class="rating" value="{{ rateview.rating }}" readonly="true" />
                                </div>
                            </div>
                            <div class="review-message-container">
                                <p>{{ rateview.review }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
