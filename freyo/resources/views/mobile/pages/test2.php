<div ui-content-for="link-css">
  <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/appP.css') ?>">
</div>
<div ui-content-for="title">
  Browse
</div>
<div ui-content-for="navbarLeftSide">
  <i class="fa fa-search"></i>
</div>
<div ui-content-for="navbarAction">
	<div class="btn">
  		<i class="fa fa-heart"></i>
	</div>
</div>

<div>
	<h1 class="text-center" style="padding:50px;">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</h1>
</div>