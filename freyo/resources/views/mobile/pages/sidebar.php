<div class="scrollable container-fluid f-sidenav">
    <div class="col-xs-12 f-close-menu" ui-turn-off='uiSidebarLeft'>
        <a class="btn_close_sidenav" href="#"><i class="fa fa-times"></i>&nbsp;<span>Close Menu</span></a>
    </div>
    <div class="col-xs-12 f-profile-details text-center">
        <a href="<?php echo asset('profile/edit');?>">
			<!-- <img class="f-profile-pic img-circle" src="<?php echo asset('assets/mobile/images/gintoki_shock.jpg') ?>"> -->
			<img class="f-profile-pic img-circle" ng-src="{{url_profile_pic}}">
			<p class="f-profile-name">{{user_profile.first_name+' '+user_profile.last_name}}</p>
			<p class="f-profile-email">{{user_profile.email}}</p>
		</a>
    </div>
    <div class="f-nav-items" ui-turn-off='uiSidebarLeft'>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('dashboard');?>">
				<img src="<?php echo asset('assets/mobile/images/dashboard-nav.png');?>"> <span>Dashboard</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('profile/subscriptions');?>">
				<img src="<?php echo asset('assets/mobile/images/following-nav.png');?>" style="top:15px;"> <span>Subscription</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('profile/likes');?>">
				<img src="<?php echo asset('assets/mobile/images/myclippings-nav.png');?>"> <span>Clippings</span>
			</a>
        </div>
        <!-- <div class="col-xs-12 f-nav-page">
			<a href="<?php //echo asset('search');?>">
				<img src="<?php //echo asset('assets/mobile/images/browse-nav.png');?>"> <span>Browse</span>
			</a>
		</div> -->
        <div class="col-xs-12 f-nav-page my-profile-nav">
            <a href="<?php echo asset('profile/likes');?>">
				<img src="<?php echo asset('assets/mobile/images/myprofile-nav.png');?>"> <span>My Profile</span>
			</a>
            <a class="edit-profile-nav" href="<?php echo asset('profile/edit');?>"><i class="fa fa-pencil-square-o"></i></a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('support');?>">
				<img src="<?php echo asset('assets/mobile/images/support-nav.png');?>"> <span>Support</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a ui-turn-on="modal1" href="#">
				<img src="<?php echo asset('assets/mobile/images/signout-nav.png');?>"> <span>Sign Out</span>
			</a>
        </div>
    </div>
</div>
