<div ui-content-for="modals">
  <div class="modal signout-dialog-box text-center" ui-if="modal1" ui-state='modal1'>
    <div class="modal-backdrop in"></div>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Sign out on Freyo?</h4>
        </div>
        <div class="modal-body">
          <p>You can always access your content by signing back in.</p>
        </div>
        <div class="modal-footer">
          <a ui-turn-off="modal1" href="#" class="">Cancel</a>
          <a ui-turn-off="modal1" href="<? echo asset('signout'); ?>" class="sign-out-now">Sign Out</a>
        </div>
      </div>
    </div>
  </div>
</div>