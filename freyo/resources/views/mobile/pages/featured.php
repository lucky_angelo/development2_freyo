<!-- ng-repeat featured image -->
<div ng-if="sliderTotal > 1"class="owlCarousel" data-options="{lazyPrefetch:1,lazyLoad:true, loop:true, nav : false, items : 1, autoplay : true}">
    <div class="item owlCarouselItem" ng-repeat="slider in featuredSlider">
        <img data-src="{{sliderUrl+'/'+slider.featured_image}}" class="img-responsive owl-lazy" alt="">
    </div>
</div>
<div ng-if="sliderTotal == 1" class="owlCarousel" data-options="{nav : false, items : 1}">
    <div class="item owlCarouselItem" ng-repeat="slider in featuredSlider">
        <img ng-src="{{sliderUrl+'/'+slider.featured_image}}" class="img-responsive" alt="">
    </div>
</div>

<!-- <div ng-repeat="featured in dashboardFeatured">
    <b>{{ featured.category_id }} - {{featured.category_name}}</b>
    <div ng-repeat="mags in featured.magazines">
        <i>{{ mags }} <br/>WWWWWWWW</i>
    </div>
</div> -->
<div class="col-xs-12 col-p-10 featured-magazine-container feat-category-{{featured.category_id}}" ng-repeat="featured in dashboardFeatured">
    <div class="category-items-container">
        <div class="category-item-header">
            <h4 class="category-title">{{ featured.category_name }}</h4> <a class="category-all" href="{{magBrowser+'/'+featured.category_id}}">
            <span ng-if="featured.category_id == -1" class="freyo-prime-all">SEE ALL </span>
            <span ng-if="featured.category_id != -1" class="category-others">SEE ALL </span>
            <i class="fa fa-chevron-circle-right"></i></a>
        </div>
        <div class="category-item-body owlCarousel"  ng-if="featured.category_name == 'Recommended'" data-options="{items:3, margin: 10}">
            <div class="magazine-item featured-issue-dashboard owlCarouselItem"  ng-repeat="magazine in featured.magazines">
                <a href="{{magBrowser+'/'+featured.category_id+'/'+magazine.magazine_id}}">
                    <div class="is-loading">
                        <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                    </div>
                    <p class="magazine-title">{{magazine.title}}</p>
                </a>
            </div>
        </div>
        <div class="category-item-body"  ng-if="featured.category_name != 'Recommended'">
            <div class="magazine-items">
                <div class="magazine-item featured-issue-dashboard" ng-repeat="magazine in featured.magazines">
                    <a href="{{magBrowser+'/'+featured.category_id+'/'+magazine.magazine_id}}">
                        <div class="is-loading">
                            <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                        </div>
                        <p class="magazine-title">{{magazine.title}}</p>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>