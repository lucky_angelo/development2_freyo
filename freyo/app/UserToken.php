<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'freyo_user_token';
	protected $fillable = [
		'token',
		'unique_id',
		'os',
		'date_created',
		'date_updated'
	];

	public $timestamps = false;
}
