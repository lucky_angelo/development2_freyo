<?php 

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class SystemVariable extends Model {

    protected $table = 'sys_variable';
    public $timestamps = false;

}
