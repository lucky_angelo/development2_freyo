<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazinePublish extends Model {

	protected $table = 'magazine_publisher';
	protected $fillable = [
		'views',
		'favorites'
	];
    public $timestamps = false;
}
