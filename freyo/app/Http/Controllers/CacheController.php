<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;

use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;

class CacheController extends Controller
{
    	protected $key;
    	protected $data;
        protected $min;

    	public function __construct($key)
    	{
    		$this->key = md5($key);
    	}

    	public function get($data, $min = false)
    	{
    		$this->data = $data;

            if($min === false)
                $this->min = \Config::get('constants.sett_cache_min');
            else
                $this->min = $min;

            if(\Config::get('constants.sett_cache') === true) {
                return \Cache::get($this->key, function () {
                    $cached = $this->data->get();
                    \Cache::put($this->key, $cached, $this->min);
                    return $cached;
                });
            } else {
                return $this->data->get();
            }
    	}
}
