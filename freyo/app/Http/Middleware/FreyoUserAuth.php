<?php

namespace Freyo\Http\Middleware;

use Closure;

class FreyoUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->cookie('freyo_user') == null){
            return view('mobile.launchscreen');
        }
        return $next($request);
    }
}
