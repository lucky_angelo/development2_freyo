<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineAds extends Model {

	protected $table = 'ads_item';
	protected $fillable = [
		'views',
		'status',
	];
	public $timestamps = false;

}
