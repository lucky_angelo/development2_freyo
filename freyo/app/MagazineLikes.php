<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineLikes extends Model {

	protected $table = 'magazine_likes';
	protected $fillable = [
		'page_id',
		'user_id',
	];
	public $timestamps = false;

}
