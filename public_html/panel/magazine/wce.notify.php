<style type="text/css">
	textarea {
		width: 200px;
	}
</style>
<table border=0 width=100%><tr><td><b>Magazine Publisher</b></td><td><? include("wce.menu.php") ?></td></tr></table>

<hr size=1 color=#606060><? echo $status_message ?><br>

<!-- <table align="center" border=0 cellspacing="0" cellpadding="3">
	<tr>
		<td> -->
			<table border=0 cellpadding=3 cellspacing=0 width=98% align="center">
				<form name="notiform" id="noti-form">
					<tbody>
						<tr>
							<td width="20%">Notification Title:<small>(For android users)</small> </td>
							<td><input id="n-title" type="text" name="ntitle" required/></td>
						</tr>
						<tr>
							<td width="20%">Notification Message: </td>
							<td><textarea id="n-message" name="nmessage" cols="50" required></textarea><small id="count"></small></td>
						</tr>
						<tr>
							<td width="20%">Notify to : </td>
							<!-- <td><input id="all" type="radio" checked name="notify-to"/> <label for="all">All</label> <input id="magazine" type="radio" name="notify-to"/> <label for="magazine">Magazine Subscribers</label></td> -->
							<td>
								<select id="n-magazine" name="magazine" required  >
									<option value="0">All</option>
									<?php
										$oMagz->getMagazineList();
									?>
								</select>
							</td>
						</tr>
						<!-- <tr id="mag-list" style="display: none">
							<td width="20%">Select magazine: </td>
							<td>
								<select id="n-magazine" name="magazine" required disabled="disabled" >
									<?php
										$oMagz->getMagazineList();
									?>
								</select>
							</td>
						</tr> -->
						<tr>
							<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><input id="send-noti" type="submit" value="Notify" /></td>
						</tr>
					</tbody>
				</form>
			</table>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		<!-- </td>
	</tr>
</table> -->

<script type="text/javascript">
	$(document).ready(function() { 
		$('#n-magazine').chosen({
		    disable_search_threshold: 10, width: "30%"
		}); 
		$('#noti-form').submit(function(e) {
			e.preventDefault();
			// if(!$('#n-magazine').is(':disabled')) {
			// 	var magazine = $('#n-magazine').val();
			// } else {
			// 	var magazine = 'all';
			// }
			var magazine = $('#n-magazine').val();
			$.ajax({
				type: 'POST',
				url: 'http://m.freyo.com/api/pushnotify',
				data: {
					'security': 'ed70ce2e8edfc',
					'title': $('#n-title').val(),
					'message': $('#n-message').val(),
					'magazine': magazine,
				},
				cache: false,
			}).done(function(response, xhr) {
				alert('Notified');
			}).fail(function(xhr) {
				alert('Failed');
			});
		});
		$('#all').click(function() {
			if($(this).is(':checked')) {
				$('#mag-list').css({
					'display': 'none',
				});
				$('#n-magazine').attr('disabled', 'disabled');
			} 
		});
		$('#magazine').click(function() {
			if($(this).is(':checked')) {
				$('#mag-list').css({
					'display': 'table-row',
				});
				$('#n-magazine').removeAttr('disabled');
			}
		});
		// function updateCount() { 
		// 	var maxChar = 60;
		// 	var currentLength = $('#n-message').val().length;
		// 	if(currentLength < maxChar) {
		// 		var remaining = maxChar - currentLength;
		// 		$('#count').text(remaining+'/'+maxChar);
		// 	} else {
		// 		return false;
		// 	}
		// };
		// updateCount(); 
		// $('#n-message').keypress(function(e) { 
		// 	var maxChar = 60;
		// 	var currentLength = $(this).val().length;
		// 	if(currentLength < maxChar) {
		// 		var remaining = maxChar - currentLength;
		// 		$('#count').text(remaining+'/'+maxChar);
		// 	} else if(currentLength == maxChar) {
		// 		e.preventDefault();
		// 		// return false;
		// 	} else if(currentLength >= maxChar) {
		// 		e.preventDefault();
		// 		$(this).val() = $(this).val().substring(0, maxChar);
		// 	}
		// 	// 
		// });
		// $('#n-message').keydown(function(e) { 
		// 	// console.log(e.which)
		// 	var maxChar = 60;
		// 	var currentLength = $(this).val().length;
		// 	if(e.which == 8) {
				
		// 			var remaining = maxChar - currentLength;
		// 			$('#count').text(remaining+'/'+maxChar);
				
		// 	}
		// });
		var maxChar = 60;
        var cs = $(this).val().length;
        $('#count').text(cs+"/"+maxChar);

	    $('#n-message').on('input', function(){
	    	var maxChar = 60;
            var cs = $(this).val().length;
            if(cs>maxChar){ 
                // alert("Title max character is 140!");
                $(this).val($(this).val().substring(0, maxChar));
                cs = $(this).val().length;
                $('#count').text(cs+"/"+maxChar);
            }
            else{
                $('#count').text(cs + "/"+maxChar);
            }
	    });

	    // $('#n-message').change(function(){
	    // 	var maxChar = 60; 
     //        var cs = $(this).val().length;
     //        if(cs>maxChar){
     //            alert("Title max character is 140!");
     //            $(this).val('');
     //            cs = $(this).val().length;
     //            $('#count').text(cs+"/"+maxChar);
     //        }
     //        else{
     //            $('#count').text(cs + "/"+maxChar);
     //        }
	    // });
	});
</script>