<?


class Magazine_Category extends Item{
	
	var $table="magazine_category",$primarykey="category_id",$shortname="magazine",$version="1.0";

	function Magazine_Category(){}
	
	function getName($id){
		$sql="select name from $this->table where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}

	function delete($id){
		$sql="select parent_id from $this->table where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	$thisparentid=$myrow[0];	}
		mysql_free_result($result);
			
		$sql="select category_id from $this->table where parent_id='$id'";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			$sql="update $this->table set parent_id='$thisparentid' where category_id='$myrow[0]'";
			mysql_query($sql,$this->db);
		}
		mysql_free_result($result);
		
		$sql="select magazine_id from magazine_publisher where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			$sql="update magazine_publisher set category_id='$thisparentid' where magazine_id='$myrow[0]'";
			mysql_query($sql,$this->db);
		}
		mysql_free_result($result);
		
		$sql="delete from $this->table where category_id='$id'";
		mysql_query($sql,$this->db);
		
		$this->logEvent("Delete");
	}	

	function getCategoryTree($parent_id, $parentOnly = FALSE, $childOnly = FALSE){
		global $cattree_top,$vvcategory_id,$vcategory_id,$vparent_id,$category_id;
				
		$sql="select category_id,name,parent_id from $this->table where parent_id='$parent_id' order by name";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			if($parent_id!=0){ $cattree_top = $cattree_top." > ".stripslashes($myrow[1]); }else{ $cattree_top = stripslashes($myrow[1]); }
			
			if($childOnly && $parent_id != 0){
				if($myrow[0]!=$category_id){
					echo "<option value=\"$myrow[0]\" ".($myrow[0]==$vparent_id?"selected":"").">$cattree_top</option>";	
				}else{
					echo "<option value=\"$myrow[0]\" selected>$cattree_top</option>";	
				}
			}
			else if(!$childOnly){
				if($myrow[0]!=$vcategory_id){ 
					if($myrow[0]!=$category_id){
						echo "<option value=\"$myrow[0]\" ".($myrow[0]==$vparent_id?"selected":"").">$cattree_top</option>";	
					}else{
						echo "<option value=\"$myrow[0]\" selected>$cattree_top</option>";	
					}
				}
			}

			if(!$parentOnly){
				$sql1="select category_id from $this->table where parent_id='$myrow[0]'";
				$result1=mysql_query($sql1,$this->db);
				if(mysql_num_rows($result1)!=0){	$this->getCategoryTree($myrow[0]);		}
				mysql_free_result($result1);
				
				$cattree_top = substr($cattree_top,0,strrpos($cattree_top,">")-1);
			}
		}
		mysql_free_result($result);
	}
}

?>