Contact Form 4.1
========================

You can choose to download only the Contact Form component or the complete Panel Admin Suite. 
Before installing, we recommend you to take a look at the System Requirements.

System Requirements
-------------------

Script Type 			: PHP Server Side Scripting 

Operating System 		: Linux or Windows Server

Web Server 			: Apache or IIS 

PHP Version 			: PHP 4.2 or above 

PHP Configuration 		: ZLib library *
				: Safe_Mode = OFF
						
Database 			: MySQL 

Web Browser 			: Internet Explorer 5 or above, Firefox, Mozilla, Javascript and cookie enable 

Computer Hardware 		: PC and Mac 


* ZLib library is required to run the encrypted codes. We recommend PHP 4.2 and above as the lower 
  versions will results in buffer handling error.



Installation Guides
-------------------

1. Download the Contact Form distribution ZIP file. 

2. Extract and upload the entire panel folder to the root level of your web server. 

	Example : http://your_domain_name.com/panel/ 
		
 
3. For Linux system, set the permission for the panel/_files folder and panel/config.php file to Read, Write, 
   Execute for all parties.


	Linux command example : chmod 777 _files, chmod 777 config.php
		
		
4. To access the Installation Wizard, visit Panel Admin URL using your web browser, http://your_domain_name.com/panel/. Follow the instructions in 
   the Installation Wizard.
   
   * For Windows Server, you access to Panel Admin using http://your_domain_name.com/panel/index.php
   
5. Once the installation is completed, reset the file permission for 'config.php' file to 644.

6. You may proceed to login to the Administration area. 


Integration Guides
------------------

1. Using a text editor, open the web page file (.php file), and put the code below to the top of the file (line 1), to call the 'config.php' file. This will load all the necessary database connection, class declaration files and load session.

	<?php include_once("document_path_of_panel_folder/config.php"); include_once("document_path_of_panel_folder/common/session.php"); include_once("document_path_of_panel_folder/common/css.php"); ?>

	* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.


2. In your .php web page file, put the line below to call the Contact Form content body.

	<?php include("document_path_of_panel_folder/contactform/home.contact.php"); ?>


3. For sample integration code, please refer to the source code of this file. To do this, open the file with your text editor.

** To find your 'document_path_of_panel_folder', check the path['docroot'] value from the panel/config.php file 


TIPS
----

You can put your front-end file in any location you like, but put the call to the Panel Admin folder using the 
relative path. 
   
Example, if your front-end file is at http://yourdomain.com/contact.php and the Panel Admin folder is located 
at http://yourdomain.com/panel/ , use the code below.

	<?php include_once("panel/config.php") ?>
		
	<?php include("panel/contactform/home.contact.php") ?> 
	



