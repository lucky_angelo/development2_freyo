#Contact Form 4.1


CREATE TABLE contact_field (
  field_id int(10) NOT NULL auto_increment,
  fieldname varchar(30) NOT NULL default '',
  fieldtype varchar(30) NOT NULL default '',
  fieldtext text NOT NULL,
  fieldrequire varchar(10) NOT NULL default '',
  sequence int(10) NOT NULL default '0',
  width int(10) NOT NULL default '0',
  height int(10) NOT NULL default '0',
  default_val text NOT NULL,
  valid_type varchar(10) NOT NULL default '',
  sendauto varchar(5) NOT NULL default '',
  itemrow int(5) NOT NULL default '0',
  fieldsize int(5) NOT NULL default '0',
  fileexten varchar(10) NOT NULL default '',
  uploadtype text NOT NULL,
  PRIMARY KEY  (field_id)
) ENGINE=MyISAM;


INSERT INTO contact_field VALUES (1, 'field1', 'textbox', 'First Name', 'yes', 1, 200, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (2, 'field2', 'textbox', 'Last Name', 'yes', 2, 200, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (3, 'field3', 'textbox', 'Email', 'yes', 3, 200, 50,'','Validemail','Yes','','','','');
INSERT INTO contact_field VALUES (4, 'field4', 'textbox', 'Phone', 'no', 4, 150, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (5, 'field5', 'textbox', 'Fax', 'no', 5, 150, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (6, 'field6', 'textbox', 'Website URL', 'no', 6, 250, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (7, 'field7', 'textbox', 'Subject', 'yes', 7, 350, 50,'','None','','','','','');
INSERT INTO contact_field VALUES (8, 'field8', 'textarea', 'Message', 'yes', 8, 350, 150,'','None','','','','','');


CREATE TABLE contact_fieldoption (
  option_id int(10) NOT NULL auto_increment,
  field_id int(10) NOT NULL default '0',
  optionvalue text NOT NULL,
  as_default varchar(5) default NULL,
  use_empty varchar(5) default NULL,
  PRIMARY KEY  (option_id)
) ENGINE=MyISAM;


CREATE TABLE contact_form (
  contact_id int(10) NOT NULL auto_increment,
  datepost datetime NOT NULL default '0000-00-00 00:00:00',
  field1 text,
  field2 text,
  field3 text,
  field4 text,
  field5 text,
  field6 text,
  field7 text,
  field8 text,
  PRIMARY KEY  (contact_id)
) ENGINE=MyISAM;


CREATE TABLE contact_predefinelist(
  predefine_id int(10) NOT NULL auto_increment,
  predefine_name varchar(50) default NULL,
  PRIMARY KEY  (predefine_id)
) ENGINE=MyISAM;

CREATE TABLE contact_predefineitem(
  predefineitem_id int(255) NOT NULL auto_increment,
  predefine_id int(10) default NULL,
  item_name varchar(50) default NULL,
  sequence int(10) default NULL,
  PRIMARY KEY  (predefineitem_id)
) ENGINE=MyISAM;


INSERT INTO sys_component VALUES (NULL, 'Contact Form', 'contactform', 'contactform', 'Simple and easy-to-use Contact Form for your website. Easily fit with web design pages. Customizable form fields, validation and email notification. ', '4.1', 'wce.listcontact.php', '', 'Show', '');
INSERT INTO sys_variable VALUES ('contact_listno', '20');
INSERT INTO sys_variable VALUES ('contact_emailnotice', 'yes');
INSERT INTO sys_variable VALUES ('contact_noticesubject', 'New Inquiry Posted #ID : [[contact_id]]');
INSERT INTO sys_variable VALUES ('contact_noticecontent', 'There was a new inquiry posted at your website. \r\n\r\n[[message]]\r\n\r\n');
INSERT INTO sys_variable VALUES ('contact_autoresponse', 'yes');
INSERT INTO sys_variable VALUES ('contact_emailsubject', 'Thank You For Contacting Us');
INSERT INTO sys_variable VALUES ('contact_emailcontent', 'This is an acknowledgement for you that your message has  been sent successfully. We will attend to your inquiry shortly.\r\n\r\n[[message]]\r\n\r\nRegards\r\nYour Company');
INSERT INTO sys_variable VALUES ('contact_thankyou', '<p><font size=3><strong>Thank You</strong></font></p><p>Your message has been sent. We will attend to your inquiry shortly.</p>');
INSERT INTO sys_variable VALUES ('contact_titlefont', 'Arial');
INSERT INTO sys_variable VALUES ('contact_titlecolor', '#000000');
INSERT INTO sys_variable VALUES ('contact_titlesize', '16');
INSERT INTO sys_variable VALUES ('contact_titletext', 'Contact Form');
INSERT INTO sys_variable VALUES ('contact_descpfont', 'Arial');
INSERT INTO sys_variable VALUES ('contact_descpcolor', '#000000');
INSERT INTO sys_variable VALUES ('contact_descpsize', '12');
INSERT INTO sys_variable VALUES ('contact_descptext', 'Please submit your contact to us.');
INSERT INTO sys_variable VALUES ('contact_buttoncaption', 'Submit Form');
INSERT INTO sys_variable VALUES ('contact_enableimage', 'Yes');
INSERT INTO sys_variable VALUES ('contact_adminemail', '');
INSERT INTO sys_variable VALUES ('contact_sendextra', '');


INSERT INTO contact_predefinelist(predefine_id,predefine_name) VALUES ('1', 'Country List');

INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Afghanistan','1');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Albania','2');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Algeria','3');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Andorra','4');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Angola','5');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Anguilla','6');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Antigua and Barbuda','7');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Antilles (Netherlands)','8');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Argentina','9');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Armenia','10');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Australia','11');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Austria','12');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Azerbaijan','13');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bahamas','14');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bahrain','15');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bangladesh','16');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Barbados','17');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Belarus','18');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Belgium','19');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Belize','20');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Benin','21');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bermuda','22');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bhutan','23');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bolivia','24');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bosnia and Herzegovina','25');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Botswana','26');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Brazil','27');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','British Virgin Islands','28');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Brunei Darussalam','29');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Bulgaria','30');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Burkina Faso','31');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Burundi','32');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cambodia','33');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cameroon','34');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Canada','35');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cape Verdi','36');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cayman Islands','37');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Central African Republic','38');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Chad','39');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Chile','40');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','China','41');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Colombia','42');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Comoros','43');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cook Islands','44');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Costa Rica','45');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cote d\'Ivoire','46');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Croatia','47');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cuba','48');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Cyprus','49');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Czech Republic','50');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Democratic Republic of Congo','51');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Denmark','52');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Diego Garcia','53');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Djibouti','54');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Dominica','55');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Dominican Republic','56');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','East Timor','57');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Ecuador','58');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Egypt','59');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','El Salvador','60');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Equatorial Guinea','61');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Eritrea','62');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Estonia','63');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Ethiopia','64');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Falklands','65');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Faroe Islands','66');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Fiji','67');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Finland','68');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','France','69');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','French Guiana','70');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','French Polynesia','71');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','FSM','72');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Gabon','73');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Gambia','74');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Georgia','75');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Germany','76');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Ghana','77');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Gibraltar','78');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Greece','79');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Greenland','80');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Grenada','81');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guadeloupe','82');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guam','83');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guatemala','84');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guinea','85');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guinea-Bissau','86');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Guyana','87');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Haiti','88');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Hawaii','89');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Holy See (Vatican City)','90');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Honduras','91');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Hong Kong','92');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Hungary','93');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Iceland','94');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','India','95');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Indonesia','96');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Iran','97');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Iraq','98');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Ireland','99');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Israel','100');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Italy','101');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Jamaica','102');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Japan','103');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Jordan','104');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Kazakhstan','105');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Kenya','106');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Kiribati','107');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Korea, North','108');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Korea, South','109');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Kuwait','110');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Kyrgyzstan','111');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Laos','112');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Latvia','113');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Lebanon','114');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Lesotho','115');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Liberia','116');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Libya','117');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Liechtenstein','118');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Lithuania','119');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Luxembourg','120');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Macedonia','121');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Madagascar','122');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Malawi','123');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Malaysia','124');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Maldives','125');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mali','126');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Malta','127');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Marshall Islands','128');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Martinique','129');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mauritania','130');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mauritius','131');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mexico','132');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Moldova','133');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Monaco','134');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mongolia','135');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Montserrat','136');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Morocco','137');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Mozambique','138');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Myanmar','139');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Namibia','140');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Nauru','141');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Nepal','142');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Netherlands','143');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','New Caledonia','144');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','New Zealand','145');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Nicaragua','146');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Niger','147');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Nigeria','148');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Northern Marianas','149');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Norway','150');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Oman','151');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Pakistan','152');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Palau','153');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Palestine','154');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Panama','155');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Papua New Guinea','156');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Paraguay','157');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Peru','158');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Philippines','159');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Pitcairn Island','160');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Poland','161');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Portugal','162');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Puerto Rico','163');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Qatar','164');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Republic of Cong','165');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Romania','166');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Russia','167');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Rwanda','168');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Saint Kitts and Nevis','169');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Saint Vincent and the Grenadines','170');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Samoa','171');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','San Marino','172');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Sao Tome and Principe','173');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Saudi Arabia','174');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Senegal','175');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Serbia and Montenegro','176');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Seychelles','177');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Sierra Leone','178');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Singapore','179');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Slovak Republic','180');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Slovenia','181');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Solomon Islands','182');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Somalia','183');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','South Africa','184');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Spain','185');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','St Lucia','186');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Sudan','187');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Suriname','188');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Swaziland','189');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Sweden','190');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Switzerland','191');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Syria','192');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Taiwan','193');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Tajikistan','194');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Tanzania','195');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Thailand','196');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Togo','197');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Tonga','198');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Trinidad and Tobago','199');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Tunisia','200');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Turkey','201');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Turkmenistan','202');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Turks and Caicos Islands','203');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Tuvalu','204');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Uganda','205');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Ukraine','206');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','United Arab Emirates','207');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','United Kingdom','208');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','United States','209');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','United States Virgin Islands','210');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Uruguay','211');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Uzbekistan','212');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Vanuatu','213');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Venezuela','214');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Vietnam','215');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Wallis and Futuna','216');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Western Sahara','217');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Yemen','218');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Zambia','219');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'1','Zimbabwe','220');

INSERT INTO contact_predefinelist(predefine_id,predefine_name) VALUES ('2', 'Day');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','01','1');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','02','2');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','03','3');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','04','4');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','05','5');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','06','6');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','07','7');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','08','8');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','09','9');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','10','10');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','11','11');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','12','12');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','13','13');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','14','14');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','15','15');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','16','16');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','17','17');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','18','18');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','19','19');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','20','20');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','21','21');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','22','22');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','23','23');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','24','24');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','25','25');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','26','26');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','27','27');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','28','28');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','29','29');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','30','30');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'2','31','31');


INSERT INTO contact_predefinelist(predefine_id,predefine_name) VALUES ('3', 'Month');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','01','1');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','02','2');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','03','3');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','04','4');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','05','5');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','06','6');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','07','7');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','08','8');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','09','9');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','10','10');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','11','11');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'3','12','12');

INSERT INTO contact_predefinelist(predefine_id,predefine_name) VALUES ('4', 'Year');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1900','1');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1901','2');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1902','3');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1903','4');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1904','5');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1905','6');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1906','7');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1907','8');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1908','9');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1909','10');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1910','11');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1911','12');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1912','13');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1913','14');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1914','15');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1915','16');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1916','17');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1917','18');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1918','19');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1919','20');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1920','21');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1921','22');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1922','23');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1923','24');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1924','25');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1925','26');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1926','27');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1927','28');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1928','29');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1929','30');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1930','31');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1931','32');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1932','33');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1933','34');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1934','35');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1935','36');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1936','37');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1937','38');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1938','39');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1939','40');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1940','41');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1941','42');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1942','43');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1943','44');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1944','45');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1945','46');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1946','47');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1947','48');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1948','49');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1949','50');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1950','51');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1941','52');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1942','53');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1943','54');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1944','55');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1945','56');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1946','57');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1947','58');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1948','59');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1949','60');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1960','61');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1961','62');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1962','63');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1963','64');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1964','65');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1965','66');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1966','67');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1967','68');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1968','69');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1969','70');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1970','71');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1971','72');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1972','73');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1973','74');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1974','75');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1975','76');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1976','77');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1977','78');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1978','79');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1979','80');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1980','81');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1981','82');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1982','83');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1983','84');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1984','85');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1985','86');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1986','87');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1987','88');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1988','89');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1989','90');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1990','91');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1991','92');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1992','93');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1993','94');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1994','95');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1995','96');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1996','97');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1997','98');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1998','99');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','1999','100');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2000','101');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2001','102');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2002','103');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2003','104');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2004','105');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2005','106');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2006','107');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2007','108');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2008','109');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2009','110');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2010','111');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2011','112');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2012','113');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2013','114');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2014','115');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2015','116');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2016','117');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2017','118');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2018','119');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2019','120');
INSERT INTO contact_predefineitem (predefineitem_id,predefine_id,item_name,sequence) VALUES (NULL,'4','2020','121');