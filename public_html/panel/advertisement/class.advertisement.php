<?





class Advertisement extends Item{

	

	var $table="ads_item",$primarykey="ads_id",$shortname="advertisement",$version="1.0";


	function Advertisement() {}

	function All_Ads(){
		global $wselected_id;

		$select_ads = "";

		$sqlQuery = "SELECT ads_id, caption FROM $this->table ORDER BY caption";
		$result = mysql_query($sqlQuery, $this->db);
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$select_ads .= "<option value='".$row['ads_id']."' ".($row['ads_id']==$wselected_id?"selected":"").">".$row['caption']."</option>";
		}
		echo $select_ads;
	}

	function AdsDetails($ads_id = FALSE){
		global $wselected_id;
		if($ads_id === FALSE){
			$ads_id = $wselected_id;
		}
		if($ads_id !== FALSE){
			$sqlQuery = "SELECT * FROM $this->table WHERE ads_id = $ads_id LIMIT 1";
			$result = mysql_query($sqlQuery, $this->db);
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			mysql_free_result($result);
			return $row;
		}
		return false;
	}

	function AdsReport($ads_id = FALSE){
		global $wselected_id;
		$adsReport = [];

		if($ads_id === FALSE){
			$sqlQuery = "SELECT ads_id FROM $this->table LIMIT 1";
			$result = mysql_query($sqlQuery, $this->db);
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$ads_id = $row['ads_id'];
			mysql_free_result($result);
		}
		
		if($ads_id !== FALSE){
			$ads_id = preg_replace('[!0-9]', '', $ads_id);
			$wselected_id = $ads_id;

			$totalQuery = "SELECT count( report_id ) as total_view FROM ads_report WHERE ads_id = $ads_id";
			$result = mysql_query($totalQuery, $this->db);
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$adsReport['total_view'] = $row['total_view'];
			mysql_free_result($result);

			/* GENDER */
			$genderQuery = "SELECT fup.sex, count( ar.report_id ) as total_view,
			ai.caption
			FROM `ads_report` AS ar
			LEFT JOIN ads_item AS ai ON ai.ads_id = ar.ads_id
			LEFT JOIN freyo_user_profile AS fup ON ar.user_id = fup.user_id
			WHERE ai.ads_id = $ads_id
			GROUP BY fup.sex";

			$result = mysql_query($genderQuery, $this->db);
			$x=0;
			while($theresult = mysql_fetch_array($result, MYSQL_ASSOC)){
				$arrayName = ucfirst($theresult['sex']);
				if($theresult['sex'] == null){
					$arrayName = 'Guest';
				}elseif($theresult['sex'] == 'unknown'){
					$arrayName = 'Other';
				}
				unset($theresult['sex']);
				$adsReport['Gender'][$arrayName] = $theresult;
				$x++;
			}
			mysql_free_result($result);

			/* LOCATION */
			$locationQuery = "SELECT (SELECT name FROM ph_provinces WHERE id = fup.province LIMIT 1) as province, ai.caption, count(ar.report_id) as total_view
			FROM `ads_report` AS ar
			LEFT JOIN ads_item AS ai ON ai.ads_id = ar.ads_id
			LEFT JOIN freyo_user_profile AS fup ON ar.user_id = fup.user_id
			WHERE ai.ads_id = $ads_id
			GROUP BY fup.province";

			$result = mysql_query($locationQuery, $this->db);
			$x=0;
			while($theresult = mysql_fetch_array($result, MYSQL_ASSOC)){
				$arrayName = ucfirst($theresult['province']);
				if($theresult['province'] == null){
					$arrayName = 'Unknown';
				}
				unset($theresult['province']);
				$adsReport['Location'][$arrayName] = $theresult;
				$x++;
			}
			mysql_free_result($result);

			/* MAGAZINE */
			$magazineQuery = "SELECT mp.title, mp.views, ai.caption, count(ar.report_id) as total_view
			FROM `ads_report` AS ar
			LEFT JOIN ads_item AS ai ON ai.ads_id = ar.ads_id
			LEFT JOIN magazine_publisher AS mp ON ar.magazine_id = mp.magazine_id
			WHERE ai.ads_id = $ads_id
			GROUP BY mp.magazine_id";

			$result = mysql_query($magazineQuery, $this->db);
			$x=0;
			while($theresult = mysql_fetch_array($result, MYSQL_ASSOC)){
				$arrayName = ucfirst($theresult['title']);
				if($theresult['title'] == null){
					$arrayName = 'Removed';
				}
				unset($theresult['title']);
				$adsReport['Magazine'][$arrayName] = $theresult;
				$x++;
			}
			mysql_free_result($result);

			$magazineQuery = "SELECT title, views FROM magazine_publisher WHERE magazine_id NOT IN (SELECT DISTINCT magazine_id FROM ads_report WHERE ads_id = $ads_id)";
			$result = mysql_query($magazineQuery, $this->db);
			$x=0;

			while($theresult = mysql_fetch_array($result, MYSQL_ASSOC)){
				$arrayName = ucfirst($theresult['title']);
				unset($theresult['title']);
				$theresult['caption'] = "";
				$theresult['total_view'] = 0;
				$adsReport['Magazine'][$arrayName] = $theresult;
				$x++;
			}
			mysql_free_result($result);

			/* BY DAY */
			$byDayQuery = "SELECT ai.caption, DATE_FORMAT( ar.datetime_viewed, '%Y %M %d' ) AS day, count(ar.report_id) as total_view
			FROM `ads_report` AS ar
			LEFT JOIN ads_item AS ai ON ai.ads_id = ar.ads_id
			WHERE ai.ads_id = $ads_id
			GROUP BY DATE_FORMAT( ar.datetime_viewed, '%Y-%m-%d' )";

			$result = mysql_query($byDayQuery, $this->db);
			$x=0;
			while($theresult = mysql_fetch_array($result, MYSQL_ASSOC)){
				$arrayName = $theresult['day'];
				if($theresult['day'] == null){
					$arrayName = 'Removed';
				}
				unset($theresult['day']);
				$adsReport['Daily'][$arrayName] = $theresult;
				$x++;
			}
			mysql_free_result($result);
		}
		else{
			// DO SOMETHING?
		}
		return $adsReport;
	}

}



?>