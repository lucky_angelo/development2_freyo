<style type="text/css">
	.ads-description {
		width: 60%;
		float: left;
	}
	.gender-canvas {
		width: 40%;
		float: left;
	}
	.magazine-canvas {
		width: 100%;
		float: left;
	}
	.location-canvas {
		width: 100%;
		float: left;
	}
	.daily-canvas {
		width: 100%;
		float: left;
	}
</style>

<?

switch($pageaction){
	case 'search':
		$adsReport = $oAds->adsReport($ads_id);
		$adsDetails = $oAds->adsDetails($ads_id);
	break;
	default:
		$adsReport = $oAds->adsReport();
		$adsDetails = $oAds->adsDetails();
	break;
}

echo "
<table border=0 width=100%><tr><td><b>Advertisement Manager</b></td><td>";
include("wce.menu.php");
echo "</td></tr></table><hr size=1 color=#606060><br>";
?>   
<div style="margin-bottom: 20px">
<form name="thisform" method="post" action="index.php?component=advertisement&page=wce.adsdashboard.php">
<input type=hidden name=pageaction value="search">
<span>Select Ads: </span>
<select name="ads_id">
	<? $oAds->All_Ads(); ?>
</select>
</form>
</div>

<div class="ads-description">
<? 
if($adsDetails['type'] == 'image'){
	echo "<img src='".$path['webroot']."_files/advertisement/".$adsDetails['content']."' style='float:left; height: 190px; width: auto; margin-right: 20px'>";
}
echo "
<h4>".$adsDetails['caption']."</h4>
<h5>Impression: ".$adsDetails['view']."</h5>
<h5>Ads Type: ".$adsDetails['type']."</h5>
";
?>
</div>
<div class="gender-canvas">
    <canvas id="genderCanvas"></canvas>
</div>
<div class="magazine-canvas">
    <canvas id="magazineCanvas"></canvas>
</div>
<div class="location-canvas">
    <canvas id="locationCanvas"></canvas>
</div>
<div class="daily-canvas">
    <canvas id="dailyCanvas"></canvas>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		$('select[name="ads_id"]').chosen({
		    disable_search_threshold: 10, width: "30%"
		});
		$('select[name="ads_id"]').change(function(){
			$(this).parent('form').submit();
		});
	});

	var adsReport = <? echo json_encode($adsReport); ?>;
	console.log(adsReport);
	
	var randomColorFactor = function() {
	    return Math.round(Math.random() * 255);
	};
	var randomColor = function(opacity) {
	    return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
	};

	var configGen = {
	    type: 'bar',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Gender'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    suggestedMax: suggestedMax,
	                }
	            }]
	        }
	    }
	};
	var configMag = {
	    type: 'bar',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Magazine'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    suggestedMax: suggestedMax,
	                }
	            }]
	        }
	    }
	};
	var configLoc = {
	    type: 'bar',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Location'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    suggestedMax: suggestedMax,
	                }
	            }]
	        }
	    }
	};
	var configDaily = {
	    type: 'line',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Date'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    suggestedMax: suggestedMax,
	                }
	            }]
	        }
	    }
	};

	var total_impression;
	var suggestedMax;
	var adsGender = {};
	var adsLocation = {};
	var adsMagazine = {};
	var adsDaily = {};

	var populateData = function(adsData){
		var wlabel = [];
		var wdata = [];
		var wwdata = [];
		$.each(adsData, function(index, value) {
			switch(index){
				case 'total_view':
					suggestedMax = Math.ceil(value/1000)*1000;
					break;
				case 'Gender':
					wlabel = [];
					wdata = [];
					$.each(value, function(windex, wvalue){
						wlabel.push(windex);
						wdata.push(wvalue.total_view);
					});
					adsGender = {
						labels: wlabel,
						datasets: [{
							label: 'Impression by '+index,
							data: wdata,
							backgroundColor: '#3c3f44',
						}]
					};
					configGen.data = adsGender;
					break;
				case 'Magazine':
					wlabel = [];
					wdata = [];
					wwdata = [];
					$.each(value, function(windex, wvalue){
						wlabel.push(windex);
						wdata.push(wvalue.total_view);
						wwdata.push(wvalue.views);
					});
					adsMagazine = {
						labels: wlabel,
						datasets: [{
							label: 'Impression by '+index,
							data: wdata,
							backgroundColor: '#3c3f44',
							},
							{
							label: 'Total Views of '+index,
							data: wwdata,
							backgroundColor: '#ccc',
						}]
					};
					configMag.data = adsMagazine;
					break;
				case 'Location':
					wlabel = [];
					wdata = [];
					$.each(value, function(windex, wvalue){
						wlabel.push(windex);
						wdata.push(wvalue.total_view);
					});
					adsLocation = {
						labels: wlabel,
						datasets: [{
							label: 'Impression by '+index,
							data: wdata,
							backgroundColor: '#3c3f44',
						}]
					};
					configLoc.data = adsLocation;
					break;
				case 'Daily':
					wlabel = [];
					wdata = [];
					$.each(value, function(windex, wvalue){
						wlabel.push(windex);
						wdata.push(wvalue.total_view);
					});
					adsDaily = {
						labels: wlabel,
						datasets: [{
							label: 'Impression '+index,
							data: wdata,
							backgroundColor: '#3c3f44',
							pointBackgroundColor: 'rgba(1,1,1,0.5)',
							pointBorderColor: 'rgba(1,1,1,0.5)',
							pointBorderWidth: 5
						}]
					};
					configDaily.data = adsDaily;
					break;
			}
		});
	}

	window.onload = function() {
		populateData(adsReport);
		console.log('configGen');
		console.log(configGen);
		console.log('configMag');
		console.log(configMag);
		console.log('configLoc');
		console.log(configLoc);
		console.log('configDaily');
		console.log(configDaily);
	    var ctxGender = document.getElementById("genderCanvas").getContext("2d");
	    window.myLine = new Chart(ctxGender, configGen);
	    var ctxMag = document.getElementById("magazineCanvas").getContext("2d");
	    window.myLine = new Chart(ctxMag, configMag);
	    var ctxLoc = document.getElementById("locationCanvas").getContext("2d");
	    window.myLine = new Chart(ctxLoc, configLoc);
	    var ctxDaily = document.getElementById("dailyCanvas").getContext("2d");
	    window.myLine = new Chart(ctxDaily, configDaily);
	};

	$('#changeDataObject').click(function() {
	    config.data = {
	        labels: ["July", "August", "September", "October", "November", "December"],
	        datasets: [{
	            label: "My First dataset",
	            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
	            fill: false,
	        }, {
	            label: "My Second dataset",
	            fill: false,
	            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
	        }]
	    };

	    $.each(config.data.datasets, function(i, dataset) {
	        dataset.borderColor = randomColor(0.4);
	        dataset.backgroundColor = randomColor(0.5);
	        dataset.pointBorderColor = randomColor(0.7);
	        dataset.pointBackgroundColor = randomColor(0.5);
	        dataset.pointBorderWidth = 1;
	    });

	    // Update the chart
	    window.myLine.update();
	});

	$('#addDataset').click(function() {
	    var newDataset = {
	        label: 'Dataset ' + config.data.datasets.length,
	        borderColor: randomColor(0.4),
	        backgroundColor: randomColor(0.5),
	        pointBorderColor: randomColor(0.7),
	        pointBackgroundColor: randomColor(0.5),
	        pointBorderWidth: 1,
	        data: [],
	    };

	    for (var index = 0; index < config.data.labels.length; ++index) {
	        newDataset.data.push(randomScalingFactor());
	    }

	    config.data.datasets.push(newDataset);
	    window.myLine.update();
	});

	$('#addData').click(function() {
	    if (config.data.datasets.length > 0) {
	        var month = MONTHS[config.data.labels.length % MONTHS.length];
	        config.data.labels.push(month);

	        $.each(config.data.datasets, function(i, dataset) {
	            dataset.data.push(randomScalingFactor());
	        });

	        window.myLine.update();
	    }
	});

	$('#removeDataset').click(function() {
	    config.data.datasets.splice(0, 1);
	    window.myLine.update();
	});

	$('#removeData').click(function() {
	    config.data.labels.splice(-1, 1); // remove the label first

	    config.data.datasets.forEach(function(dataset, datasetIndex) {
	        dataset.data.pop();
	    });

	    window.myLine.update();
	});
</script>