#Advertisement v1.0

CREATE TABLE IF NOT EXISTS `ads_item` (
  `ads_id` int(11) NOT NULL,
  `type` enum('image','video','form') NOT NULL COMMENT 'ads type',
  `section` enum('all','category','issue') NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `content` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `age2` int(11) NOT NULL,
  `gender` enum('a','m','f') NOT NULL COMMENT 'a - all, m - male, f - female',
  `location` varchar(255) NOT NULL DEFAULT 'all',
  `impression` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `status` enum('a','p','e') NOT NULL COMMENT 'a - approved, p - pending, e - expired',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `important` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO sys_component VALUES (NULL, 'Advertisement', 'ads', 'ads', 'Advertisement', '1.0', 'wce.addads.php', '1', 'Show','');