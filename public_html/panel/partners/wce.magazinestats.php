<style type="text/css">
	.detail-heading {
		font-size: 2em; 
		margin-bottom: 20px; 
		width:100%
	}
	.detail-content {
		font-size: 1.5em; 
		width:100%;
	}
	.mag-title {
		width: 100%;
		margin-bottom: 30px;
	}
	.mag-cover {
		height: 150px;
		width: 125px;
	}
	.cover-container {
		width: 100%;
		margin-bottom: 15px;
	}
	.content-center {
		text-align: center;
	}
</style>
<?

echo "
<table border=0 width=100%><tr><td><b>Partners Dashboard</b></td><td>";
include("wce.menu.php");
echo "</td></tr></table><hr size=1 color=#606060><br>";

switch ($pageaction) {
	case 'filter':
		$magReport = $oPartners->getMagReport($magazine_id, $month, $year);
		$magClickReport = $oPartners->getClickReport($magazine_id, $month, $year);
		break;
	
	default:
		$magReport = $oPartners->getMagReport($magazine_id, date('m'), date('Y'));
		$magClickReport = $oPartners->getClickReport($magazine_id, date('m'), date('Y'));
		break;
}
$details = $oPartners->getMagDetails($magazine_id);

// echo '<pre>';
// var_dump($magReport);
// echo '</pre>';
// echo '<pre>';
// var_dump($magClickReport);
// echo '</pre>';
?>
<script type="text/javascript">
	var magReport = <? echo json_encode($magReport); ?>;
	console.log(magReport);

	var magClickReport = <? echo json_encode($magClickReport); ?>;
	console.log(magClickReport);
</script>
<!-- <div class="mag-title content-center">
</div> -->
<table border=0 cellpadding="3" cellspacing="3" width="100%">
	<tr><td>
		<div class="cover-container"><img src="_files/magazines/cover/<? echo $details[3] ?>" class="mag-cover" /></div>
		<span class="detail-content"><? echo $details[2] ?></span>
	</td><td align="center">
		<div class="detail-heading">Magazine Views</div>
		<div class="detail-content"><? echo $details[0] ?></div>

	</td><td align="center">
		<div class="detail-heading">Magazine Subscribers</div>
		<div class="detail-content"><? echo $details[1] ?></div>

	</td></tr>
</table>
<br><br>
<table>
	<form name="thisform" action="index.php?component=partners&page=wce.magazinestats.php&magazine_id=<? echo $magazine_id ?>&pageaction=filter" method="post">
	<tr><td>
		<label for="month">Month</label>
		<select id="month" name="month">
			<? 
				$months = $oPartners->months();
				if($pageaction == 'filter') {
					foreach ($months as $i => $val) {
						$m = $i + 1;
						$select = ($m == $month) ? "selected" : "";
						echo "<option value=\"$m\" $select>$val</option>";
					}
				} else {
					foreach ($months as $i => $val) {
						$m = $i + 1;
						$select = ($m == date('m')) ? "selected" : "";
						echo "<option value=\"$m\" $select>$val</option>";
					}
				}
				
			?>
		</select>
	</td><td>
		<label for="year">Year</label>
		<select id="year" name="year">
			<?
				$years = $oPartners->years();
				if($pageaction == 'filter') {
					foreach ($years as $val) {
						$select = ($val == $year) ? "selected" : "";
						echo "<option value=\"$val\" $select>$val</option>";
					}
				} else {
					foreach ($years as $val) {
						$select = ($val == date('Y')) ? "selected" : "";
						echo "<option value=\"$val\" $select>$val</option>";
					}
				}
				
			?>
		</select>
	</td><td>
		<input type="button" value="Go" onclick="submit()">
	</td></tr>
	</form>
</table>
<script type="text/javascript">
	function submit() {
		document.thisform.submit();
	}
</script>
<div id="label-income">
	<h1>Impressions</h1>
	<hr>
	<? // echo $d = ($pageaction == 'filter') ? $month : date('m')?><b style="font-size: 1.8em;">Total: P <span></span></b>
</div>
<div class="daily-income">
	<canvas id="dailyIncome"></canvas>
</div>
<br><br>
<div id="label-income-c">
	<h1>Clicks</h1>
	<hr>
	<? // echo $d = ($pageaction == 'filter') ? $month : date('m')?><b style="font-size: 1.8em;">Total: P <span></span></b>
</div>
<div class="daily-income-click">
	<canvas id="dailyIncomeClick"></canvas>
</div>
<script type="text/javascript">
	var configDaily = {
	    type: 'bar',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Date'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    // suggestedMax: Math.ceil(value/1000)*1000;,
	                }
	            }]
	        }
	    }

	};

	var configClickDaily = {
	    type: 'bar',
	    data: [],
	    options: {
	        responsive: true,
	        tooltips: {
	            mode: 'label',
	        },
	        hover: {
	            mode: 'dataset'
	        },
	        scales: {
	            xAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Date'
	                }
	            }],
	            yAxes: [{
	                display: true,
	                scaleLabel: {
	                    show: true,
	                    labelString: 'Impression'
	                },
	                ticks: {
	                    suggestedMin: 0,
	                    // suggestedMax: Math.ceil(value/1000)*1000;,
	                }
	            }]
	        }
	    }

	};

	var incomeDaily = {};
	var incomeClickDaily = {};
	var populate_data = function(magReport) {
		// suggestedMax = Math.ceil(value/1000)*1000;
	    wlabel = [];
		wdata = [];
		var income = 0;
		$.each(magReport, function(windex, wvalue){
			wlabel.push(wvalue.day);
			wdata.push(wvalue.income);
			income += wvalue.income;
			// console.log(wlabel);
			// console.log(wdata);
		    incomeDaily = {
				labels: wlabel,
				datasets: [{
					label: 'Income',
					data: wdata,
					backgroundColor: '#3c3f44',
					pointBackgroundColor: 'rgba(1,1,1,0.5)',
					pointBorderColor: 'rgba(1,1,1,0.5)',
					pointBorderWidth: 5
				}]
			};
			configDaily.data = incomeDaily;
		});
		$('#label-income span').text(income);

		
	};

	var populate_click_data = function(magClickReport) {
		wlabel = [];
		wdata = [];
		var income = 0;
		$.each(magClickReport, function(windex, wvalue){
			wlabel.push(wvalue.day);
			wdata.push(wvalue.income);
			income += wvalue.income;
			// console.log(wlabel);
			// console.log(wdata);
		    incomeClickDaily = {
				labels: wlabel,
				datasets: [{
					label: 'Income',
					data: wdata,
					backgroundColor: '#06c',
					pointBackgroundColor: 'rgba(0, 102, 204, 0.788)',
					pointBorderColor: 'rgba(0, 102, 204, 0.788)',
					pointBorderWidth: 5
				}]
			};
			configClickDaily.data = incomeClickDaily;
		});
		$('#label-income-c span').text(income);
	}

	window.onload = function() {
		populate_data(magReport);
		populate_click_data(magClickReport);
		var ctxDaily = document.getElementById("dailyIncome").getContext("2d");
	    window.myLine = new Chart(ctxDaily, configDaily);
	    var ctxDaily2 = document.getElementById("dailyIncomeClick").getContext("2d");
	    window.myLine = new Chart(ctxDaily2, configClickDaily);
	}
	 
</script>