<?

while( $g_var = each( $_GET ) ){ ${$g_var['key']} = $g_var['value']; }    
while( $p_var = each( $_POST ) ){ ${$p_var['key']} = $p_var['value']; }
while( $c_var = each( $_COOKIE ) ){ ${$c_var['key']} = $c_var['value']; }

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {

	include($path["docroot"]."common/lib.function.php");
	
	include($path["docroot"]."common/lib.template.php");
	include($path["docroot"]."common/class.template.php");
	$oTemplate = new Template($app_path);

	include($path["docroot"]."common/class.generic.php");
	include($path["docroot"]."common/class.validate.php");
	include($path["docroot"]."common/class.item.php");

	include($path["docroot"]."common/class.dbconnect.php");
	$oDb = new DBConnect;
	$oDb->dbhost=$db["host"];
	$oDb->dbuser=$db["user"];
	$oDb->dbpass=$db["pass"];
	$oDb->mydb=$db["used"];
	$oDb->connect();

	include($path["docroot"]."common/class.phpmailer.php");

	include($path["docroot"]."common/class.menu.php");

	include($path["docroot"]."common/richtext/fckeditor.php");

	include($path["docroot"]."common/class.system.php");
	$oSystem = new System;
	$oSystem->db = $oDb->db;

	include($path["docroot"]."common/class.user.php");
	$oUser = new User;
	$oUser->db = $oDb->db;
	
	include($path["docroot"]."common/class.component.php");
	$oComponent = new Component;
	$oComponent->db = $oDb->db;
	
	include($path["docroot"]."common/class.sequence.php");
	$oSequence = new Sequence;
	$oSequence->db = $oDb->db;	
	
	/***** Load Social Media Libraries *****/
	include($path["docroot"]."common/sociallib/connectfb/include/webzone.php");
	include($path["docroot"]."common/sociallib/connectfb/classes/facebook.php");
	
	include($path["docroot"]."common/sociallib/connecttw/twitteroauth/twitteroauth.php");
	include($path["docroot"]."common/sociallib/connecttw/config.php");
	
	/***** Load Language File For Front End *****/
	$lang = array();
	$oUser->data = array("language");
	$result=$oUser->getDetail(1);
	if($myrow=mysql_fetch_row($result)){	$languagefile = $myrow[0]; }
	mysql_free_result($result);

	if(file_exists($path["docroot"]."common/language/$languagefile")){
		include_once($path["docroot"]."common/language/$languagefile"); 
	}

	$oComponent->data = array("shortname");
	$result = $oComponent->includeClass();
	while($myrow=mysql_fetch_row($result)){	

		if(file_exists($path["docroot"]."$myrow[0]/include.php")){
			include_once($path["docroot"]."$myrow[0]/include.php"); 
		}
		if(file_exists($path["docroot"]."$myrow[0]/language/$languagefile")){
			include_once($path["docroot"]."$myrow[0]/language/$languagefile"); 
		}	
	}
	mysql_free_result($result);

	$path["skin"] = $oUser->getSkin();	

}

?>