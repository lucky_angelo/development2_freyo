<?

if($pageaction=="add"){
	$permission = "";
	for($i=0; $i<count($permissionArr)-1;$i++){
		$permission .= $permissionArr[$i] . ", ";
	}
	$error = FALSE;

	$permission .= $permissionArr[$i];
	if( $password != $confirm_password )
	{
		$error = TRUE;
		$error_message = $lang['common']['invalid_password'];
		$status_message = "<b>".$lang['common']['status']." :</b> ".$error_message."<br>";

	}
	else
	{
		$password = setupPassword($username, $password);
		mysql_query('BEGIN');
		mysql_query('START TRANSACTION');
		try{
			switch($user_type) {
				case 'partner':
				if($permission == 'partners') {
					$oUser->data = array("user_id","name","email","username","password","permission","language","fullmenu","pageeffect", "role");
					$oUser->value = array("NULL",addslashes(trim($name)),addslashes(trim($email)),addslashes(trim($username)),addslashes($password),$permission,$language,"Yes","Yes", $user_type);
					$user_id = $oUser->add();
					$partnerName = addslashes(trim($name));
					$sql = "INSERT INTO freyo_partners(partner_name, user_id) VALUES('$partnerName', '".$user_id."')";
					mysql_query($sql) or die(mysql_error());

					foreach($magazine as $index => $mag_id) {
						$sql = "INSERT INTO freyo_partner_magazine(partner_id, magazine_id) VALUES('$user_id', '$mag_id')";
						mysql_query($sql) or die(mysql_error());
					}
					$msg = "<b>".$lang['common']['status']." :</b> ".$lang['common']['newuseradded']."<br>";
				} else {
					$msg = "<b>Error :</b> Select only <b>Partners</b> component if user is partner<br>";
				}
				break;
				case 'advertiser':

				break;
				default:
				if(!in_array('partners', $permissionArr)) {
					$oUser->data = array("user_id","name","email","username","password","permission","language","fullmenu","pageeffect", "role");
					$oUser->value = array("NULL",addslashes(trim($name)),addslashes(trim($email)),addslashes(trim($username)),addslashes($password),$permission,$language,"Yes","Yes", $user_type);
					$user_id = $oUser->add();
					$msg = "<b>".$lang['common']['status']." :</b> ".$lang['common']['newuseradded']."<br>";
				} else {
					$msg = "<b>Error :</b> <b>Partners</b> component is only for partners access<br>";
				}	
				break;
				
			}
			mysql_query('COMMIT');
		} catch (Exception $e) {
			mysql_query('ROLLBACK');
		}
		// $status_message = "<b>".$lang['common']['status']." :</b> ".$lang['common']['newuseradded']."<br>";
		$status_message = $msg;
		$name=""; $email=""; $language=""; $username=""; $password=""; $permission="";
	}
}

if($pageaction=="edit"){
   	$oUser->data = array("name","email","language","username","permission");
	$result=$oUser->getDetail($user_id);

	if($myrow=mysql_fetch_row($result)){
		$name=stripslashes(htmlentities($myrow[0]));
		$email=stripslashes(htmlentities($myrow[1]));
		$language=$myrow[2];
		$username=stripslashes(htmlentities($myrow[3]));
		$permission=$myrow[4];
		$permissionArr = preg_split('/, /', $permission);
	}
	$status_message = "<b>".$lang['common']['status']." :</b> ".$lang['common']['edituserdetail']."<br>";
}

if($pageaction=="update"){
	$permission = "";
	$error = FALSE;
	$error_message = "";

	for($i=0; $i<count($permissionArr)-1;$i++){
		$permission .= $permissionArr[$i] . ", ";
	}
	$permission .= $permissionArr[$i];
	if($user_id==1){
		$password = setupPassword($username, $password);
	   	$oUser->data = array("email","language","password");
		$oUser->value = array(addslashes(trim($email)),$language,addslashes($password));
		$oUser->update($user_id);
	}else{
		if( ! empty( $password ) )
		{
			if( $password != $confirm_password )
			{
				$error = TRUE;
				$error_message = $lang['common']['invalid_password'];
			}
			else
			{
				$oUser->data = array("password");
				$oUser->value = array( addslashes( setupPassword($username, $password) ) );
				$oUser->update($user_id);
			}
		}

		$oUser->data = array("name","email","language","username","permission");
		$oUser->value = array(addslashes(trim($name)),addslashes(trim($email)),$language,addslashes(trim($username)),$permission);
		$oUser->update($user_id);
	}

   	$oUser->data = array("name","email","language","username","permission");
	$result=$oUser->getDetail($user_id);
	if($myrow=mysql_fetch_row($result)){
		$name=stripslashes(htmlentities($myrow[0]));
		$email=stripslashes(htmlentities($myrow[1]));
		$language=$myrow[2];
		$username=stripslashes(htmlentities($myrow[3]));
		$permission=$myrow[4];
		$permissionArr = preg_split('/, /', $permission);
	}

	if( $error === FALSE )
		$status_message = "<b>".$lang['common']['status']." :</b> ".$lang['common']['userdetailupdated']."<br>";
	else
		$status_message = "<b>".$lang['common']['status']." :</b> ".$error_message."<br>";
}

if($pageaction=="delete"){
	$oUser->delete($user_id);
	$sql = "DELETE FROM freyo_partners WHERE user_id = $user_id";
	mysql_query($sql);
	$sql = "DELETE FROM freyo_partner_magazine WHERE partner_id = $user_id";
	mysql_query($sql);
	$status_message = "<b>".$lang['common']['status']." :</b> ".$lang['common']['userdeleted']."<br>";
}
if(!isset($permissionArr)){ $permissionArr = array();}
?>

<!--Menu Start-->
<table border=0 cellpadding=0 cellspacing=0 width=100%>
<tr><td><b><? echo $lang['common']['admin'] ?></b></td><td valign=bottom align=right><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Menu End-->

<!--Body Start-->
<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=index.php?component=common&page=wce.user.php method=post>
<input type=hidden name="pageaction">
<input type=hidden name="user_id" value="<? echo $user_id ?>">
<tr><td width=20%><? echo $lang['common']['name'] ?></td><td><input type=text name=name style="width:230px" value="<? echo $name ?>"></td></tr>
<tr><td><? echo $lang['common']['email'] ?></td><td><input type=text name=email style="width:230px" value="<? echo $email ?>"></td></tr>
<tr><td><? echo $lang['common']['language'] ?></td><td>
	<select name=language style="width:135px">
	<?
		$handle = opendir("./common/language");
		while ($file = readdir($handle)) {
			if(getfileextension($file)==".php"){
				$thislanguage = substr($file,0,strrpos($file,"."));
				if($file==$language){	echo "<option value=\"$file\" selected>$thislanguage</option>";	}else{	echo "<option value=\"$file\">$thislanguage</option>";	}
			}
		}
		closedir($handle);
	?>
	</select>
</td></tr>
<tr><td><? echo $lang['common']['username'] ?></td><td><input type="text" name="username" style="width:135px" value="<? echo $username ?>"></td></tr>

<tr><td><? echo $lang['common']['password'] ?></td><td><input type="password" name="password" style="width:135px" value=""></td></tr>
<tr><td><? echo $lang['common']['confirm_password'] ?></td><td><input type="password" name="confirm_password" style="width:135px" value=""></td></tr>
<tr><td valign=top><? echo $lang['common']['modulepermission']?></td><td>
	<select name=permissionArr[] multiple style="width:230px" size=8>
	<?
		$oSystem->data=array("shortname","name");
		//$oSystem->where = "shortname!='modbuild'";
		$result=$oSystem->getCompList();
		while($myrow=mysql_fetch_row($result)){
			$select = ($permissionArr[0]=="ALL" || in_array($myrow[0],$permissionArr)?"selected":"");
			echo "<option value=\"$myrow[0]\" $select>$myrow[1]</option>";
		}
	?>
	</select>
</td></tr>
<? 	if($pageaction == 'edit' || $pageaction == 'update') {

   	} else {
?>
<tr>
	<td>
		User type
	</td>
	<td>
		<input id="partner" type="checkbox" name="user_type" value="partner"><label for="partner">Partner</label>
	</td>
</tr>
<tr id="choose-magazine" style="display: none">
	<td>Choose magazine</td>
	<td>
		<select id="magazine" name="magazine[]" disabled multiple size="7">
			<? 
				$result = $oUser->getMagazineList();
				while($myrow = mysql_fetch_row($result)) {
					 
						echo "<option value=\"$myrow[0]\">$myrow[1] > $myrow[2]</option>";
					
				} 
			?>
		</select>
	</td>
</tr>
<? 	} ?>
<tr><td></td><td>
	<? if($pageaction=="edit" || $pageaction=="update"){ ?>
		<input type=button value="<? echo $lang['common']['update'] ?>" onclick="document.thisform.pageaction.value='update';document.thisform.submit()">
		<input type=button value="<? echo $lang['common']['delete'] ?>" onclick="<? echo "ConfirmDelete($user_id)"; ?>">
		<input type=button value=" <? echo $lang['common']['back'] ?> " onclick="window.location='index.php?component=common&page=wce.user.php'">
	<? }else{ ?>
		<input type=button value=" <? echo $lang['common']['add'] ?> " onclick="document.thisform.pageaction.value='add';document.thisform.submit()">
	<? } ?>
</td></tr></form></table><br>

<?
	$sort=array("username","permission","lastlogin","lastip");

	if($sortby==""){ $sortby=$sort[0]; }
	if($sortseq==""){ $sortseq="asc"; }
	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sort[$i]&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort In Ascending Order\"></a>"; }
	for($i=0;$i<count($sort);$i++){
		if($sortby==$sort[$i]){
		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=desc><img src=\"common/image/sort_desc.gif\" border=0 alt=\"Sort In Descending Order\"></a>"; }
		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort In Ascending Order\"></a>"; }
	}}

	echo "
	<table border=0 cellpadding=1 cellspacing=0 width=98% align=center>
	    <tr bgcolor=\"#E6E6E6\" height=\"24\">
	      <td class=\"gridTitle\" align=\"center\" width=\"25\">&nbsp;<b>".$lang['common']['number']."</b>&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"100\">&nbsp;<b>".$lang['common']['username']."</b> $sortlink[0]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['common']['permission']."</b> $sortlink[1]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"140\">&nbsp;<b>".$lang['common']['lastlogin']."</b> $sortlink[2]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"50\">&nbsp;</td>
	    </tr>
	";

   	$oUser->data = array("user_id","username","permission","lastlogin","lastip");
   	$oUser->where = "";  $oUser->order = "$sortby $sortseq";
   	$result=$oUser->getList();
   	$count=0;
   	while($myrow=mysql_fetch_row($result)){
   		$count++;
   		$myrow[1] = stripslashes($myrow[1]);
   		$myrow[4] = ($myrow[4]==""?"0.0.0.0":$myrow[4]);
   		echo "
	    <tr style=\"background:#F6F6F6\" height=\"24\">
	      <td class=\"gridRow\" align=\"center\">$count</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[1]</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[2]</td>
	      <td class=\"gridRow\" align=\"center\">&nbsp;$myrow[3] <br> [$myrow[4]]</td>
   		  <td class=\"gridRow\" align=\"center\">
	   		  <a href=index.php?component=common&page=wce.user.php&pageaction=edit&user_id=$myrow[0]><img src=common/image/ico_edit.gif border=0 alt=\"".$lang['common']['editthisrecord']."\"></a>&nbsp;&nbsp;
   		  	  <a href=javascript:ConfirmDelete($myrow[0])><img src=common/image/ico_del.gif border=0 alt=\"".$lang['common']['deletethisrecord']."\"></a>
   		  </td>
		</tr>
		";
   	}
   	mysql_free_result($result);

	echo "</table>";
?>


<!--Body End-->

<script language=javascript>
$('#magazine').chosen({
    disable_search_threshold: 10, width: "30%"
});
	function ConfirmDelete(id){
		if(id==1){
			alert('<? echo $lang['common']['sorryadmindelete'] ?>');
	    }else if(confirm('<? echo $lang['common']['confirmdeleteuser'] ?>')){
			window.location='index.php?component=common&page=wce.user.php&pageaction=delete&user_id='+id;
		}
	}

	$(function() {
		$('#partner').change(function() {
		    if($(this).is(':checked')) {
		        $('#choose-magazine').show();
		        $('#magazine').removeAttr('disabled').trigger('chosen:updated');
		        // $('#magazine_chosen').removeClass('chosen-disabled');
		    }
		    else {
		        $('#choose-magazine').hide();
		        $('#magazine').attr('disabled', 'disabled').trigger('chosen:updated');		
		        // $('#magazine_chosen').addClass('chosen-disabled');    
		    }
		});
	})
</script>

