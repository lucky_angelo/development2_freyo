<?
session_start();

include("../../../config.php");
?>
<?

if (isset($_GET['page'])) {
	$oSystem->setValue("sys_socialmedia_fb_pg", $_GET['page']);
	header("Location: ".$path['webroot']."/index.php?component=common&page=wce.info.php");
}

$f1 = new Facebook_class();
$f1->loadJsSDK($path_to_library);

$fb_cookie = $f1->getCookie();
if($fb_cookie!='') {
	$oSystem->setValue("sys_socialmedia_fb", $f1->cookie["user_id"]);
	$oSystem->setValue("sys_socialmedia_fb_at", $f1->cookie["access_token"]);
	
	$graph_url = 'https://graph.facebook.com/'.$f1->cookie["user_id"].'/accounts?access_token='.$f1->cookie["access_token"];
	$accounts = json_decode(file_get_contents($graph_url));
	
	//header("Location: ".$path['webroot']."/index.php?component=common&page=wce.info.php");
}
?>
<html>
<head>
	<script type="text/javascript" src="<?=$path['webroot']?>/common/scripts/jquery-1.8.2.js"></script>
    <? if($accounts=='') { ?>
	<script type="text/javascript">
		fbActionConnect();
	</script>
    <? } ?>
</head>

<body>
Select Page:
<?
	if ($accounts->data) {
		foreach($accounts->data as $account) {
	?><li><a href="connect.php?page=<?=$account->id?>"><?=$account->name?></a></li><?
		}
	}
?>
</body>
</html>