<?

if($pageaction=="add"){

	$thumbnailname = strtolower(str_replace(" ","_",trim($_FILES['thumbnail']['name'])));
	$uniqueid = md5(uniqid(rand(), true));	
	$uniqueid=substr($uniqueid,0,5);
	
	if($thumbnailname!=""){ 
		$thumbnailname=$uniqueid."_".$thumbnailname; 
		if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			$oNews->uploadFile($_FILES['thumbnail']['tmp_name'],$path["docroot"]."_files/newspublish",$thumbnailname);
		}
	}

	if ($datepost==""){ $datepost="0000-00-00"; }
	if ($dateexpire==""){ $dateexpire="0000-00-00"; }
	$content = addslashes($FCKeditor);
	$oNews->data = array("category_id","newstype","title","datepost","dateexpire","display","summary","content","thumbnail","exlink_url","seq");
	$oNews->value = array($category_id,$newstype,addslashes($title),$datepost,$dateexpire,$display,addslashes($summary),$content,$thumbnailname,addslashes($exlink_url),trim($seq));
	$oNews->add();
	
	//create rss feed
	if ($oSystem->getValue("news_rss_enable")=="Yes"){
		if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			include($path["docroot"]."common/class.rssfeed.php");
		}
		
		$mynewsurl = $oSystem->getValue("news_pageurl");
		$rsstitle = $oSystem->getValue("news_rss_title");
		$rssdescription = $oSystem->getValue("news_rss_description");
		$rsslanguage = $oSystem->getValue("news_rss_language");
		$rsscopyright = $oSystem->getValue("news_rss_copyright");
		$rsseditor = $oSystem->getValue("news_rss_editor");
		$rssimage = $oSystem->getValue("news_rss_logo");
		$rsslive = $oSystem->getValue("news_rss_live");
		$rssname = "news.xml";
		$rsstitleurl = $mynewsurl;
		
		$oNews->data = array("category_id");
		$oNews->where="display='Yes' and DATE_FORMAT(curdate(), '%Y%m%d')-DATE_FORMAT(datepost, '%Y%m%d')<$rsslive and dateexpire>curdate() and newstype='news' group by category_id"; 
		$oNews->order="category_id asc, datepost desc";
	   	$groupresult=$oNews->getList();		
		//while group by
		while($grouprow=mysql_fetch_row($groupresult)){
			$rsstitleurl = $mynewsurl;
			$rsstitle = $oSystem->getValue("news_rss_title");
			if ($grouprow[0] != 0){
				
				$oNews_Category->data = array("name","descp","parent_id");
				$oNews_Category->where="category_id='".$grouprow[0]."'";
			   	$groupnameresult=$oNews_Category->getList();
			   	if (mysql_num_rows($groupnameresult)>0){
					$groupnamerow=mysql_fetch_row($groupnameresult);
				   	$rsstitle = $rsstitle.": ".$groupnamerow[0];
				   	$rsstitleurl = "$mynewsurl?category_id=$grouprow[0]&parent_id=$groupnamerow[2]";
					$rssdescription = $groupnamerow[1];
					$rssname = "news_".str_replace(" ", "_", $groupnamerow[0]).".xml";
					$nogroup = false;
				}else{ $nogroup = true; }
				mysql_free_result($groupnameresult);
			}else{ $nogroup = false; }
			if (!$nogroup){
				$oRssFeed = new RssFeed();
				$oRssFeed->feedtitle($rsstitle, $rsstitleurl, $rssdescription, $rsslanguage, $rsscopyright, $rsseditor);
				if ($rssimage != ""){ $oRssFeed->feedimage($rsstitle, $path['webroot']."_files/rss/news/".$rssimage, $mynewsurl, $rssdescription); }		
				$oNews->data = array("news_id","title","summary","datepost");
				$oNews->where="display='Yes' and DATE_FORMAT(curdate(), '%Y%m%d')-DATE_FORMAT(datepost, '%Y%m%d')<$rsslive and dateexpire>curdate() and newstype='news' and category_id=$grouprow[0]"; 
				$oNews->order="datepost desc";
			   	$result=$oNews->getList();
			   	
			   	while($myrow=mysql_fetch_row($result)){
				   	$oRssFeed->feeditem(stripslashes($myrow[1]), $mynewsurl."?news_id=".$myrow[0], stripslashes($myrow[2]), $myrow[3]);
			   	}
			   	mysql_free_result($result);
			   	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			   		$oRssFeed->createFeed($path["docroot"]."_files/rss/news/",$rssname);
		   		}
		   	}
	   	}
	   	//end while
	   	mysql_free_result($groupresult);
	   	
	}//rss end
	
	$status_message = "<b>". $lang['newspublish']['status'] ." : </b>" . $lang['newspublish']['statusadd'] . "<br>";
}

$oNews->data = array("max(seq)");
$rst = $oNews->getList();
$myr = @mysql_fetch_row($rst);	$lastseq=  $myr[0] + 1;	mysql_free_result($rst);

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newspublish']['newspublisher'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newspublish&page=wce.addnews.php" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="add">

<tr><td width=20%><? echo $lang['newspublish']['choosetype'] ?></td><td valign=top><input type=radio name=newstype value="announce" onclick="hideContent()">&nbsp;<? echo $lang['newspublish']['announcement'] ?></td><td colspan=2><input type=radio name=newstype value="news" onclick="showContent()" checked >&nbsp;<? echo $lang['newspublish']['newsarticle'] ?></td></tr>
<tr><td>&nbsp;</td><td colspan=3><input type=radio name=newstype value="external" onclick="hideContent()">&nbsp;<? echo $lang['newspublish']['externallink'] ?>&nbsp;&nbsp;<input type=text name=exlink_url style="width:300px" value="http://"></td></tr>
<tr><td><? echo $lang['newspublish']['category'] ?></td><td colspan=3 valign=top><select name=category_id><option value=""></option><?	$oNews_Category->getCategoryTree(0); ?></select></td></tr>
<tr><td><? echo $lang['newspublish']['datepost'] ?></td><td><input type=text id=datepost name=datepost style="width:95px" value="<? echo date("Y-m-d") ?>">&nbsp; <a href="javascript:show_calendar('datepost', document.thisform.datepost.value);"><img src="common/datepick/calendar.gif" border="0" align=absmiddle></a></td>
	<td><? echo $lang['newspublish']['dateexpire'] ?></td><td><input type=text id=dateexpire name=dateexpire style="width:95px">&nbsp; <a href="javascript:show_calendar('dateexpire', document.thisform.dateexpire.value);"><img src="common/datepick/calendar.gif" border="0" align=absmiddle></a></td></tr>
<tr><td><? echo $lang['newspublish']['title'] ?></td><td colspan=3><input type=text name=title style="width:400px"> *</td></tr>
<tr><td><? echo $lang['newspublish']['thumbnail'] ?></td><td colspan=3><input type=file name=thumbnail style="width:230px"><? echo $lang['newspublish']['idealimagesize'] ?></td></tr>
<tr><td><? echo $lang['newspublish']['display'] ?></td><td valign=middle><input type=radio name=display value="Yes" checked>&nbsp;&nbsp;<? echo $lang['newspublish']['yes'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=radio name=display value="No">&nbsp;&nbsp;<? echo $lang['newspublish']['no'] ?></td><td><?= $lang['newspublish']['sequence'] ?></td><td valign=top><input type=text name=seq value="<?= $lastseq; ?>" style="width:40px;"></td></tr>
<tr><td colspan=4><? echo $lang['newspublish']['summary'] ?></td></tr>
<tr><td colspan=4>
<table border=0 width=100% cellpadding=0 cellspacing=0><tr><td>
	<?
	$oFCKeditor = new FCKeditor('summary') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '250' ;
	$oFCKeditor->Create() ;
	?>
</td></tr></table><br>
</td></tr>
<tr><td colspan=4>
	<table border=0 id="newscontent" width=100% cellpadding=0 cellspacing=0>
	<? 
	echo "<tr><td>".$lang['newspublish']['newscontent']."</td><td width=80%>&nbsp;<br></td></tr><tr><td colspan=2>";
	
	$oFCKeditor = new FCKeditor('FCKeditor') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '450' ;
	$oFCKeditor->Create() ;
	?>
	<br></td></tr></table>
</td></tr>
<tr><td colspan=4><input type=button value="<? echo $lang['newspublish']['add'] ?>" onclick="validate()"></td></tr>
</form></table>
<br>

<iframe width=163 height=187 id="iCal" name="iCal" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<script language="JavaScript" src="common/datepick/datepick.js"></script>
<script language=javascript>
	function validate(){
		if(document.thisform.title.value==""){
			alert('<? echo $lang['newspublish']['plstitle'] ?>'); document.thisform.title.select();  return false;
		}else{
			document.thisform.submit();
		}	
	}
	
	function showContent(){
		document.getElementById("newscontent").style.display = 'Table';	
	}
	
	function hideContent(){
		document.getElementById("newscontent").style.display = 'None';
	}
	
	//hideContent();
	showContent();
	
</script>	