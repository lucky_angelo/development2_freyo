<?
echo "";
	$thisfile=$oSystem->getValue("news_pageurl");
	$news_seq=$oSystem->getValue("news_seq");
	$monthArr = array($lang['newspublish']['january'], $lang['newspublish']['february'], $lang['newspublish']['march'], $lang['newspublish']['april'], $lang['newspublish']['may'], $lang['newspublish']['june'], $lang['newspublish']['july'], $lang['newspublish']['august'], $lang['newspublish']['september'], $lang['newspublish']['october'], $lang['newspublish']['november'], $lang['newspublish']['december']);
	$sefurl = $oSystem->getValue("news_sefurl");
	
	$localtz = $oSystem->getValue("sys_timezone");
	$localtime = $localtz==""?time():getLocalTime($localtz);
	
	$year = date("Y",$localtime); 
	$month = date("m",$localtime); 
	$day = date("d",$localtime); 
	$currentdate = $year."-".$month."-".$day;

	if ($oSystem->getValue("news_dateformat")=="m-d-Y"){	
		$dateformat="%m-%d-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="d-m-Y"){	
		$dateformat="%d-%m-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="dbY"){	
		$dateformat="%d %b %Y";
	} else if ($oSystem->getValue("news_dateformat")=="bdY"){	
		$dateformat="%b %d, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="dMY"){	
		$dateformat="%d %M, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="MdY"){	
		$dateformat="%M %d, %Y";
	} else {
		$dateformat="%Y-%m-%d";
	}
/***** News Listing *****/
if($news_id=="" && $pageaction==""){	
	$listno = $oSystem->getValue("news_newsno");
	
	echo"<table border=0 cellpadding=0 cellspacing=0 width=100% valign=\"top\" align=center><tr><td valign=\"top\">";	
	if($parent_id!="0"){	
		$subcat_list=$oSystem->getValue("news_subcatno");
		$val_width=100/$subcat_list."%";
		$countline=0;
		$oNews_Category->data=array("category_id","name","descp");
		$resultsub=$oNews_Category->getSubCategory($category_id);
		if(mysql_num_rows($resultsub)!=0){
			$toptitle.="<table border=0 cellpadding=2 cellspacing=2 width=100% valign=\"top\"><tr>";
			while($myrowsub=mysql_fetch_row($resultsub)){
				$myrowsub[1]=stripslashes($myrowsub[1]);	
				if ($oSystem->getValue("news_showcatdescp")=="Yes" && !empty($myrowsub[2])){ $myrowsub[2]="<br><font style=\"font-size:".$oSystem->getValue("news_textsmall")."px\">".stripslashes(nl2br($myrowsub[2]))."</font>"; } else { $myrowsub[2]=""; }				
				if ($sefurl == "Yes"){
					$toptitle.="<td valign=top width=\"$val_width\">&bull; <a href=\"cat-$myrowsub[0]-$myrowsub[0].html\" class=tnews>$myrowsub[1]&nbsp;</a>$myrowsub[2]</font></td>";
				}else{
					$toptitle.="<td valign=top width=\"$val_width\">&bull; <a href=\"".$thisfile."?category_id=$myrowsub[0]&parent_id=$myrowsub[0]\" class=tnews>$myrowsub[1]&nbsp;</a>$myrowsub[2]</td>";
				}
				$countline++; if($countline>$subcat_list-1){ $toptitle.="</tr><tr>"; $countline=0; }
			}mysql_free_result($resultsub);
			$toptitle.="</tr></table>";	
		}//end if
	}	
	
	$oNews->data = array("news_id","title", "summary", "date_format(datepost,' %d, %Y')", "date_format(datepost,'%m')","thumbnail","date_format(datepost,'$dateformat')","newstype","exlink_url","category_id","datepost");
	if($category_id!="" && $category_id!=0){
		$oNews->where = "(newstype='news' OR newstype='external') AND display='Yes' AND (dateexpire='0000-00-00' OR dateexpire>date_format('$currentdate', '%Y-%m-%d')) AND datepost<=date_format('$currentdate', '%Y-%m-%d') AND category_id='$category_id'";
		$toptitle=$toptitle;
	}else if($arcyear!="" && $arcmonth!=""){
		$oNews->where = "(newstype='news' OR newstype='external') AND display='Yes' AND date_format(datepost,'%Y')=$arcyear AND date_format(datepost,'%c')=$arcmonth AND datepost<=date_format('$currentdate', '%Y-%m-%d')";
		$news_month = date("m",mktime(0,0,0,$arcmonth,1,$arcyear))-1;
		$toptitle ="<b>".$lang['newspublish']['titlearchive']." - ".$monthArr[$news_month].date(" Y",mktime(0,0,0,$arcmonth,1,$arcyear))."</b>";	
	}else{
		$oNews->where = "(newstype='news' OR newstype='external') AND display='Yes' AND (dateexpire='0000-00-00' OR dateexpire>date_format('$currentdate', '%Y-%m-%d')) AND datepost<=date_format('$currentdate', '%Y-%m-%d') ";
		$toptitle ="<b>".$lang['newspublish']['titlelatest']."</b>";
	}
	
	$oNews->order = "seq $news_seq, datepost desc";
	$result=$oNews->getList();
	if(mysql_num_rows($result)!=0){	$total=mysql_num_rows($result); }else{ $total=0; }	
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$listno; $next=$start+$listno;	$from=$start+1; $to=$listno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }
	
	if($prev>=0){ 
		if ($sefurl == "Yes"){
			$prevlink="<a href=\"navigate-$prev-$category_id-$parent_id-$arcyear-$arcmonth.html\" class=tnews><b>".$lang['newspublish']['prev']."</b></a>"; 
		}else{
			$prevlink="<a href=\"".$thisfile."?start=$prev&category_id=$category_id&parent_id=$parent_id&arcyear=$arcyear&arcmonth=$arcmonth\" class=tnews><b>".$lang['newspublish']['prev']."</b></a>"; 
		}	
	}else{ $prevlink=""; }
	if($next<$total){ 
		if ($sefurl == "Yes"){
			$nextlink="<a href=\"navigate-$next-$category_id-$parent_id-$arcyear-$arcmonth.html\" class=tnews><b>".$lang['newspublish']['next']."</b></a>"; 
		}else{
			$nextlink="<a href=\"".$thisfile."?start=$next&category_id=$category_id&parent_id=$parent_id&arcyear=$arcyear&arcmonth=$arcmonth\" class=tnews><b>".$lang['newspublish']['next']."</b></a>"; 
		}		
	}else{ $nextlink=""; }
	if($prevlink!="" && $nextlink!=""){	$navline="&nbsp;&nbsp;";	}
	$oNews_Category->getCategoryTreeLink($category_id);
	echo "
		<table border=0 cellpadding=0 cellspacing=0 width=100% valign=\"top\" align=center>".(!$CategoryTreeLink==""?"<tr><td colspan=2 valign=top><b>$CategoryTreeLink</b></td></tr>":"")."
			<tr><td colspan=2 valign=top></td></tr>
		".(!(empty($prevlink) && empty($navline) && empty($nextlink))?"<tr><td></td><td align=right width=25%>$prevlink $navline $nextlink&nbsp;</td></tr>":"")."
		</table><br>
		
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
	";
		
	$oNews->order = "seq $news_seq, datepost desc limit $start, $listno";
	$result = $oNews->getList();
	if (mysql_num_rows($result)>0){	
		while($myrow=mysql_fetch_row($result)){
			$myrow[1] = stripslashes($myrow[1]);
			if ($oSystem->getValue("news_showsummary")=="Yes" && !empty($myrow[2])){ $myrow[2] = "<div>".stripslashes($myrow[2])."</div>"; } else { $myrow[2] =""; }
			if ($oSystem->getValue("news_showpubdate")=="Yes" && (!(empty($myrow[10]) || $myrow[10]=="0000-00-00"))){ $posted = "<div  class=news-date>$myrow[6]<br></div>"; } else { $posted =""; }
			$thumbnail="";
			if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
				if(file_exists($path["docroot"]."_files/newspublish/$myrow[5]") && $myrow[5]!=""){
					$thumbnail = ($myrow[5]!=""?"<td valign=top width=10%><img src=\"".$path["webroot"]."_files/newspublish/$myrow[5]\" align=left></td>":"");
				}
			}
			echo "<tr>".($thumbnail!=""?$thumbnail."<td valign=top>":"<td valign=top colspan=2>")."<div class=txt-orange>";
			if ($myrow[7]=="external"){
				$exlink_url = stripslashes($myrow[8]);
				echo"<a href=\"$exlink_url\" target=_blank class=tnews>$myrow[1]</a>";
			} else {
				if ($oSystem->getValue("news_disstyle")=="Same"){
					echo $sefurl=="Yes"?"<a href=\"news-$myrow[0]-$start-$myrow[9]-$myrow[9]-$arcyear-$arcmonth.html\" class=titlenews1>$myrow[1]</a>":"<a href=\"".$thisfile. "?news_id=$myrow[0]&start=$start&category_id=$myrow[9]&parent_id=$myrow[9]&arcyear=$arcyear&arcmonth=$arcmonth\" class=titlenews1>$myrow[1]</a>";
				} else {
					echo "<a href=javascript:newsdetails(\"$myrow[0]\"); class=tnews>$myrow[1]</a>";
				}
			}
			echo "
				</div><span class=newsbody>$posted $myrow[2]</span></td></tr><tr><td><br></td></tr>
			";
		}
	}else{
		echo "<tr><td align=center><br><br>".$lang['newspublish']['nonewsfound']."<br><br></td></tr>";
	}
	echo "</table><br></td></tr></table>";
}
	
/***** News Details *****/
if($news_id!="" && $pageaction==""){	

	echo"<table border=0 cellpadding=0 cellspacing=0 width=100% valign=\"top\" align=center>";
	
	if ($arcyear!="" && $arcmonth!=""){
		if ($sefurl == "Yes"){
			echo"<tr><td><b><a href=\"arc-$arcyear-$arcmonth.html\" class=tnews>".$lang['newspublish']['backtoarchives']."</a></b><br><br></td></tr>";
		}else{
			echo"<tr><td><b><a href=\"".$thisfile."?arcyear=$arcyear&arcmonth=$arcmonth\" class=tnews>".$lang['newspublish']['backtoarchives']."</a></b><br><br></td></tr>";
		}
	}
	
	if ($pageaction2!="" && $searchby!=""){
		if ($sefurl == "Yes"){
			echo"<tr><td><b><a href=\"search-$pageaction2-$start-$searchby-$searchkey-$datesearch.html\" class=tnews>".$lang['newspublish']['backtosearchlist']."</a></b><br><br></td></tr>";
		}else{
			echo"<tr><td><b><a href=\"".$thisfile."?pageaction=$pageaction2&start=$start&searchby=$searchby&searchkey=$searchkey&datesearch=$datesearch\" class=tnews>".$lang['newspublish']['backtosearchlist']."</a></b><br><br></td></tr>";
		}
	}
	
	echo"<tr><td valign=\"top\">";	
	/*****Navigation Link Calculation*****/
	
	$oNews->data=array("news_id");
	$oNews->data = array("news_id","date_format(datepost,'%M %d, %Y')", "title", "summary", "content");
	if($category_id!="" && $category_id!=0){
		$oNews->where = "newstype='news' and display='Yes' and (dateexpire='0000-00-00' or dateexpire>date_format('$currentdate', '%Y-%m-%d')) and datepost<=date_format('$currentdate', '%Y-%m-%d') and category_id='$category_id'";
	}else if($arcyear!="" && $arcmonth!=""){
		$oNews->where = "display='Yes' and newstype='news' and date_format(datepost,'%Y')=$arcyear and date_format(datepost,'%c')=$arcmonth and datepost<=date_format('$currentdate', '%Y-%m-%d')";
	}else{
		$oNews->where = "display='Yes' and newstype='news' and (dateexpire='0000-00-00' or dateexpire>date_format('$currentdate', '%Y-%m-%d')) and datepost<=date_format('$currentdate', '%Y-%m-%d')";
	}
	$oNews->order = "seq $news_seq, datepost desc";	$result=$oNews->getList();
	$count=0; while($myrow=mysql_fetch_row($result)){ if($myrow[0]==$news_id){ $curpos=$count; }	$count++; }		
	$prevpos=$curpos-1; $nextpos=$curpos+1;	$curpos=$curpos+1;	$total=mysql_num_rows($result);
	if($prevpos>=0){ mysql_data_seek($result,$prevpos);	if($myrow=mysql_fetch_row($result)){ $previd=$myrow[0]; }}
	if($nextpos<$total){ mysql_data_seek($result,$nextpos); if($myrow=mysql_fetch_row($result)){ $nextid=$myrow[0]; }}		
	mysql_free_result($result);
	
	/*****Begin Previous and Next Navigation Link. Modify To Match Your Display*****/	
	if($previd!=""){ 
		if ($sefurl == "Yes"){
			$prevlink="<a href=\"news-$previd-$start-$category_id-$parent_id-$arcyear-$arcmonth.html\" class=tnews><b>".$lang['newspublish']['prev']."</b></a>";
		}else{
			$prevlink="<a href=\"".$thisfile."?news_id=$previd&start=$start&category_id=$category_id&parent_id=$parent_id&arcyear=$arcyear&arcmonth=$arcmonth\" class=tnews><b>".$lang['newspublish']['prev']."</b></a>";
		}
	}
	if($nextid!=""){ 
		if ($sefurl == "Yes"){
			$nextlink="<a href=\"news-$nextid-$start-$category_id-$parent_id-$arcyear-$arcmonth.html\" class=tnews><b>".$lang['newspublish']['next']."</b></a>";
		}else{
			$nextlink="<a href=\"".$thisfile."?news_id=$nextid&start=$start&category_id=$category_id&parent_id=$parent_id&arcyear=$arcyear&arcmonth=$arcmonth\" class=tnews><b>".$lang['newspublish']['next']."</b></a>";
		}
	}
	if($prevlink!="" && $nextlink!=""){	$navline="&nbsp;&nbsp;";	}


	/*****Get News Details*****/
	$oNews->data = array("title", "content", "date_format(datepost,' %d, %Y')", "date_format(datepost,'%m')","date_format(datepost,'$dateformat')","summary","datepost");
	$result = $oNews->getDetail($news_id);
	if($myrow=mysql_fetch_row($result)){
		$title = stripslashes($myrow[0]);
		$content = stripslashes($myrow[1]);
		if ($oSystem->getValue("news_showpubdate")=="Yes" && (!(empty($myrow[6]) || $myrow[6]=="0000-00-00"))){ $posted = $lang['newspublish']['postedon']."&nbsp;$myrow[4]"; } else { $posted ="&nbsp;"; }
		$sum = stripslashes($myrow[5]);
	}mysql_free_result($result);

	/*****Category Tree*****/
	$oNews_Category->getCategoryTreeLink($category_id);	
	echo "
	<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
	<tr><td colspan=2>".($CategoryTreeLink!=""?"<b>".$CategoryTreeLink."</b>":"")."</td></tr>";
		
	echo "".(!(empty($prevlink) && empty($navline) && empty($nextlink))?"<tr><td>&nbsp;<td align=center></td><td align=right width=25%>$prevlink $navline $nextlink</td></tr>":"")."</table>
	
	<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
	<tr><td colspan=2 class=titlenews>$title <br><br></td></tr>
	<tr><td  class=news-date>$posted</td><td align=right  class=news-date>";
	if ($oSystem->getValue("news_showemail")=="Yes"){
		echo "<a href=javascript:emailthis(\"$news_id\"); class=tnews>".$lang['newspublish']['emailthis']."</a>";
	}else{ echo "&nbsp;";}
	if ($oSystem->getValue("news_showprint")=="Yes"){
		echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=javascript:printthis(\"$news_id\"); class=tnews>".$lang['newspublish']['printthis']."</a>";
	}else{ echo "&nbsp;";}
	
	echo "
	</td></tr>
	<tr><td colspan=2>".($oSystem->getValue("news_showsummaryart")=="Yes"?"<br>$sum<br>":"")."<br>$content</td></tr>	
	</table><br>
	";
	echo "</td></tr></table>";
}

if ($pageaction=="searchnews"){
	$entry1 = $searchkey; $entry2 = $datesearch;
	$searchresultkey = array();
	$searchkey = str_replace("+"," ",$searchkey);
	
	$vsearchkey=addslashes($searchkey);
	$vsearchkey=addslashes($vsearchkey);
	
	if ($searchby=="bykey" && $searchkey!=""){
		$array_stopword = array("a","an","are","you","and","because","begin","bf","both","but","can","do");
		$special = array("~","!","@","#","$","%","^","&","*","(",")",",","�","-","+","{","[","}","]",":",".","<",">","|","/","?","'");
	
		if (!in_array($vsearchkey,$special)){
			if (!in_array($vsearchkey,$array_stopword)){
			$oNews->data = array("news_id");	
			$oNews->order = "";
			$oNews->where="(newstype='news' OR newstype='external') AND title like '%$vsearchkey%'"; 
			$result=$oNews->getList();
			while($myrow=mysql_fetch_row($result)){	if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }	}
			mysql_free_result($result);	
			}
		}
		
		$searchkey = preg_replace('/\s+/',' ',$searchkey);
		$searchkey=trim($searchkey);
		$searchresultkeylist = explode(" ",$searchkey);	
		for($k=0;$k<count($searchresultkeylist);$k++){	
			if (!in_array($searchresultkeylist[$k],$special)){
				if (!in_array($searchresultkeylist[$k],$array_stopword)){
					$vsearchkey=addslashes($searchresultkeylist[$k]);
					$vsearchkey=addslashes($vsearchkey);
					$oNews->where="(newstype='news' OR newstype='external') AND title like '%$vsearchkey%'";
					$result=$oNews->getList();
					while($myrow=mysql_fetch_row($result)){	if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }}
					mysql_free_result($result);			
				}
			}
		}
	} else {
		$oNews->data = array("news_id");
		$dt = explode("-",$datesearch);	$datesearch = $dt[2]."-".$dt[1]."-".$dt[0];
		$oNews->where="(newstype='news' OR newstype='external') AND datepost='$datesearch'"; 
		$oNews->order = "title";
		$result=$oNews->getList();
		while($myrow=mysql_fetch_row($result)){	if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }	}
		mysql_free_result($result);	
	}
	
	if ($searchby==""){$searchby="bykey";}
	echo" ";		//news

	$listno= $oSystem->getValue("news_newsno");
	$total= count($searchresultkey);
	
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$listno; $next=$start+$listno;	$from=$start+1; $to=$listno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }
	
	$searchkey1 = stripslashes(htmlentities(str_replace(" ","+",$searchkey)));
	
	if($prev>=0){ 
		if ($sefurl=="Yes"){
			$prevlink="<a href=\"search-$pageaction-$prev-$searchby-$searchkey1-$datesearch\"><b>".$lang['newspublish']['prev']."</b></a>";
		} else {
			$prevlink="<a href=\"".$thisfile."?pageaction=$pageaction&start=$prev&searchby=$searchby&searchkey=$searchkey1&datesearch=$datesearch\"><b>".$lang['newspublish']['prev']."</b></a>";	
		}
	} else { $prevlink=""; }
	if($next<$total){ 
		if ($sefurl=="Yes"){
			$nextlink="<a href=\"search-$pageaction-$next-$searchby-$searchkey1-$datesearch\"><b>".$lang['newspublish']['next']."</b></a>";
		} else {
			$nextlink="<a href=\"".$thisfile."?pageaction=$pageaction&start=$next&searchby=$searchby&searchkey=$searchkey1&datesearch=$datesearch\"><b>".$lang['newspublish']['next']."</b></a>";
		}
	}  else { $nextlink=""; }
	
	if($prevlink!="" && $nextlink!=""){ $navline="&nbsp;|&nbsp;"; }

	
	echo "<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>";
	
	if ($searchby=="bykey"){
		$lblsearchkey=stripslashes(htmlentities($searchkey));
		echo"<tr><td valign=top colspan=3>".$lang['newspublish']['searchbykey']."&nbsp;:&nbsp;".$lang['newspublish']['searchfound']."'<b>$lblsearchkey</b>'<br><br></td></tr>";
	} else {
		$lblsearchkey=stripslashes($datesearch);
		echo"<tr><td valign=top colspan=3>".$lang['newspublish']['searchbydate']."&nbsp;:&nbsp;".$lang['newspublish']['searchfound']."'<b>$lblsearchkey</b>'<br><br></td></tr>";
	}
		
	echo"
		<tr>
			<td width=20%>&nbsp;</td><td valign=top align=center>".$lang['newspublish']['showing']." ".$from."-".$to." ".$lang['newspublish']['of']." ".$total."</td>
			<td align=right width=20% valign=top>&nbsp;$prevlink $navline $nextlink&nbsp;</td>
		</tr>
		</table>
		<hr size=1 color=#B6B6B6 width=100%>
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
	";
		
	if(count($searchresultkey)!=0){
		echo "
			<tr><td valign=top>
		";
		for($k=0;$k<count($searchresultkey);$k++){
				$oNews->data = array("news_id","title", "summary", "date_format(datepost,' %d, %Y')", "date_format(datepost,'%m')","thumbnail","date_format(datepost,'$dateformat')","newstype","exlink_url","category_id","datepost");
				$result=$oNews->getDetail($searchresultkey[$k]);
				if($myrow=mysql_fetch_row($result)){	
								
					if($k >= $start && $k < $start+$listno){
						$myrow[1] = stripslashes($myrow[1]);
						if ($oSystem->getValue("news_showsummary")=="Yes" && !empty($myrow[2])){ $myrow[2] = "<div>".stripslashes($myrow[2])."</div>"; } else { $myrow[2] =""; }
			
						if ($oSystem->getValue("news_showpubdate")=="Yes" && (!(empty($myrow[10]) || $myrow[10]=="0000-00-00"))){ $posted = "<div><span class=news-date>$myrow[6]</span><br><br><div>"; } else { $posted =""; }
						$thumbnail = ($myrow[5]!=""?"<td valign=top width=10%><img src=\"".$path["webroot"]."_files/newspublish/$myrow[5]\" align=left></td>":"");
						
						echo "
							<table border=0 cellpadding=3 cellspacing=0 width=100%>
							<tr>".($thumbnail!=""?$thumbnail."<td valign=top>":"<td valign=top colspan=2>")."<div class=txt-orange>";
						if ($myrow[7]=="external"){
							$exlink_url = stripslashes($myrow[8]);
							echo"<a <a href=\"$exlink_url\" target=_blank class=tnews>$myrow[1]</a>";
						} else {
							if ($oSystem->getValue("news_disstyle")=="Same"){
								echo $sefurl=="Yes"?"<a href=\"searchnews-$myrow[0]-$start-$myrow[9]-$myrow[9]-searchnews-$searchby-$searchkey1-$datesearch.html\" class=tnews>$myrow[1]</a>":"<a href=\"".$thisfile. "?news_id=$myrow[0]&start=$start&category_id=$myrow[9]&parent_id=$myrow[9]&pageaction2=$pageaction&searchby=$searchby&searchkey=$searchkey1&datesearch=$datesearch\" class=tnews>$myrow[1]</a>";
							} else {
								echo "<a href=javascript:newsdetails(\"$myrow[0]\"); class=tnews>$myrow[1]</a>";
							}
						}
						echo "
							</div>$posted $myrow[2]</td></tr>
							</table><br>
						";
					}
				} mysql_free_result($result);	
		}//end for
		echo"<br></td></tr>";
	}else{	
		echo "<tr><td><br><center><font color=\"#ff000\">".$lang['newspublish']['hnofound']."</font></center></td></tr>";	
	}
	echo "</table><br>
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center><tr>
			<td valign=top>&nbsp;</td>
			<td align=right width=20% valign=top>&nbsp;$prevlink $navline $nextlink&nbsp;</td>
		</tr></table><br>
	";
}

?>

<script language=javascript>
function printthis(id){
	window.open("<? echo $path["webroot"] ?>newspublish/home.print.php?news_id="+id,"Popup","width=600,height=500,top=70,left=100,dependent=yes,titlebar=no,scrollbars=yes,resizable=yes");	
}

function emailthis(id){
	window.open("<? echo $path["webroot"] ?>newspublish/home.tell.php?news_id="+id,"Popup","width=420,height=380,top=170,left=250,dependent=yes,titlebar=no,scrollbars=yes");	
}

function newsdetails(id){
	window.open("<? echo $path["webroot"] ?>newspublish/home.viewdetails.php?news_id="+id,"Popup","width=800,height=600,top=50,left=100,dependent=yes,titlebar=no,scrollbars=yes,resizable=yes");	
}
</script>