<?
$lang['newspublish']['menutitle'] 				= "News Publisher";
$lang['newspublish']['componentdescp']			= "Post your latest news article and announcements. Set posted and expiry date for your news, HTML editor, Tell-A-Friend and printer version.";

/***** Top Links *****/
$lang['newspublish']['newspublisher'] 			= "News Publisher";
$lang['newspublish']['addnews'] 				= "Add News";
$lang['newspublish']['listnews'] 				= "List News";
$lang['newspublish']['category'] 				= "Category";
$lang['newspublish']['subscriberss']			= "Subscribe RSS";
$lang['newspublish']['preference'] 				= "Preference";
$lang['newspublish']['help'] 					= "Help";


/***** Add News *****/
$lang['newspublish']['status'] 					= "Status";
$lang['newspublish']['statusadd'] 				= "News added. Click on List News to view.";
$lang['newspublish']['datepost'] 				= "Publish Date";
$lang['newspublish']['dateexpire'] 				= "Date Expired";
$lang['newspublish']['choosetype'] 				= "Choose Type";
$lang['newspublish']['newsarticle'] 			= "News Article";
$lang['newspublish']['announcement'] 			= "Announcement";
$lang['newspublish']['externallink']			= "External Link";
$lang['newspublish']['display'] 				= "Display";
$lang['newspublish']['title'] 					= "Title";
$lang['newspublish']['summary'] 				= "Summary";
$lang['newspublish']['newscontent'] 			= "News Article Content";
$lang['newspublish']['add'] 					= "  Add  ";
$lang['newspublish']['yes'] 					= "Yes";
$lang['newspublish']['no'] 						= "No";
$lang['newspublish']['sequence'] 				= "Sequence";
$lang['newspublish']['plstitle']				= "Please enter your News Title.";
$lang['newspublish']['idealimagesize']			= "Image Size: 214px x 139px";


/***** List News *****/
$lang['newspublish']['statusdelete'] 			= "Selected news deleted.";
$lang['newspublish']['newstype'] 				= "News Type";
$lang['newspublish']['searchtitle'] 			= "Search Title";
$lang['newspublish']['search'] 					= " Search ";
$lang['newspublish']['deleteselected'] 			= "Delete Selected";
$lang['newspublish']['sequpdated'] 				= "Sequence Updated";
$lang['newspublish']['updseq'] 					= "Update Sequence";
$lang['newspublish']['showing'] 				= "Showing";
$lang['newspublish']['to'] 						= "To";
$lang['newspublish']['type']					= "Type";
$lang['newspublish']['note']					= "Type Indicators";
$lang['newspublish']['newstitle'] 				= "News Title";
$lang['newspublish']['posted'] 					= "Published";
$lang['newspublish']['expired'] 				= "Expired";
$lang['newspublish']['seq'] 					= "Seq";
$lang['newspublish']['confirmdelete'] 			= "Are you sure you want to delete the selected record(s) ?";
$lang['newspublish']['sortasc']					= "Sort In Ascending Order";
$lang['newspublish']['sortdesc']				= "Sort In Descending Order";
$lang['newspublish']['editthisrecord']			= "Edit This Record";


/***** Edit News *****/
$lang['newspublish']['statusupdate'] 			= "News details updated.";
$lang['newspublish']['prev'] 					= "Prev";
$lang['newspublish']['next'] 					= "Next";
$lang['newspublish']['backtolist'] 				= "Back to List";
$lang['newspublish']['number'] 					= "No.";
$lang['newspublish']['of'] 						= "Of";
$lang['newspublish']['update'] 					= "Update";
$lang['newspublish']['delete']			 		= "Delete";
$lang['newspublish']['back'] 					= " Back ";
$lang['newspublish']['confirmdeletethis']		= "Are you sure you want to delete this record ?";
$lang['newspublish']['thumbnail']	 			= "Thumbnail";
$lang['newspublish']['viewimage']				= "View";
$lang['newspublish']['delimage']				= "Delete";


/***** Category *****/
$lang['newspublish']['catadd'] 					= "New category added.";
$lang['newspublish']['catedit'] 				= "Edit category details.";
$lang['newspublish']['catupdate'] 				= "Category details updated.";
$lang['newspublish']['catdel'] 					= "Category deleted.";
$lang['newspublish']['catid'] 					= "#ID";
$lang['newspublish']['catname'] 				= "Category Name";
$lang['newspublish']['catupdatedseq'] 			= "Sequence Updated";
$lang['newspublish']['catupdselect'] 			= "Update Sequence";
$lang['newspublish']['catdelselect'] 			= "Delete Selected";
$lang['newspublish']['parentcat'] 				= "Parent Category";
$lang['newspublish']['topcat'] 					= "Top";
$lang['newspublish']['catdescp'] 				= "Description";
$lang['newspublish']['catseq'] 					= "Sequence";
$lang['newspublish']['btnupdate'] 				= "Update";
$lang['newspublish']['btndel'] 					= "Delete";
$lang['newspublish']['btnback'] 				= "Back";
$lang['newspublish']['no'] 						= "No";
$lang['newspublish']['subcat'] 					= "Category";
$lang['newspublish']['catseqlist'] 				= "Seq";
$lang['newspublish']['delrecord'] 				= "Delete This Record";
$lang['newspublish']['suredelete'] 				= "Are you sure you want to delete this record ?";
$lang['newspublish']['plsname']					= "Please enter your Category Name.";
$lang['newspublish']['catdelete'] 				= "Selected category deleted.";


/***** RSS Page *****/
$lang['newspublish']['subscribe']				= "Subscribe";
$lang['newspublish']['rssurl']					= "RSS URL";
$lang['newspublish']['valenterrss']				= "Please enter RSS URL.";
$lang['newspublish']['statusrssadd']			= "New RSS subscribed.";
$lang['newspublish']['statusrsssubscribed']		= "This feed have been subscibed.";
$lang['newspublish']['statusrssupdate']			= "RSS details updated.";
$lang['newspublish']['statusrssdelete']			= "RSS unsubscribed.";
$lang['newspublish']['statusrssfilenotfound']	= "RSS Feed not found";
$lang['newspublish']['statusrssinvalidrss']		= "Invalid RSS Feed.";
$lang['newspublish']['rsstitle']				= "RSS Title";
$lang['newspublish']['rssversion']				= "RSS Version";
$lang['newspublish']['confirmunsubscribe'] 		= "Are you sure you want to unsubscribe this RSS ?";
$lang['newspublish']['rssreader']				= "RSS Reader";
$lang['newspublish']['switchfeed']				= "Switch Feed";
$lang['newspublish']['readnews']				= "Read News";
$lang['newspublish']['load']					= "Loading... Please wait.";


/***** Preference Settings *****/
$lang['newspublish']['statusprefersave'] 		= "Preference settings saved.";
$lang['newspublish']['statuspreferdefault'] 	= "Preference settings restored to default values.";
$lang['newspublish']['adminsetting']		 	= "Admin Settings";
$lang['newspublish']['frontendsetting']		 	= "Front End Settings";
$lang['newspublish']['listno'] 					= "Records per List News Page";
$lang['newspublish']['newsno'] 					= "Records per News Listing";
$lang['newspublish']['subcatno']				= "Columns of Sub-categories";
$lang['newspublish']['menublock']				= "Show/Hide Menu Blocks";
$lang['newspublish']['menucategory']			= "Categories";
$lang['newspublish']['menuarchive']				= "Archives";
$lang['newspublish']['menurssnews']				= "RSS Reader";
$lang['newspublish']['from'] 					= "From";
$lang['newspublish']['subject'] 				= "Subject";
$lang['newspublish']['message'] 				= "Message";
$lang['newspublish']['definetellfriend']		= "Define Tell-A-Friend Email";
$lang['newspublish']['newsannouncetitle']		= "Title of News/Annoucement";
$lang['newspublish']['newsannouncesmall']		= "Small Fonts of News/Annoucment Content";
$lang['newspublish']['fontface'] 				= "Font Face";
$lang['newspublish']['color'] 					= "Color";
$lang['newspublish']['size'] 					= "Size";
$lang['newspublish']['small'] 					= "Small";
$lang['newspublish']['link'] 					= "Link";
$lang['newspublish']['hover'] 					= "Hover";
$lang['newspublish']['style'] 					= "Style";
$lang['newspublish']['fontsncolor'] 			= "Fonts & Colors";
$lang['newspublish']['newstext']	 			= "News Text";
$lang['newspublish']['save'] 					= " Save ";
$lang['newspublish']['default'] 				= "Use Default";
$lang['newspublish']['showprint']				= "Enable Print Version of News Article";
$lang['newspublish']['discatdescp']				= "Show Category Description";
$lang['newspublish']['sortnews'] 				= "Sort News Article By Sequence In";
$lang['newspublish']['sortnewsasc'] 			= "Ascending Order";
$lang['newspublish']['sortnewsdesc'] 			= "Descending Order";
$lang['newspublish']['dateformat']				= "Date Format For News";
$lang['newspublish']['displaystyle']			= "Display Full News Article In";
$lang['newspublish']['popupwindow']				= "Pop Up Window";
$lang['newspublish']['samewindow']				= "Same Window";
$lang['newspublish']['showpubdate']				= "Show Publish Date in News Listing";
$lang['newspublish']['showsummary']				= "Show Summary In News Listing";
$lang['newspublish']['showsummaryart']			= "Show Summary In News Article";
$lang['newspublish']['sefurl']					= "Enable Static URL";
$lang['newspublish']['sefurlnote']				= "Only apply on apache web server with mod_rewrite enabled.";
$lang['newspublish']['htaccess']				= "Copy the below code to newspublish/.htaccess file.";
$lang['newspublish']['showemail']				= "Enable Email-to-Friend";
$lang['newspublish']['frontendurl']				= "Front-End URL";
$lang['newspublish']['rsssetting']				= "RSS Feed Setting";
$lang['newspublish']['rssenable']				= "Enable RSS Feed";
$lang['newspublish']['rsscopyright']			= "Copyright Text";
$lang['newspublish']['rsseditor']				= "RSS Editor Email";
$lang['newspublish']['rsslanguage']				= "Language";
$lang['newspublish']['rsssitelogo']				= "Image Thumbnails";
$lang['newspublish']['rssdescription']			= "RSS Description";
$lang['newspublish']['rsstitle']				= "RSS Title";
$lang['newspublish']['rsslive']					= "Publish News Period";
$lang['newspublish']['day']						= "day/s";
$lang['newspublish']['remove']					= "Remove";
$lang['newspublish']['prefsetting']				= "Preference Setting";

/*****Integration Guide*****/
$lang['newspublish']['interguide']				= "Integration Guides";
$lang['newspublish']['viewsamplepage']			= "Click Here To View Sample Page";
$lang['newspublish']['frontinterguide']			= "Front-End Integration Guidelines";
$lang['newspublish']['interguide1']				= "You can easily integrate the front-end with your web design pages.";
$lang['newspublish']['before']					= "Before";
$lang['newspublish']['afterintergration']		= "After Integration";
$lang['newspublish']['intergrationins']			= "Integration Instructions";
$lang['newspublish']['invalidfrontend']			= "Invalid Front End URL, ensure that the Front-End URL in Preference > Preference Setting page is not blank and valid";
$lang['newspublish']['viewfrontend']			= "Front End View";
$lang['newspublish']['guide1']					= "Using a text editor, open the web page file (.php file) that you wish to integrate the News Publisher to, insert the line below to the top of the file, to call the 'config.php' file.";
$lang['newspublish']['guide1note']				= "* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.";
$lang['newspublish']['guide2']					= "In your .php web page file, put the line below to call the menu area - news categories and archives.";
$lang['newspublish']['guide3']					= "Meanwhile, to display the news's content area such as news title, full article page, put the line below.";
$lang['newspublish']['guide4']					= "Next, use the following code to display the announcement section.";


/***** 	Subscribe RSS *****/
$lang['newspublish']['imgallowed']				= "Image file not recognized.";
$lang['newspublish']['warnexceed']				= "Uploaded file size exceeds.";
$lang['newspublish']['dateadd']					= "Date Add";


/***** Front-End *****/
$lang['newspublish']['nonewsfound']		 		= "No News Article Found";
$lang['newspublish']['emailthis']				= "Email To Friend";
$lang['newspublish']['printthis'] 				= "Print Version";
$lang['newspublish']['postedon'] 				= "Published on";
$lang['newspublish']['noannounce'] 				= "No Announcement";
$lang['newspublish']['print'] 					= "Print";
$lang['newspublish']['close'] 					= "Close";
$lang['newspublish']['tellfrienddesc'] 			= "Tell Your Friend About This News";
$lang['newspublish']['tellfriendthankyou']		= "Thank you. Your request has been sent to your friend's email.";
$lang['newspublish']['yourname'] 				= "Your Name";
$lang['newspublish']['youremail'] 				= "Your Email";
$lang['newspublish']['friendname'] 				= "Friend's Name";
$lang['newspublish']['friendemail'] 			= "Friend's Email";
$lang['newspublish']['yourmessage'] 			= "Your Message";
$lang['newspublish']['send2friend'] 			= "Send To Friend";
$lang['newspublish']['plsyourname']				= "Please enter Your Name.";
$lang['newspublish']['plsyouremail']			= "Please enter Your Email.";
$lang['newspublish']['plsfriendname']			= "Please enter Your Friend\'s Name.";
$lang['newspublish']['plsfriendemail']			= "Please enter Your Friend\'s Email.";
$lang['newspublish']['all'] 					= "ALL";
$lang['newspublish']['backtop'] 				= "Back To Top";
$lang['newspublish']['nocategories']			= "No Categories";
$lang['newspublish']['noarchives']				= "No Archives";
$lang['newspublish']['titlecategory']			= "News Categories";
$lang['newspublish']['titlearchive']			= "News Archives";
$lang['newspublish']['titlelatest']				= "Latest News";
$lang['newspublish']['announcements']			= "Announcements";
$lang['newspublish']['searchnews']				= "Search News";
$lang['newspublish']['enterkeyword']			= "By Keywords";
$lang['newspublish']['enterdatepost']			= "By Publish Date";
$lang['newspublish']['go']						= "Go";
$lang['newspublish']['keyword']					= "Keyword";
$lang['newspublish']['resultsearch']			= "Result Search";
$lang['newspublish']['searchfound']				= "Records(s) found with ";
$lang['newspublish']['hnofound']				= "No result found";
$lang['newspublish']['searchbykey']				= "Search by Keyword";
$lang['newspublish']['searchbydate']			= "Search by Date Publish";
$lang['newspublish']['backtoarchives']			= "Back To Archives";
$lang['newspublish']['backtosearchlist']		= "Back To Search List";

/***** Others *****/
$lang['newspublish']['january'] 				= "January";
$lang['newspublish']['february'] 				= "February";
$lang['newspublish']['march'] 					= "March";
$lang['newspublish']['april'] 					= "April";
$lang['newspublish']['may'] 					= "May";
$lang['newspublish']['june'] 					= "June";
$lang['newspublish']['july'] 					= "July";
$lang['newspublish']['august'] 					= "August";
$lang['newspublish']['september'] 				= "September";
$lang['newspublish']['october'] 				= "October";
$lang['newspublish']['november'] 				= "November";
$lang['newspublish']['december'] 				= "December";

?>