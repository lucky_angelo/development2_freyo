<?
include_once ("../config.php");
if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	include_once($path["docroot"]."common/css.php");
	include ($path["docroot"]."common/class.rssparser.php");
	
	$rss = new lastRSS(); 
	
	$location=$path["docroot"]."_files/cache";
	if(!file_exists($location)){
		@mkdir ($location, 0777);
		@chmod ($location, 0777);
	}
	$rss->cache_dir = $location; 
	$rss->cache_time = '';
	
	$oNews_Rss->data = array("rss_id","url");
	$oNews_Rss->where = "rss_id='$switchrss'";  	
	$result=$oNews_Rss->getList();
	if($myrow=mysql_fetch_row($result)){		
		$rssurl = "http://".$myrow[1];
	}else{ $rssurl=""; }
	mysql_free_result($result);
}

echo "<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>";
if ($pageaction=="details"){
	if ($rs = $rss->get($rssurl)) {
	    if ($rs[image_url] != '') {
	        echo "<tr><td colspan=2 align=center><a href=\"$rs[image_link]\" target=\"_blank\"><img src=\"$rs[image_url]\" alt=\"$rs[image_title]\" vspace=\"1\" border=\"0\" /></a></td></tr>";
	    }
	    echo "<tr><td colspan=2 align=center><big><b><a href=\"$rs[link]\" target=\"_blank\">$rs[title]</a></b></big></td></tr>";
	    foreach($rs['items'] as $item) {
        	echo "\t<tr><td colspan=2><br/><a href=\"$item[link]\" target=\"_blank\">".html_entity_decode($item['title'])."</a>";
        	if ($item['pubDate'] != ""){
	    		echo "</td></tr><tr><td colspan=2><i><font color=gray size=1>$item[pubDate]</font></i></td></tr>";
    		}
    		echo "</td></tr>";
    		$desc = html_entity_decode($item['description']);
    		preg_match("/\<\!\[CDATA\[(.*)\]\]/",$desc,$match);
    		if (count($match)!=0){ $desc = $match[1]; }
        	echo "<tr><td width=5% valign=top align=center>&nbsp;</td><td>".$desc."</td></tr>";
	    }
	    echo "";
	    if ($rs['copyright'] != ""){
	    	echo "<tr><td colspan=2><br /><br /><i><font color=gray size=1>".html_entity_decode($rs[copyright])."</font></i></td></tr>";
    	}
	    if ($rs['managingEditor'] != ""){
	    	echo "<tr><td colspan=2><i><font color=gray size=1>From $rs[managingEditor]</font></i></td></tr>";
    	}
    }else {
		echo "<tr><td><br />".$lang['newspublish']['statusrssfilenotfound']." at $rssurl</td></tr>";
	}
    
}else{
	$oNews_Rss->data = array("url");
	$oNews_Rss->where = "";
	$oNews_Rss->order = "title";  	
	$result=$oNews_Rss->getList();
	while($myrow=mysql_fetch_row($result)){		
		$rssurl = "http://".$myrow[0];	
		if ($rs = $rss->get($rssurl)) {
			echo "<tr><td><br /><b><a href=\"$rs[link]\" target=\"_blank\">$rs[title]</a></b> - $rs[description]</td></tr>";
		}else {
			echo "<tr><td><br />".$lang['newspublish']['statusrssfilenotfound']." at $rssurl</td></tr>";
		}
	}
	mysql_free_result($result);
}
echo "</table>";
?>