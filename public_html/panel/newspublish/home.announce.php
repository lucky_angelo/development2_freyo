<?
$monthArr = array($lang['newspublish']['january'], $lang['newspublish']['february'], $lang['newspublish']['march'], $lang['newspublish']['april'], $lang['newspublish']['may'], $lang['newspublish']['june'], $lang['newspublish']['july'], $lang['newspublish']['august'], $lang['newspublish']['september'], $lang['newspublish']['october'], $lang['newspublish']['november'], $lang['newspublish']['december']);

	if ($oSystem->getValue("news_dateformat")=="m-d-Y"){	
		$dateformat="%m-%d-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="d-m-Y"){	
		$dateformat="%d-%m-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="dbY"){	
		$dateformat="%d %b %Y";
	} else if ($oSystem->getValue("news_dateformat")=="bdY"){	
		$dateformat="%b %d, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="dMY"){	
		$dateformat="%d %M, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="MdY"){	
		$dateformat="%M %d, %Y";
	} else {
		$dateformat="%Y-%m-%d";
	}
	
echo "<table border=0 cellspacing=2 cellpadding=3 width=100%>";

	$localtz = $oSystem->getValue("sys_timezone");
	$localtime = $localtz==""?time():getLocalTime($localtz);
	
	$year = date("Y",$localtime); 
	$month = date("m",$localtime); 
	$day = date("d",$localtime); 
	$currentdate = $year."-".$month."-".$day;
	
$oNews->data = array("title", "summary", "date_format(datepost,' %d, %Y')", "date_format(datepost,'%m')","thumbnail", "date_format(datepost,'$dateformat')","datepost");
$oNews->where = "newstype='announce' and display='Yes' and (dateexpire='0000-00-00' or dateexpire>date_format('$currentdate', '%Y-%m-%d')) and datepost<=date_format('$currentdate', '%Y-%m-%d') ";
$oNews->order = "seq $news_seq,datepost desc";
$result = $oNews->getList();
if (mysql_num_rows($result)!=0){
	while($myrow=mysql_fetch_row($result)){
		if ($oSystem->getValue("news_showpubdate")=="Yes" && (!(empty($myrow[6]) || $myrow[6]=="0000-00-00"))){ $posted = "<div class=textsmall>$myrow[5]</div>"; } else { $posted =""; }
		$thumbnail = ($myrow[4]!=""?"<td valign=top><img src=\"".$path["webroot"]."_files/newspublish/$myrow[4]\" border=0 align=left></td><td valign=top>":"<td valign=top colspan=2>");
		
		echo "
		<tr>$thumbnail<div class=title-n>".stripslashes($myrow[0])."</div><div class=textsmall>$posted</div><br>
		<div><span class=annoucebody>".nl2br(stripslashes($myrow[1]))."</span></div><br></td></tr>
		";
	}
	mysql_free_result($result);
	
}else{

	echo "<tr><td valign=top><br>".$lang['newspublish']['noannounce']."</td></tr>";

}

echo "</table>";
	

?>