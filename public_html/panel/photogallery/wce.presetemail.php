<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:48 28.12.2006                  */
/**************************************************/


if($pageaction=="save"){
  $oSystem->setValue("photo_adminemail",$photo_adminemail); 
  $oSystem->setValue("photo_forgetsubject",$photo_forgetsubject);
  $oSystem->setValue("photo_forgetbody",$photo_forgetbody);
   
  $oSystem->setValue("photo_photoapprove",$photo_photoapprove); 
  $oSystem->setValue("photo_photosubject",$photo_photosubject);
  $oSystem->setValue("photo_photobody",$photo_photobody);
  $oSystem->setValue("photo_commentapprove",$photo_commentapprove); 
  $oSystem->setValue("photo_commentsubject",$photo_commentsubject);
  $oSystem->setValue("photo_commentbody",$photo_commentbody);
  $status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['prefsave']."<br>";	  

}

if($pageaction=="default"){
  $oSystem->setValue("photo_adminemail","");
  $oSystem->setValue("photo_forgetsubject","Your login information at YourDomain.com");
  $oSystem->setValue("photo_forgetbody","Dear [[name]],\n\nThank you for contacting us. \n\nBelow are your account information:\n\nUsername : [[username]]\nPassword : [[password]]\n\nPlease keep us informed if you need any of our assistance.\n\n\nRegards\nYourDomain.com\n");
  
  $oSystem->setValue("photo_photoapprove","Yes"); 
  $oSystem->setValue("photo_photosubject","New Photo Submitted To Your Gallery");
  $oSystem->setValue("photo_photobody","We would like to notify you about new photo submission.\n\nBelow is the new photo information:\n\nID: [[photoid]]\nName : [[name]]\nEmail : [[email]]\nDate Posted: [[datepost]]\nTitle : [[title]]\nCategory : [[category]]\nBrief Description : [[briefdescp]]\nDetail Description : [[moredescp]]\nPhoto : [[photolink]]\n\nRegards,\nYourDomain.com\n");

  $oSystem->setValue("photo_commentapprove","Yes"); 
  $oSystem->setValue("photo_commentsubject","New Comment Posted To Your Photo");
  $oSystem->setValue("photo_commentbody","We would like to notify you about new submission comment approval by you. \n\nBelow is the comment information:\n\nID: [[photoid]]\nPhoto: [[photo]]\nCategory: [[category]]\nDate Posted: [[datepost]]\nAuthor Name : [[author]]\nComment : [[comment]]\n\nRegards,\nYourDomain.com\n");
  $status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['prefrestore']."<br>";	  

}

echo"<table border=0 width=100%><tr><td><b>".$lang['photogallery']['photogallery']."</b></td><td>";
include("wce.menu.php");
echo"
	</td></tr></table>
	<hr size=1 color=#606060>$status_message<br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=\"index.php?component=$component&page=$page\" method=post>
<input type=hidden name=pageaction>
<tr><td valign=top>
	<table border=0 width=100%>
		<tr><td width=23%>".$lang['photogallery']['adminemail']."</td><td><input type=text name=\"photo_adminemail\" style=\"width:200px\" value=\"".stripslashes(htmlentities($oSystem->getValue("photo_adminemail")))."\"></td></tr>
	</table><br>
	&nbsp;<b><u>".$lang['photogallery']['photonotifycopy']."</u></b><br><br>
	<table border=0 width=100%>
	<tr><td>".$lang['photogallery']['autoapprove']."</td><td><input type=checkbox name=\"photo_photoapprove\" value=\"Yes\" ".($oSystem->getValue("photo_photoapprove")=="Yes"?"checked":"").">
	<tr><td width=23%>".$lang['photogallery']['forgetsubject']."</td><td><input type=text name=\"photo_photosubject\" style=\"width:330px\" value=\"".stripslashes(htmlentities($oSystem->getValue("photo_photosubject")))."\"></td></tr>
	<tr><td valign=top>".$lang['photogallery']['forgetbody']."</td><td><textarea name=\"photo_photobody\" cols=80 rows=10 style=\"width:400px\">".stripslashes(htmlentities($oSystem->getValue("photo_photobody")))."</textarea></td></tr>
	</table><br>
	
	&nbsp;<b><u>".$lang['photogallery']['notifyadmincopy']."</u></b><br><br>
	<table border=0 width=100%>
	<tr><td>".$lang['photogallery']['autoapprove']."</td><td><input type=checkbox name=\"photo_commentapprove\" value=\"Yes\" ".($oSystem->getValue("photo_commentapprove")=="Yes"?"checked":"").">
	<tr><td width=23%>".$lang['photogallery']['forgetsubject']."</td><td><input type=text name=\"photo_commentsubject\" style=\"width:330px\" value=\"".stripslashes(htmlentities($oSystem->getValue("photo_commentsubject")))."\"></td></tr>
	<tr><td valign=top>".$lang['photogallery']['forgetbody']."</td><td><textarea name=\"photo_commentbody\" cols=80 rows=10 style=\"width:400px\">".stripslashes(htmlentities($oSystem->getValue("photo_commentbody")))."</textarea></td></tr>
	</table><br>

	&nbsp;<b><u>".$lang['photogallery']['forgetpasscopy']."</u></b><br><br>
	<table border=0 width=100%>
	<tr><td width=23%>".$lang['photogallery']['forgetsubject']."</td><td><input type=text name=\"photo_forgetsubject\" style=\"width:330px\" value=\"".stripslashes(htmlentities($oSystem->getValue("photo_forgetsubject")))."\"></td></tr>
	<tr><td valign=top>".$lang['photogallery']['forgetbody']."</td><td><textarea name=\"photo_forgetbody\" cols=80 rows=10 style=\"width:400px\">".stripslashes(htmlentities($oSystem->getValue("photo_forgetbody")))."</textarea></td></tr>
	</table><br>
	
</td></tr>
<tr><td valign=top>	
	<input type=button value=\" ".$lang['photogallery']['btnsave']." \" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
	<input type=button value=\"".$lang['photogallery']['btndefault']."\" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">
</td></tr>
</form></table>
";
?>