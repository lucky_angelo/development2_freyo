<?php

	include_once("../config.php");
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		include_once($path["docroot"]."common/css.php");
	}
	if($parent_id==""){ $parent_id=0; }
	$localtz = $oSystem->getValue("sys_timezone");
	$localtime = $localtz==""?time():getLocalTime($localtz);
	$slideshowtime = $oSystem->getValue("photo_slideshowtime");
	$thisfile=$oSystem->getValue("photo_page");
	$sefurl = $oSystem->getValue("photo_sefurl");
	$photo_seq = $oSystem->getValue("photo_seq");

	$oPhoto_Gallery->data=array("distinct photo_id");
	$oPhoto_Gallery->where = "photo_gallery.category_id='$category_id' and photo_gallery.category_id=photo_category.category_id and showphoto='Yes'";	
	$oPhoto_Gallery->order = "photo_gallery.sequence $photo_seq";	
	$result=$oPhoto_Gallery->getListAccess();	
	$i_count=1;
	while($myrow=mysql_fetch_row($result)){ 
		if($myrow[0]==$photo_id){ $i_curpos=$i_count;}	
		$i_count++;
	}			
	$i_total = mysql_num_rows($result);
	mysql_free_result($result);	
	
	$parent_url=$thisfile."?category_id=$category_id&parent_id=$parent_id&photo_id=$photo_id&start=$start&countdisplay=No";	
		
	echo"
		<script language=javascript>
			function stopSlid(){
				parent.location.href='$parent_url';
			}
		</script>
	";
	
	echo "
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center valign=top>
			<tr><td width=20%><b><a href=javascript:stopSlid();>".$lang['photogallery']['hstopshow']."</a></td><td align=center><b>".$lang['photogallery']['showingslid']."&nbsp;$i_curpos&nbsp;".$lang['photogallery']['of']."&nbsp;$i_total</b></td><td width=20% align=right>&nbsp;</td></tr>
		</table><br>
	";
	
	/*****Public Category*****/
	$publiccat = array();
	$query = "select distinct photo_category.category_id from photo_category inner join photo_access on (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid') or (showprivate!='Yes')";
	$result = mysql_query($query,$oPhoto_Category->db);
	while($myrow=mysql_fetch_row($result)){	array_push($publiccat,$myrow[0]);	}
	mysql_free_result($result);	
	

	/*****Photo Details. Modify To Match Your Display*****/
	$oPhoto_Gallery->data = array("title","publish","descp","descpmore","photo","downloadfile","newphoto","hit","category_id","author");
	$result=$oPhoto_Gallery->getDetail($photo_id);
	if($myrow=mysql_fetch_row($result)){
		if(in_array($myrow[8],$publiccat)){
					
			$title=""; $publish=""; $descp=""; $descpmore=""; 
			$photo="<img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" border=0 alt=\"".$lang['photogallery']['havailable']."\"></a>"; 
			$downloadfile=""; $newphoto=""; $hit="<b>".$lang['photogallery']['hhit']." :</b> 0<br><br>"; 

			if($myrow[6]=="Yes"){				$newphotoico="<img src=\"".$path["webroot"]."photogallery/image/new.gif\" border=0>"; }else{$newphotoico="";}			
			if($myrow[0]!=""){					$title=stripslashes($myrow[0])."&nbsp;".$newphotoico."<br><br>";	}else{$title="".$newphotoico."<br><br>";}
			if($myrow[1]!="0000-00-00" && $oSystem->getValue("photo_showpubdate")=="Yes"){		$publish="<b>".$lang['photogallery']['publishdate']." :</b> $myrow[1]<br><br>";   }else {$publish="";}		 
			if($myrow[2]!=""){					$descp=stripslashes("$myrow[2]<br><br>"); }else{$descp="";}		
			if($myrow[3]!=""){					$descpmore=stripslashes("<br>$myrow[3]<br>"); }else{$descpmore="";}			
			if($myrow[4]!=""){					$photo="<img src=\"".$path["webroot"]."_files/photogallery/$myrow[4]\" border=0 alt=\"$myrow[0]\" style=\"border:1px #333333 solid\">"; }
			if($myrow[5]!=""){					$downloadfile="<a href=\"".$path["webroot"]."_files/photogallery/$myrow[5]\" target=_blank>[Download File]</a><br><br>"; }else{$downloadfile="";}				
			if($oSystem->getValue("photo_showhits")=="Yes"){if($myrow[7]!=0 || $myrow[7]!=""){	$hit="<b>".$lang['photogallery']['hhit']." :</b> $myrow[7]<br><br>"; }else{$hit="<b>".$lang['photogallery']['hhit']." :</b> 0<br><br>"; }} else {$hit="";}
			if($myrow[9]!=""){					$author="<b>".$lang['photogallery']['author']." :</b> $myrow[9]<br><br>";   }else {$author="";}		
			if($oSystem->getValue("photo_showrating")=="Yes"){
				$thisrate = $oPhoto_Gallery->getRating($photo_id);		
				if($thisrate!=0 || $thisrate!=""){	$rating="<b>".$lang['photogallery']['hrating']." :</b> ".$thisrate." ".$lang['photogallery']['of']." 5<br><br>"; }else{	$rating="<b>".$lang['photogallery']['hrating']." :</b> ".$lang['photogallery']['hnotrated']."<br><br>"; }
			}else{
				$rating="";		
			}
		
			switch($oSystem->getValue("photo_style")){
			
				/*****Side Details Style*****/
				case "1":
					echo "<table border=0 cellpadding=2 width=98% align=center>";
					echo "<tr><td valign=top align=center width=70%>$photo</td><td valign=top>$title $publish $author $hit $rating $downloadfile $descp</td></tr>";
					echo "<tr><td colspan=2>$descpmore</td></tr></table><br>";
					break;
					
				/*****Bottom Details Style*****/
				case "2":
					echo "<table border=0 cellpadding=5 width=98% align=center>";
					echo "<tr><td valign=top align=center colspan=4>$photo</td></tr>";
					echo "<tr><td colspan=4 align=center>$title $downloadfile</td></tr>";
					echo "<tr><td>$publish</td><td align=center>$author</td><td align=center>$hit</td><td align=right>$rating</td></tr>";
					echo "<tr><td colspan=4>$descp $descpmore</td></tr></table><br>";
					break;	
			}	
			
			/*****Visitor Comment*****/
			if($oSystem->getValue("photo_showcomment")=="Yes"){
				$count_review=$oSystem->getValue("photo_reviewlist");
				if ($oPhoto_Review->getCountReview($photo_id)){
					echo "
						<table border=0 cellspacing=2 cellpadding=2 width=98% align=center>
							<tr><td colspan=2><b><u>".$lang['photogallery']['hvisitorcomm']."</u></b></td></tr>
					";
					
					$oPhoto_Review->data = array("review_id","author","reviewcontent");
					$oPhoto_Review->where = "photo_id='$photo_id' and status='Active'";	    
		   			$oPhoto_Review->order = "date_poster DESC,review_id DESC limit 0,$count_review";	    
		   			$result_review=$oPhoto_Review->getList();
		   			while($myrow_review=mysql_fetch_row($result_review)){	   
		   				$myrow_review[1]=stripslashes($myrow_review[1]);
		   				$myrow_review[2]=stripslashes(nl2br($myrow_review[2]));
		   				echo"<tr><td width=15%>".$lang['photogallery']['authorname']."</td><td>&nbsp;:&nbsp;$myrow_review[1]</td></tr><tr><td colspan=2>$myrow_review[2]<br><hr size=1 color=#C0C0C0 width=100%></td></tr>";
		   			} mysql_free_result($result_review);
   			
					echo"</table><br>";
				}
			}		
		}
	}
	mysql_free_result($result);	
	echo"</td></tr></table>";
	
	$oPhoto_Gallery->data=array("distinct photo_id");
	$oPhoto_Gallery->where = "photo_gallery.category_id='$category_id' and photo_gallery.category_id=photo_category.category_id and showphoto='Yes'";	
	$oPhoto_Gallery->order = "photo_gallery.sequence $photo_seq";	
	$result=$oPhoto_Gallery->getListAccess();	
	$count=0; 
	while($myrow=mysql_fetch_row($result)){ 
		if($myrow[0]==$photo_id){ $curpos=$count; }	
		$count++; 
	}		
	$prevpos=$curpos-1; $nextpos=$curpos+1;	$curpos=$curpos+1;
	$total = mysql_num_rows($result);

	if($nextpos < $total){ 
		mysql_data_seek($result,$nextpos); 
		if($myrow=mysql_fetch_row($result)){ 
			$next_id=$myrow[0];
		}
	}
	mysql_free_result($result);
	$url="".$path["webroot"]."photogallery/home.slidshow.php?category_id=$category_id&parent_id=$parent_id&photo_id=$next_id&pageaction=slideshowplay&start=$start";

if ($curpos<$total){	
	echo"<meta http-equiv=\"Refresh\" Content=\"$slideshowtime;URL=$url\">";
} else {
	$reftime=$slideshowtime*1000;
	$parent_url=$thisfile."?category_id=$category_id&parent_id=$parent_id&photo_id=$photo_id&start=0&countdisplay=No";
	
	echo"
		<script language=javascript>
			var t=setTimeout('parentBack()',$reftime);
			
			function parentBack(){
				parent.location.href='$parent_url';
			}
		</script>
	";
	
}
	
?>
