<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:43 28.12.2006                  */
/**************************************************/


if($pageaction=="updatereview" && $review_id !=""){
	$oPhoto_Review->data = array("date_poster","author","reviewcontent");
	$oPhoto_Review->value = array($date_poster,addslashes($author),addslashes($reviews));
	$oPhoto_Review->update($review_id);	
	$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['commentupdate']."<br>";
}

$oPhoto_Review->data = array("date_poster","author","reviewcontent","photo_id");
$result = $oPhoto_Review->getDetail($review_id);
if ($myrow = mysql_fetch_row($result)){
	$date_poster = $myrow[0];
	$author = stripslashes(htmlentities($myrow[1]));
	$reviews = stripslashes(htmlentities($myrow[2]));
	$photo_id = $myrow[3];
}
mysql_free_result($result);

$oPhoto_Gallery->data = array("title","category_id","thumbnail");
$rst=$oPhoto_Gallery->getDetail($photo_id);
if($myr=mysql_fetch_row($rst)){	
	$phototitle = stripslashes($myr[0]); 
	$cat_id = $myr[1]; 
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if(file_exists($path["docroot"]."_files/photogallery/$myr[2]") && $myr[2]!=""){
			$thumbnail ="<img src=\"".$path["webroot"]."_files/photogallery/$myr[2]\" border=0 align=left>";
		} else {
			$thumbnail ="<img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" border=0 align=left>";
		}
	}
} mysql_free_result($rst);

$catname=stripslashes($oPhoto_Category->getCatName($cat_id));

?>
<html>
<head><title><? echo $lang['photogallery']['editreview'] ?></title>
<link rel=stylesheet type=text/css href="<? echo $path["webroot"]."".$path["skin"]."style.css" ?>">
</head>
<body topmargin=10 leftmargin=10>
<? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=photogallery&page=wce.editreview.php&headfoot=no" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="updatereview">
<input type=hidden name=review_id value="<? echo $review_id ?>">
<tr><td valign=top width=20%><table border=0 cellpadding=2 cellspacing=2 width=98% align=center><tr><td><? echo $thumbnail ?></td></tr></table>
</td><td valign=top>
<table border=0 cellpadding=2 cellspacing=2 width=98% align=center>
	<tr><td valign=top><? echo $lang['photogallery']['lblphotoid'] ?></td><td valign=top>:&nbsp;<? echo $photo_id ?></td></tr>
	<tr><td valign=top><? echo $lang['photogallery']['lbltitle'] ?></td><td valign=top>:&nbsp;<? echo $phototitle ?></td></tr>
	<tr><td valign=top><? echo $lang['photogallery']['lblcategory'] ?></td><td valign=top>:&nbsp;<? echo $catname ?></td></tr>
	<tr><td width=20%><? echo $lang['photogallery']['author'] ?></td><td><input type=text name=author style="width:180px" value="<? echo $author ?>"></td></tr>
	<tr><td><? echo $lang['photogallery']['dateposter'] ?></td><td><input type=text id=date_poster name=date_poster style="width:100px" value="<? echo $date_poster ?>" onclick="this.select()">&nbsp;<a href="javascript:show_calendar('date_poster', document.thisform.date_poster.value);"><img src="common/datepick/calendar.gif" border="0" align=absmiddle></a></td></tr>
	<tr><td valign=top><? echo $lang['photogallery']['review'] ?></td><td valign=top><textarea name=reviews rows=7 cols=50 style="width:91%"><? echo $reviews ?></textarea></td></tr>
	<tr><td>&nbsp;</td>
	<td>
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
			<tr><td valign=top><input type=button value="<? echo $lang['photogallery']['btnupdate'] ?>" onclick="document.thisform.submit();"></td></tr>
		</table>
	</td></tr>
</table>
</td></tr>
</form></table>

<br><br>
<table border=0 width=100%><tr><td align=right><input type=button onclick=javascript:window.close(); value="<?echo $lang['photogallery']['close'] ?>"></td></tr></table>
</body></html>

<iframe width=163 height=187 id="iCal" name="iCal" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<script language="JavaScript" src="common/datepick/datepick.js"></script>