<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:49 28.12.2006                  */
/**************************************************/


if ($pageaction=="addreview"){
	$oPhoto_Review->data = array("photo_id","date_poster,author,reviewcontent","status");
	$oPhoto_Review->value = array($photo_id,$date_poster,addslashes($author),addslashes($reviews),$status);
	$oPhoto_Review->add();
	
	$date_poster = ""; $author =""; $reviews ="";
	$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['reviewadd']."<br>";	  
}

if($pageaction=="editreview" && $review_id !=""){
	$oPhoto_Review->data = array("date_poster","author","reviewcontent","status");
	$result = $oPhoto_Review->getDetail($review_id);
	if ($myrow = mysql_fetch_row($result)){
		$date_poster = $myrow[0];
		$author = stripslashes(htmlentities($myrow[1]));
		$reviews = stripslashes(htmlentities($myrow[2]));
		if ($myrow[3]=="Pending"){
			$chkpending="selected";
		} else {
			$chkactive="selected";
		}
	}
	mysql_free_result($result);
}

if($pageaction=="updatereview" && $review_id !=""){
	$oPhoto_Review->data = array("date_poster","author","reviewcontent","status");
	$oPhoto_Review->value = array($date_poster,addslashes($author),addslashes($reviews),$status);
	$oPhoto_Review->update($review_id);	
	$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['reviewupdate']."<br>";	
	
	$oPhoto_Review->data = array("date_poster","author","reviewcontent","status");
	$result = $oPhoto_Review->getDetail($review_id);
	if ($myrow = mysql_fetch_row($result)){
		$date_poster = $myrow[0];
		$author = stripslashes(htmlentities($myrow[1]));
		$reviews = stripslashes(htmlentities($myrow[2]));
		if ($myrow[3]=="Pending"){
			$chkpending="selected";
		} else {
			$chkactive="selected";
		}
	}
	mysql_free_result($result);	 
}

if($pageaction=="deletereview"){	
	if(count($delete_id)>0){
		for($i=0;$i<count($delete_id);$i++){
			$review_id_ref=$delete_id[$i];
			$oPhoto_Review->delete($review_id_ref);	
		}
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['reviewdelete']."<br>";	  
	}
}

if($pageaction=="cancel"){	
	$date_poster = ""; $author =""; $reviews ="";
}

if ($status==""){$chkactive="selected";}
if ($date_poster=="") { $date_poster=date("Y-m-d"); }

$oPhoto_Gallery->data = array("title");
$rst=$oPhoto_Gallery->getDetail($photo_id);
if($myr=mysql_fetch_row($rst)){	$phototitle = stripslashes($myr[0]); mysql_free_result($rst);}

?>
<table border=0 width=100%><tr><td><b><? echo $lang['photogallery']['menutitle'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->


<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=photogallery&page=wce.review.php&photo_id=<? echo $photo_id ?>&sortby=<? echo $sortby ?>&sortseq=<? echo $sortseq ?>&keyword=<? echo $keyword ?>&category_id=<? echo $category_id ?>&start=<? echo $start ?>&frompage=<? echo $frompage ?>" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction>
<input type=hidden name=review_id value="<? echo $review_id ?>">
<tr><td colspan=3 valign=top><u><b><? echo $lang['photogallery']['review'] ?></b></u>: <?= $phototitle ?><br><br></td>
	<td colspan=3 align=right valign=top><font class=bullet>&bull;</font>&nbsp;&nbsp;<a href="index.php?component=photogallery&page=wce.editphoto.php&photo_id=<? echo $photo_id ?>&sortby=<? echo $sortby ?>&sortseq=<? echo $sortseq ?>&keyword=<? echo $keyword3 ?>&category_id=<? echo $vcategory_id ?>&start=<? echo $start ?>&frompage=<? echo $frompage ?>"><? echo $lang['photogallery']['backtodetail'] ?></a>&nbsp;</td></tr>
<tr><td><? echo $lang['photogallery']['author'] ?></td><td><input type=text name=author style="width:180px" value="<? echo $author ?>"></td><td><? echo $lang['photogallery']['dateposter'] ?></td><td colspan=3><input type=text id=date_poster name=date_poster style="width:100px" value="<? echo $date_poster ?>" onclick="this.select()">&nbsp;<a href="javascript:show_calendar('date_poster', document.thisform.date_poster.value);"><img src="common/datepick/calendar.gif" border="0" align=absmiddle></a></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['review'] ?></td><td valign=top colspan=5><textarea name=reviews rows=7 cols=50 style="width:91%"><? echo $reviews ?></textarea></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['status'] ?></td><td valign=top colspan=5><select name=status>
<option value="Pending" <? echo $chkpending ?>><? echo $lang['photogallery']['pending'] ?></option>
<option value="Active" <? echo $chkactive ?>><? echo $lang['photogallery']['active'] ?></option>
</select></td></tr>
<tr><td>&nbsp;</td>
<td colspan=5>
	<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
	<? if (($pageaction=="editreview" || $pageaction=="updatereview") && $review_id!=""){?>
		<tr><td valign=top><input type=button value="<? echo $lang['photogallery']['btnupdate'] ?>" onclick="document.thisform.pageaction.value='updatereview';document.thisform.submit();" >&nbsp;<input type=button value="<? echo $lang['photogallery']['btncancel']?>" onclick="document.thisform.pageaction.value='cancel';document.thisform.submit();"></td></tr>
	<?}else{?>
		<tr><td valign=top><input type=button value=" <? echo $lang['photogallery']['add'] ?> " onclick="document.thisform.pageaction.value='addreview';document.thisform.submit();" ></td></tr>
	<?}?>
	</table>
</td></tr>
</form></table><br><br>

<!--List of review-->
<?
	echo"
		<table border=0 cellpadding=1 cellspacing=0 width=98% align=center>
		<tr><td><a href=\"javascript:ConfirmDelete()\">".$lang['photogallery']['delseleted']."</a></td></tr>
		<tr><td>

 		<form name=frmList action=\"index.php?component=$component&page=$page&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&category_id=$category_id&start=$start\" method=post>
		<input type=hidden name=review_id_ref>
		<input type=hidden name=photo_id value=\"$photo_id\">
		<input type=hidden name=pageaction value=\"deletereview\">
	";
		
	echo "
		<table border=0 cellpadding=0 cellspacing=0 width=100% align=center>
		    <tr bgcolor=\"#E6E6E6\" height=\"24\">
		      <td class=\"gridTitle\" align=\"center\" width=\"25\"><input type=checkbox id=\"checkAll\" onclick=\"selectAll()\"></td>	      
		      <td class=\"gridTitle\" width=17% align=\"left\">&nbsp;<b>".$lang['photogallery']['dateposter']."</b> &nbsp;</td>
		      <td class=\"gridTitle\" width=23% align=\"left\">&nbsp;<b>".$lang['photogallery']['author']."</b> &nbsp;</td>
		      <td class=\"gridTitle\" align=\"left\">&nbsp;<b>".$lang['photogallery']['review']."</b> &nbsp;</td>
		      <td class=\"gridTitle\" align=\"center\" width=70>&nbsp;<b>".$lang['photogallery']['status']."</b> &nbsp;</td>
		      <td class=\"gridTitle\" align=\"center\" width=\"30\">&nbsp;</td>
	    	</tr> 	
	";
	
	$oPhoto_Review->data = array("review_id","date_poster,author,reviewcontent","status");
	$oPhoto_Review->where = "photo_id='$photo_id'";	    
   	$oPhoto_Review->order = "review_id desc";	    
   	$result=$oPhoto_Review->getList();
   	$count=0;
   	while($myrow=mysql_fetch_row($result)){
   		$count++;  
   		$review_id=$myrow[0];
   		$myrow[1]=stripslashes($myrow[1]);
   		$myrow[2]=stripslashes($myrow[2]);
   		$reviewcontent=stripslashes($myrow[3]);
   		$thereviewlength=strlen($myrow[3]);
   		if ($thereviewlength>60){
   			$reviewcontent= substr($reviewcontent,0,60);
   			$reviewcontent=$reviewcontent."...";
   		} else {
   			$reviewcontent=$reviewcontent;
   		}
   		echo "
		    <tr id=\"row$count\" style=\"background:#F6F6F6\" height=\"24\">
		      	<td class=\"gridRow\" align=\"center\" width=\"25\"><input type=checkbox id=\"check$count\" name=\"delete_id[]\" value=\"$myrow[0]\" onclick=\"if(this.checked==true){ selectRow('row$count'); }else{ deselectRow('row$count'); }\"></td>
		      	<td class=\"gridRow\" align=\"left\">&nbsp;$myrow[1]&nbsp;</td>
		       	<td class=\"gridRow\" align=\"left\">&nbsp;$myrow[2]&nbsp;</td>
		        <td class=\"gridRow\" align=\"left\">&nbsp;$reviewcontent&nbsp;</td>
		         <td class=\"gridRow\" align=\"center\">&nbsp;$myrow[4]&nbsp;</td>
			  	<td class=\"gridRow\" align=\"center\">
		      		<a href=\"index.php?component=$component&page=$page&photo_id=$photo_id&review_id=$review_id&pageaction=editreview&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&category_id=$category_id&start=$start\"><img src=common/image/ico_edit.gif border=0 alt=\"".$lang['photogallery']['editthisrecord']."\"></a>
		      	</td>
	   		</tr>
	   	";	
   	}
   	mysql_free_result($result);
	echo "</form></table><td></tr></table><br><br><br><br><br>";   	
?>


<iframe width=163 height=187 id="iCal" name="iCal" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<script language="JavaScript" src="common/datepick/datepick.js"></script>

<script language=JavaScript>
	function ConfirmDelete(id){  
	    if(confirm('<? echo $lang['photogallery']['confirmdel'] ?>')){
			document.frmList.review_id_ref.value=id;
			document.frmList.submit();
		}
	}

	function selectAll(){
		if(document.getElementById("checkAll").checked==true){
			document.getElementById("checkAll").checked=true;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=true; document.getElementById(\"row$i\").style.background='#D6DEEC'; \n"; }	?>
		}else{
			document.getElementById("checkAll").checked=false;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=false; document.getElementById(\"row$i\").style.background='#F6F6F6'; \n"; } ?>		
		}
	}

	function selectRow(row){
		document.getElementById(row).style.background="#D6DEEC";
	}

	function deselectRow(row){
		document.getElementById(row).style.background="#F6F6F6";
	}
</script>