<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:33 28.12.2006                  */
/**************************************************/


if($pageaction=="add"){

	if($showphoto=="on"){ $showphoto="Yes"; }else{ $showphoto="No"; }
	if($newphoto=="on"){ $newphoto="Yes"; }
		
	$thumbnailname = strtolower(str_replace(" ","_",trim($_FILES['thumbnail']['name'])));
	$photoname = strtolower(str_replace(" ","_",trim($_FILES['photo']['name'])));
	$downloadfilename = strtolower(str_replace(" ","_",trim($_FILES['downloadfile']['name'])));

	$uniqueid = md5(uniqid(rand(), true));	
	$uniqueid=substr($uniqueid,0,5);
	
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if($thumbnailname!=""){ 
			$thumbnailname2=$uniqueid."_".$thumbnailname;
			$oPhoto_Gallery->uploadFile($_FILES['thumbnail']['tmp_name'],$path["docroot"]."_files/photogallery",$thumbnailname2); 
		}
		if($photoname!=""){ 
			$photoname2=$uniqueid."_".$photoname;
			$oPhoto_Gallery->uploadFile($_FILES['photo']['tmp_name'],$path["docroot"]."_files/photogallery",$photoname2);
		}
		if($downloadfilename!=""){ 
			$downloadfilename2=$uniqueid."_".$downloadfilename;
			$oPhoto_Gallery->uploadFile($_FILES['downloadfile']['tmp_name'],$path["docroot"]."_files/photogallery",$downloadfilename2); 
		}
	
		$ext = strrchr($photoname,".");
		if ($oSystem->getValue("photo_gd")=="20"){
			if($ext==".jpg" || $ext==".gif"){$valid_ext="yes"; } else { $valid_ext="no";}
		} else if ($oSystem->getValue("photo_gd")=="16") {
			if($ext==".jpg"){$valid_ext="yes";} else {$valid_ext="no";	}
		} else {$valid_ext="no";}
		
		if (!extension_loaded('gd')) {   $valid_ext="no";	}
			
		if($autothumbnail=="on" && $photoname!="" && $oSystem->getValue("photo_gd")!="None"){
			if($valid_ext=="yes"){	
				$thumbnailname2=$uniqueid."_t_".$photoname;			
				$src_file = $path["docroot"]."_files/photogallery/".$photoname2;
				$dest_file = $path["docroot"]."_files/photogallery/".$thumbnailname2;
				$thumbw = $oSystem->getValue("photo_thumbsize");
				$thumbh = $oSystem->getValue("photo_thumbsize");
				$oPhoto_Gallery->resizeImage($src_file,$dest_file,$thumbw,$thumbh,"85");
			}else {
				$thumbnailname2="";
				echo"<script>alert(\"".$lang['photogallery']['extwarning']."\");</script>";
			}			
		}
	
		if($autoresize=="on" & $photoname!="" && $oSystem->getValue("photo_gd")!="None"){
			if($valid_ext=="yes"){	
				$src_file = $path["docroot"]."_files/photogallery/".$photoname2;
				$dest_file = $path["docroot"]."_files/photogallery/".$photoname2;
				$resizew = $oSystem->getValue("photo_resize");
				$resizeh = $oSystem->getValue("photo_resize");
				if($copyright!="On"){
					$oPhoto_Gallery->resizeImage($src_file,$dest_file,$resizew,$resizeh,"85");
				}else{
					$oPhoto_Gallery->resizeImage($src_file,$dest_file,$resizew,$resizeh,"100");
				}
			} else {
				echo"<script>alert(\"".$lang['photogallery']['extwarning']."\");</script>";
			}
		}
		
		/*** water mark ***/
		if($watermark=="on" && $photoname!="" && $oSystem->getValue("photo_gd")=="20" && extension_loaded('gd')){
			$ext = strrchr($photoname,".");
			if($ext==".jpg"){
				if ($oSystem->getValue("photo_watermark")!=""){
					$src_file = $path["docroot"]."_files/photogallery/".$photoname2;
					$oPhoto_Gallery->createWaterMark($src_file);	
				} else {
					echo"<script>alert(\"".$lang['photogallery']['extwarning3']."\");</script>";	
				}
			} else { 
				echo"<script>alert(\"".$lang['photogallery']['extwarning2']."\");</script>";
			}
		}
		
		if($copyright=="on" && $photoname!="" && $oSystem->getValue("photo_gd")!="None"){
			if($valid_ext=="yes"){	
				$src_file = $path["docroot"]."_files/photogallery/".$photoname2;				
				$text = $oSystem->getValue("photo_copyright");
				$fontsize = 15; $x = 10; $y = 20; 
				$red = $oSystem->getValue("photo_red");
				$green = $oSystem->getValue("photo_green");

				$blue = $oSystem->getValue("photo_blue");		
				$oPhoto_Gallery->copyrightImage($src_file,$text,$fontsize,$x,$y,$red,$green,$blue);	
			} else {
				echo"<script>alert(\"".$lang['photogallery']['extwarning5']."\");</script>";
			}
		}	
	}
	
	$descp = str_replace("\r\n","<br>",$descp);
	
	$sequence = $oPhoto_Gallery->getLastSequence()+1;
	$publish = $publish==""?"0000-00-00":$publish;
	$oPhoto_Gallery->data = array("category_id","title","publish","descp","descpmore","author","thumbnail","photo","downloadfile","showphoto","newphoto","hit","sequence");
	$oPhoto_Gallery->value = array($category_id,addslashes($title),$publish,addslashes($descp),addslashes($descpmore),addslashes($author),$thumbnailname2,$photoname2,$downloadfilename2,$showphoto,$newphoto,$hit,$sequence);
	$oPhoto_Gallery->add();

	$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['statusphotoadd']."<br>";	
}

?>

<table border=0 width=100%><tr><td><b><? echo $lang['photogallery']['photogallery'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=photogallery&page=wce.addphoto.php" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="add">
<tr><td valign=top width=\"30%\"><? echo $lang['photogallery']['choosecategory'] ?></td><td colspan=5><select name=category_id><?	$oPhoto_Category->getCategoryOption(0); ?></select></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['phototitle'] ?></td><td colspan=5><input type=text name=title style="width:230px"></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['publishdate'] ?></td><td colspan=5><input type=text name=publish id=publish style="width:100px" value="<? echo date("Y-m-d") ?>" onclick="this.select()">&nbsp;<a href="javascript:show_calendar('publish', document.thisform.publish.value);"><img src="common/datepick/calendar.gif" border="0" align=absmiddle></a></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['author'] ?></td><td colspan=5><input type=text name=author style="width:230px"></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['thumbnail'] ?></td><td colspan=5><input type=file name=thumbnail style="width:230px"><? echo $lang['photogallery']['idealimagesize1'] ?></td></tr>
<tr><td valign=top>&nbsp;</td><td colspan=5><table border=0 cellpadding=0 cellspacing=0><tr><td><input type=checkbox name=autothumbnail></td><td><? echo $lang['photogallery']['autothumbnail'] ?></td></tr></table></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['enlarged'] ?></td><td colspan=5><input type=file name=photo style="width:230px"><? echo $lang['photogallery']['idealimagesize2'] ?></td></tr>
<tr><td valign=top>&nbsp;</td><td colspan=5><table border=0 cellpadding=0 cellspacing=0><tr><td><input type=checkbox name=autoresize></td><td><? echo $lang['photogallery']['autoresize'] ?></td><td>&nbsp;&nbsp; <input type=checkbox name=copyright></td><td><? echo $lang['photogallery']['addcopyright'] ?></td></tr></table></td></tr>
<tr><td valign=top>&nbsp;</td><td colspan=5><table border=0 cellpadding=0 cellspacing=0><tr><td><input type=checkbox name=watermark></td><td><? echo $lang['photogallery']['addwatermark'] ?></td></tr></table></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['filedownload'] ?></td><td colspan=5><input type=file name=downloadfile style="width:230px"></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['hits'] ?></td><td width=13%><input type=text name=hit style="width:35px" value="0"></td>
	<td width=13%><? echo $lang['photogallery']['setasnew'] ?></td><td width=15%><input type=checkbox name=newphoto></td>
	<td width=13%><? echo $lang['photogallery']['showphoto'] ?></td><td><input type=checkbox name=showphoto checked></td></tr>
<tr></tr>
<tr><td valign=top><? echo $lang['photogallery']['briefdescp'] ?><br>(<?= $lang['photogallery']['htmlenabled'] ?>)</td><td colspan=6><textarea name=descp rows=5 cols=65></textarea></td></tr>
<tr><td valign=top colspan=6><? echo $lang['photogallery']['moredescp'] ?></td></tr>
<tr><td valign=top colspan=6>
	<table border=0 width=100% cellpadding=0 cellspacing=0><tr><td>
		<?
		$oFCKeditor = new FCKeditor('descpmore') ;
		$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
		$oFCKeditor->Width		= '100%' ;
		$oFCKeditor->Height		= '350' ;
		$oFCKeditor->Create() ;
		?>
	</td></tr></table>
</td></tr>
<tr><td colspan=6><br><input type=button value="  <? echo $lang['photogallery']['add'] ?>  " onclick="document.thisform.submit();"></td></tr>
</form></table>

<iframe width=163 height=187 id="iCal" name="iCal" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<script language="JavaScript" src="common/datepick/datepick.js"></script>
