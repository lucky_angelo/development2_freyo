#Newsletter 4.1

CREATE TABLE newsletter_delivery (
  delivery_id int(10) NOT NULL auto_increment,
  delivery date default NULL,
  delivery_time time NOT NULL,
  letter_id int(10) default NULL,
  group_id int(10) default NULL,
  subscriber_id int(10) default NULL,
  status varchar(10) default NULL,
  PRIMARY KEY  (delivery_id)
) ENGINE=MyISAM;

CREATE TABLE newsletter_group (
  group_id int(10) NOT NULL auto_increment,
  name varchar(200) default NULL,
  showgroup varchar(10) default NULL,
  PRIMARY KEY  (group_id)
) ENGINE=MyISAM;

INSERT INTO newsletter_group VALUES (1, 'General','Yes');
INSERT INTO newsletter_group VALUES (2, 'Private','');

CREATE TABLE newsletter_letter (
  letter_id int(10) NOT NULL auto_increment,
  group_id int(10) default NULL,
  dateletter date default NULL,
  datesend date default NULL,
  title text,
  bodytext longtext,
  bodyhtml longtext,
  attachment text,
  send_attachment varchar(5) default NULL,
  showletter varchar(5) default NULL,
  PRIMARY KEY  (letter_id)
) ENGINE=MyISAM;

INSERT INTO newsletter_letter VALUES (1, 2, '0000-00-00', NULL, 'Personalized Newsletter Sample', 'To: \n[[firstname]],[[lastname]]\n[[position]]\n[[company]]\n[[email]]\n\nNew Product Release!\n-------------------------------\nWe are proudly to inform you that we are releasing a new product this month..\n.....\n.....\n\nThis Month Special!\n---------------------------\nCheck out our special offer for this month. Hurry while stock last!\n.....\n\n[[attachment]]', '<p><font face=Arial><font size=2><strong>To:</strong>&nbsp;<br />[[firstname]],[[lastname]]<br />[[position]]<br />[[company]]<br />[[email]]<br /><br /></font></font><font face=Arial><font size=2><u><font color=#0033ff><strong>New Product Release!<br /></strong></font><br /></u>We are&nbsp;proudly to inform you that we are releasing a new product this month..<br />.....<br />.....<br /><br /></font></font><font face=Arial color=#0033ff size=2><u><strong>This Month Special!<br /></strong></u></font><font face=Arial color=#000000 size=2>Check out our special offer for this month. Hurry while stock last!<br />.....</font></p>\n<p>[[attachment]]</p>', '', '', '');

CREATE TABLE newsletter_subscribe (
  group_id int(10) NOT NULL default '0',
  subscriber_id int(10) NOT NULL default '0'
) ENGINE=MyISAM;

CREATE TABLE newsletter_subscriber (
  subscriber_id int(10) NOT NULL auto_increment,
  firstname text default NULL,
  lastname text default NULL,
  company text default NULL,
  position text(120) default NULL,
  email text,
  lettertype varchar(5) default NULL,
  status varchar(20) default NULL,
  PRIMARY KEY  (subscriber_id)
) ENGINE=MyISAM;

CREATE TABLE newsletter_template (
  letter_id int(10) NOT NULL auto_increment,
  letter_name text,
  bodytext longtext,
  bodyhtml longtext,
  PRIMARY KEY  (letter_id)
) ENGINE=MyISAM;

INSERT INTO newsletter_template VALUES (1, 'Personal_template', 'To: \n[[firstname]],[[lastname]]\n[[position]]\n[[company]]\n[[email]]\n\nNew Product Release!\n-------------------------------\nWe are proudly to inform you that we are releasing a new product this month..\n.....\n.....\n\nThis Month Special!\n---------------------------\nCheck out our special offer for this month. Hurry while stock last!\n.....\n\n[[attachment]]', '<p><font face=Arial><font size=2><strong>To:</strong>&nbsp;<br />[[firstname]],[[lastname]]<br />[[position]]<br />[[company]]<br />[[email]]<br /><br /></font></font><font face=Arial><font size=2><u><font color=#0033ff><strong>New Product Release!<br /></strong></font><br /></u>We are&nbsp;proudly to inform you that we are releasing a new product this month..<br />.....<br />.....<br /><br /></font></font><font face=Arial color=#0033ff size=2><u><strong>This Month Special!<br /></strong></u></font><font face=Arial color=#000000 size=2>Check out our special offer for this month. Hurry while stock last!<br />.....</font></p>\n<p>[[attachment]]</p>');


INSERT INTO sys_component VALUES (NULL, 'Newsletter', 'newsletter', 'newsletter', 'Multiple groups, confirmation, activation, unsubscribe, HTML or text-based, past newsletter, personalization, HTML editor, test email, import subscribers, bulk sending tool. ', '4.1', 'wce.listletter.php', '', 'Show','');
INSERT INTO sys_variable VALUES ('newsletter_newlistno', '20');
INSERT INTO sys_variable VALUES ('newsletter_msgconfirmemail', 'Dear [[firstname]] [[lastname]],\n\nThank you for subscribing. We acknowledge that you are interested in our below newsletter:\n\n[[grouplist]] \n\nPlease click on the link below to activate your Newsletter subscription.\n\n[[activationurl]] \n\nFrom,\nwww.yourdomain.com\n');
INSERT INTO sys_variable VALUES ('newsletter_sublistno', '20');
INSERT INTO sys_variable VALUES ('newsletter_pageno', '10');
INSERT INTO sys_variable VALUES ('newsletter_newstextbody', '[[newsletterbody]]\r\n\r\n\r\nClick [[tohtmlurl]] to receive HTML-based newsletter in the future. \r\n\r\nYour are currently subscribed as: [[email]]. To unsubscribe, please click [[unsubscribeurl]].');
INSERT INTO sys_variable VALUES ('newsletter_newshtmlbody', '[[newsletterbody]]\r\n\r\n\r\n<font size=1 face=arial>Click [[totexturl]] to receive text-based newsletter in the future. Your are currently subscribed as: [[email]]. To unsubscribe, please click [[unsubscribeurl]] .</font>');
INSERT INTO sys_variable VALUES ('newsletter_seq', 'desc');
INSERT INTO sys_variable VALUES ('newsletter_enablebrowse', 'Yes');
INSERT INTO sys_variable VALUES ('newsletter_popemail', '');
INSERT INTO sys_variable VALUES ('newsletter_popserver', '');
INSERT INTO sys_variable VALUES ('newsletter_poplogin', '');
INSERT INTO sys_variable VALUES ('newsletter_poppass', '');
INSERT INTO sys_variable VALUES ('newsletter_page', 'samplenewsletter.php');
INSERT INTO sys_variable VALUES ('newsletter_enableimageverify','');