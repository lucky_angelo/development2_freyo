<?

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	include($path["docroot"]."newsletter/class.letter.php");
	$oNewsletter_Letter = new Newsletter_Letter;
	$oNewsletter_Letter->db = $oDb->db;
	
	include($path["docroot"]."newsletter/class.group.php");
	$oNewsletter_Group = new Newsletter_Group;
	$oNewsletter_Group->db = $oDb->db;
	
	include($path["docroot"]."newsletter/class.subscriber.php");
	$oNewsletter_Subscriber = new Newsletter_Subscriber;
	$oNewsletter_Subscriber->db = $oDb->db;
	
	include($path["docroot"]."newsletter/class.delivery.php");
	$oNewsletter_Delivery = new Newsletter_Delivery;
	$oNewsletter_Delivery->db = $oDb->db;
	
	include($path["docroot"]."newsletter/class.template.php");
	$oNewsletter_Template = new Newsletter_Template;
	$oNewsletter_Template->db = $oDb->db;
}
?>