<?

	$oTopMenu = new TopMenu();
	$oTopMenu->component = "newsletter";
	
	$oMenu = new Menu();
	$oMenu->label = $lang['newsletter']['writenewsletter'];
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['addnewsl'];
	$oSubMenu->page = "wce.addletter.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['listnewsl'];
	$oSubMenu->page = "wce.listletter.php";
	$oMenu->addSub($oSubMenu);
	
	$oSubMenu = new SubMenu();		//wee add template letter
	$oSubMenu->label = $lang['newsletter']['addtemplate'];
	$oSubMenu->page = "wce.addtemplateletter.php";
	$oMenu->addSub($oSubMenu);
	
	$oSubMenu = new SubMenu();		//wee list template letter
	$oSubMenu->label = $lang['newsletter']['listtemplate'];
	$oSubMenu->page = "wce.listtemplateletter.php";
	$oMenu->addSub($oSubMenu);
	$oTopMenu->add($oMenu);
		
	$oMenu = new Menu();
	$oMenu->label = $lang['newsletter']['newslettergroup'];
	$oMenu->page = "wce.group.php";
	$oTopMenu->add($oMenu);
	
	$oMenu = new Menu();
	$oMenu->label = $lang['newsletter']['subscriber'];
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['addsubscriber'];
	$oSubMenu->page = "wce.subscriberadd.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['listsubscriber'];
	$oSubMenu->page = "wce.subscriber.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['importsubsc'];
	$oSubMenu->page = "wce.subscriberimp.php";
	$oMenu->addSub($oSubMenu);
	$oTopMenu->add($oMenu);	
		
	$oMenu = new Menu();
	$oMenu->label = $lang['newsletter']['sendschedule'];
	$oMenu->page = "wce.delivery.php";
	$oTopMenu->add($oMenu);	

	$oMenu = new Menu();
	$oMenu->label = $lang['newsletter']['preference'];
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['prefsetting'];
	$oSubMenu->page = "wce.preference.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newsletter']['interguide'];
	$oSubMenu->page = "wce.guide.php";
	$oMenu->addSub($oSubMenu);
	$oTopMenu->add($oMenu);	
	
	$oTopMenu->create();

?>