Newsletter 4.1
========================

You can choose to download only the Newsletter component or the complete Panel Admin Suite. 
Before installing, we recommend you to take a look at the System Requirements.

System Requirements
-------------------

Script Type 			: PHP Server Side Scripting 

Operating System 		: Linux or Windows Server 

Web Server 			: Apache or IIS 

PHP Version 			: PHP 4.2 or above 

PHP Configuration 		: ZLib library *
				: Safe_Mode = OFF
 				: Open_Basedir = EMPTY VALUE
				: GD library with FreeType Support (required for captcha image generation) 
						
Database 			: MySQL 3, 4

Web Browser 			: Internet Explorer 5 or above, Firefox, Netscape, Javascript and cookie enable 

Computer Hardware 		: PC and Mac (The HTML editor may not load properly in Safari on MAC)


* ZLib library is required to execute our encrypted codes. We recommend PHP 4.2 and above as the lower 
  versions will results in buffer handling error.

* AVOID SPAM: Our Newsletter component is not built for spamming purpose. Our support does not include spam 
  related issues, such as by-pass spam filter, sending to invalid email addresses, etc.


Installation Guides
-------------------

1. Download the Newsletter distribution ZIP file. 

2. Extract and upload the entire folder to your website under your domain name. 

	Example : http://your_domain_name.com/panel/ 
		
 
3. For Linux system, set the permission for the _files folder and config.php file to Read, Write, 
   Execute for all parties.

	Linux command example : chmod 777 _files , chmod 777 config.php
		
		
4. To access the Installation Wizard, visit 'http://your_domain_name.com/panel/index.php' with your web browser. Follow the instructions in 
   the Installation Wizard. 
   

5. Once the installation is completed, reset the file permission for 'config.php' file to 644.

6. You may proceed to login to the Administration area. 


Integration Guides
------------------

1. Using a text editor, open the web page file (.php file) that you wish to integrate the Newsletter to, insert the line below to the top of the file, to call the 'config.php' file.

	<?php include_once("document_path_of_panel_folder/config.php"); include_once($path["docroot"]."common/session.php"); include_once($path["docroot"]."common/css.php"); ?>

	* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.


2. Meanwhile, to display the newsletter's content area, put the line below.

	<?php include($path["docroot"]."newsletter/home.newsletter.php"); ?>


3. For sample integration code, please refer to the source code of this file. To do this, open the file with your text editor.

** To find your 'document_path_of_panel_folder', check the path['docroot'] value from the panel/config.php file


