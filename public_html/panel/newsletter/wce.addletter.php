<?

function defaultValue(){

	global $group_id, $dateletter, $title, $bodytext, $bodyhtml, $showletter, $sendmail, $fileurl, $attachmentname, $IsUploaded;
	
	$group_id 		= "";
	$dateletter 	= date("Y-m-d");
	$title 			= "";
	$bodytext 		= "";
	$bodyhtml 		= "";
	$showletter 	= "Yes";
	$sendmail		= "";
	$template_id	= "0";
	$fileurl 		= "";
	$attachmentname = "";
	$IsUploaded 	= "";
}

$maxsize = ini_get('upload_max_filesize');
$upload_max = (!empty($maxsize)?substr($maxsize,0,-1)."000000":"");

if($pageaction=="add"){
	//Validation
	$oNewsletter_Subscriber->validate($lang['newsletter']['newslsubject'],$title,"NotEmpty");
	
	if($status_message==""){
		$bodyhtml = $FCKeditor;
		$oNewsletter_Letter->data = array("group_id","dateletter","title","bodytext","bodyhtml","attachment","showletter","send_attachment");
		$oNewsletter_Letter->value = array($group_id,$dateletter,addslashes($title),addslashes($bodytext),addslashes($bodyhtml),addslashes($attachmentname),$showletter,$sendmail);
		$oNewsletter_Letter->add();	
		
		defaultValue();
		$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newsladded']." <br>";	
	}else{
		$bodyhtml = $FCKeditor;  	//to remain the bodyhtml value if got error
	}
	
	$pageaction = "";
	
}elseif($pageaction=="savetemp"){ 	//save template
	
	$bodyhtml = $FCKeditor;
	
	if (trim($tempfilename) == ""){		//if user does not specified template file name
		$tempfilename = "temp_".date("Ymd");
	}

	$oNewsletter_Template->data = array("letter_id");		//check whether template exist or not
	$oNewsletter_Template->where = "letter_name='".addslashes($tempfilename)."'";
	$result=$oNewsletter_Template->getList();
	if(mysql_num_rows($result)>0){
		$oNewsletter_Template->data = array("max(letter_id)");
		$oNewsletter_Template->where = "";
		$result=$oNewsletter_Template->getList();		
		$myrow=mysql_fetch_row($result);
		$tempfilename = $tempfilename."_".($myrow[0]+1);
	}
	$oNewsletter_Template->data = array("letter_name","bodytext","bodyhtml");
	$oNewsletter_Template->value = array(addslashes($tempfilename),addslashes($bodytext),addslashes($bodyhtml));
	$oNewsletter_Template->add();
		
	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newsltempadded']." <br>";	

	$pageaction = "";
	
}elseif($pageaction=="loadtemp"){ 	//load template
	
	if ($template_id == "0"){		//blank template
		$bodyhtml = "";
		$bodytext = "";
	}else{
		$oNewsletter_Template->data = array("bodytext","bodyhtml");
		$oNewsletter_Template->where = "letter_id='".addslashes($template_id)."'";
		$result=$oNewsletter_Template->getList();
		if($myrow=mysql_fetch_row($result)){
			$bodyhtml = $myrow[1];
			$bodytext = $myrow[0];
		}
	}
	
	$pageaction = "";
	
}elseif($pageaction=="uploadfile"){
	
	$bodyhtml = $FCKeditor;
	
  	if(($_FILES['attachment']['size']<$upload_max || empty($upload_max)) && !empty($_FILES['attachment']['size'])){

	  	$attachmentname = strtolower(str_replace(" ","_",trim($_FILES['attachment']['name'])));
	  	
	  	//ensure file not replace when have same file name
	  	$counter = "10000";
	  	$attachmentname = substr($counter,1)."_".$attachmentname;	  		  	
	  	while ($counter++){
		  	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		  		if (file_exists($path["docroot"]."_files/newsletter/".$attachmentname)) {
		  			$attachmentname = substr($counter,1)."_".substr($attachmentname,5);
				}else{
					break;	
				}	
			}		
		}
	  	
		if($attachmentname!=""){ 
			$attachmentname = stripslashes($attachmentname);
			if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
				$oNewsletter_Letter->uploadFile($_FILES['attachment']['tmp_name'],$path["docroot"]."_files/newsletter",$attachmentname); 
			}
			$IsUploaded = "true";
			$fileurl = $path["webroot"]."_files/newsletter/$attachmentname";
		}		
	}else{
		$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['warnexceed']."<br>";
	}
	
	$pageaction = "";
}elseif($pageaction=="removefile"){
	
	$bodyhtml = $FCKeditor;
	
	$attachmentname = stripslashes($attachmentname);
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if (file_exists($path["docroot"]."_files/newsletter/$attachmentname")){	
	  		@unlink($path["docroot"]."_files/newsletter/$attachmentname");
		}
	}
	$IsUploaded = "";
	$fileurl = "";
	$attachmentname = "";
	$pageaction = "";
}
else{ defaultValue(); }

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center><tr>
<td><b><u><? echo $lang['newsletter']['addnewsl'] ?></u></b></td><td align=right>
</td></tr></table><br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newsletter&page=wce.addletter.php" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="add">
<input type=hidden name=bodyhtml>
<input type=hidden name=fileurl value="<?= $fileurl ?>">
<input type=hidden name=attachmentname value="<?= $attachmentname ?>">
<input type=hidden name=IsUploaded value="<?= $IsUploaded ?>">
<tr><td valign=top width=25%><? echo $lang['newsletter']['loadtemplate'] ?></td><td><select name=template_id onchange="document.thisform.pageaction.value='loadtemp';document.thisform.submit()">
<option value="0" <? if($template_id == "0") echo selected ?>>Blank</option>
<?	$oNewsletter_Template->data = array("letter_id","letter_name");
	$oNewsletter_Template->where = "";
	$oNewsletter_Template->order = "letter_name";
	$result = $oNewsletter_Template->getList();
	while($myrow=mysql_fetch_row($result)){
		$myrow[1]=stripslashes($myrow[1]);
		if($myrow[0]==$template_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; }else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>"; }
	}
	mysql_free_result($result);
?>
</select></td></tr>
<tr><td valign=top width=25%><? echo $lang['newsletter']['choosegrp'] ?></td><td><select name=group_id>
<?	$oNewsletter_Group->data = array("group_id","name");
	$oNewsletter_Group->order = "name";
	$result = $oNewsletter_Group->getList();
	while($myrow=mysql_fetch_row($result)){
		$myrow[1]=stripslashes($myrow[1]);
		if($myrow[0]==$group_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; }else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>"; }
	}
	mysql_free_result($result);
?>
</select></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['publishdate'] ?></td><td><input type=text name=dateletter style="width:135px" value="<? echo $dateletter ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=checkbox name=showletter value="Yes" <? if ($showletter=="Yes") echo "checked"; ?>><? echo $lang['newsletter']['showarchive'] ?></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['newslsubject'] ?></td><td><input type=text name=title value="<? echo $title ?>" style="width:330px"> *</td></tr>
<tr><td colspan=2><? echo $lang['newsletter']['newslbody'] ?>: <br><br></td></tr>

<tr><td valign=top colspan=2><? echo $lang['newsletter']['htmlbased'] ?><br />
	<? 
	$oFCKeditor = new FCKeditor('FCKeditor') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/" ;
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '500' ;
	$oFCKeditor->Value		= stripslashes($bodyhtml);
	$oFCKeditor->Create() ;
	?>	
</td></tr>
<tr><td valign=top colspan=2><br><? echo $lang['newsletter']['plaintext'] ?><br><textarea name=bodytext cols=110 rows=23 style="width: 100%"><?= stripslashes(htmlentities($bodytext)) ?></textarea></td></tr>
<tr><td height="25px" valign=top><? echo $lang['newsletter']['uploadfile'] ?></td>
<? 
	echo (!empty($upload_max)?"<input type=hidden name=MAX_FILE_SIZE value=$upload_max>":"");
	if ($IsUploaded == "true"){
		echo "<td valign=top><input type=\"file\" name=\"attachment\" size=\"40\" onchange=\"document.thisform.pageaction.value='uploadfile'; document.thisform.submit();\"><br><a href=\"$fileurl\" target=\"_blank\">$attachmentname</a> &nbsp;&nbsp;
		<a href=\"javascript:onclick='document.thisform.pageaction.value='removefile'; document.thisform.submit();'\">".$lang['newsletter']['remove']."</a><br><input type=checkbox name=sendmail value=\"Yes\" ";
		if ($sendmail=="Yes") echo "checked"; 
		echo "> ".$lang['newsletter']['attachfile']."</td>"; 
	}else{
		echo "<td><input type=\"file\" name=\"attachment\" size=\"40\" onchange=\"document.thisform.pageaction.value='uploadfile'; document.thisform.submit();\">"; 
	}
?>
</td></tr>
<tr><td valign=top colspan=2><br>
<input type=submit name='submitbtn' value="  <? echo $lang['newsletter']['btnsave'] ?> ">&nbsp;&nbsp;&nbsp;
<input type=button name='togglebtn' value="<? echo $lang['newsletter']['btnsavetemplate'] ?>" onclick="toggleSaveTemplate()">
</td></tr>
<tr><td valign=top colspan=2>
<!-- save template -->
<div id="tempfield">
<br /><table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<tr><td>
<? echo $lang['newsletter']['newsltempname'] ?>: <input type=text name='tempfilename' style="width:330px">&nbsp;&nbsp;&nbsp;
<input type=button name='savetempbtn' value="  <? echo $lang['newsletter']['btnsave'] ?> " onclick="document.thisform.pageaction.value='savetemp';document.thisform.submit()">&nbsp;&nbsp;&nbsp;
<input type=button name='canceltempbtn' value="  <? echo $lang['common']['cancel'] ?> " onclick="hideSaveTemplate()">
</td></tr>
<tr><td><br /></td></tr>
</table>
</div>
</td></tr>
</form></table>
	
<script language=javascript>

document.getElementsByTagName('div')['tempfield'].style.display = 'none';

function toggleHTMLEditor(){
	bodytypeText.style.display='none';
	bodytypeHTML.style.display='block';
}

function toggleTextEditor(){
	bodytypeText.style.display='block';
	bodytypeHTML.style.display='none';	
}

function toggleSaveTemplate(){
	var x = document.getElementsByTagName('div')['tempfield'];
	
	x.style.display='block';
	document.getElementsByTagName('input')['tempfilename'].focus();
	document.getElementsByTagName('input')['togglebtn'].disabled = true;
	document.getElementsByTagName('input')['submitbtn'].disabled = true;
}

function hideSaveTemplate(){
	var x = document.getElementsByTagName('div')['tempfield'];
	
	x.style.display='none';
	document.getElementsByTagName('input')['togglebtn'].disabled = false;
	document.getElementsByTagName('input')['submitbtn'].disabled = false;
}

</script>
