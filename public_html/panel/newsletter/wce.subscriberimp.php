<?


/*****Imported Subscriber List*****/
if($pageaction=="import" && $_FILES['importfile']['name']!=""){
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	   	$oNewsletter_Subscriber->uploadFile($_FILES['importfile']['tmp_name'],$path["docroot"]."_files/newsletter",$_FILES['importfile']['name']); 
	
		$handle = fopen ($path["docroot"]."_files/newsletter/".$_FILES['importfile']['name'],"r");		
		
		//Read CSV Line To Get Data
		$count=0;
		while ($myrow = fgetcsv ($handle, 1000, ",")) {
		    for ($i=0;$i<count($myrow);$i++){ 
				$firstname = addslashes($myrow[0]);
				$lastname = addslashes($myrow[1]);
				$company = addslashes($myrow[2]);
				$position = addslashes($myrow[3]);
				$email = $myrow[4];
				$lettertype = $myrow[5];
		    }
	
			$oNewsletter_Subscriber->data = array("email");
			$oNewsletter_Subscriber->where = "email='$email'";
			$result1=$oNewsletter_Subscriber->getList();
			if(mysql_num_rows($result1)==0){
	
				//Add To Subscriber List
				$oNewsletter_Subscriber->data = array("firstname","lastname","company","position","email","lettertype","status");
				$oNewsletter_Subscriber->value = array($firstname,$lastname,$company,$position,$email,$lettertype,"Active");
				$oNewsletter_Subscriber->add();
				$lastID=$oNewsletter_Subscriber->getLastID();
	
				/*****Subscribe To Newsletter Groups*****/
				$lastID = $oNewsletter_Subscriber->getLastID();
				for($i=0;$i<count($group_id);$i++){
					$oNewsletter_Subscriber->data = array("group_id","subscriber_id");
					$oNewsletter_Subscriber->value = array($group_id[$i],$lastID);
					$oNewsletter_Subscriber->subscribe();
					$oNewsletter_Group->data = array("name");
					$result=$oNewsletter_Group->getDetail($group_id[$i]);
					if($myrow=mysql_fetch_row($result)){ $groupname=$myrow[0]; }
					mysql_free_result($result);
				}
				$count++;
			}
			mysql_free_result($result1);
		}
		fclose($handle);
		unlink($path["docroot"]."_files/newsletter/".$_FILES['importfile']['name']);
		
		$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$count." ".$lang['newsletter']['subscrimport']."<br>";
	}
}

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center><tr>
<td><b><u><? echo $lang['newsletter']['importsubsc'] ?></u></b></td><td align=right>
</td></tr></table><br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center><form name=frmImport action="index.php?component=newsletter&page=wce.subscriberimp.php" method=post enctype="multipart/form-data">
<input type=hidden name="pageaction" value="import">
<input type=hidden name=subscriber_id value="<? echo $subscriber_id ?>">
<input type=hidden name=sortby value="<? echo $sortby ?>">
<input type=hidden name=sortseq value="<? echo $sortseq ?>">
<input type=hidden name=keyword value="<? echo $keyword ?>">
<input type=hidden name=vgroup_id value="<? echo $vgroup_id ?>">
<input type=hidden name=start value="<? echo $start ?>">

<tr><td valign=top>1.</td><td><? echo $lang['newsletter']['message1'] ?><br><br></td></tr>
<tr><td valign=top>2.</td><td><? echo $lang['newsletter']['message2'] ?><br><br></td></tr>
<tr><td valign=top></td><td><? echo $lang['newsletter']['format'] ?> "First Name", "Last Name", "Company", "Position", "Email Address", "Newsletter Type"<br><br><? echo $lang['newsletter']['example'] ?> "John","Dow","Infosys Inc","Manager","john@infosys.com","HTML"<br><br></td></tr>
<tr><td valign=top>3.</td><td><? echo $lang['newsletter']['browsecsv'] ?></td></tr>
<tr><td valign=top></td><td><input type=file name=importfile><br><br></td></tr>
<tr><td valign=top>4.</td><td><? echo $lang['newsletter']['checkgrp'] ?></td></tr>
<tr><td valign=top></td><td>
	<table border=0 cellspacing=0 cellpadding=0>
	<?	
		$oNewsletter_Group->data = array("group_id","name");
		$oNewsletter_Group->order = "name";
		$result = $oNewsletter_Group->getList();
		while($myrow=mysql_fetch_row($result)){
			$myrow[1]=stripslashes($myrow[1]);
			echo "<tr><td><input type=checkbox name=group_id[] value=\"$myrow[0]\" checked></td><td>$myrow[1]</td></tr>";
		}
		mysql_free_result($result);
	?>
	</table>		
</td></tr>
<tr><td colspan=2><br><input type=button value="<? echo $lang['newsletter']['btnimport'] ?>" onclick="document.frmImport.submit()"></td></tr>
</form></table>
