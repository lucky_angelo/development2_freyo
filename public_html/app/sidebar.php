<div class="scrollable container-fluid f-sidenav">
	<div class="col-xs-12 f-close-menu" ui-turn-off='uiSidebarLeft'>
		<a class="btn_close_sidenav" href="#"><i class="fa fa-times"></i>&nbsp;<span>Close Menu</span></a>
	</div>
	<div class="col-xs-12 f-profile-details text-center">
		<img class="f-profile-pic img-circle" src="assets/mobile/images/gintoki_shock.jpg">
		<p class="f-profile-name">Gintoki Sakata</p>
		<p class="f-profile-email">{{ "gintoki_sakata@yorozuya.com" }}</p>
	</div>
	<div class="f-nav-items" ui-turn-off='uiSidebarLeft'>
		<div class="col-xs-12 f-nav-page">
			<a href="<?php echo asset('#/dashboard');?>">
				<img src="assets/mobile/images/dashboard-nav.png"> <span>Dashboard</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="<?php echo asset('#/profile/following');?>">
				<img src="assets/mobile/images/following-nav.png" style="top:15px;"> <span>Following list</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="<?php echo asset('#/profile/favorites');?>">
				<img src="assets/mobile/images/myfavorite-nav.png"> <span>My Favorites</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="<?php echo asset('#/contact/hi');?>">
				<img src="assets/mobile/images/browse-nav.png"> <span>Browse</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="#">
				<img src="assets/mobile/images/myprofile-nav.png"> <span>My Profile</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="#">
				<img src="assets/mobile/images/support-nav.png"> <span>Support</span>
			</a>
		</div>
		<div class="col-xs-12 f-nav-page">
			<a href="#">
				<img src="assets/mobile/images/signout-nav.png"> <span>Sign Out</span>
			</a>
		</div>
	</div>
</div>