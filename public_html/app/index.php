<!DOCTYPE html>
<html>
<head>
  <title>Freyo Angular</title>

  <link href='https://fonts.googleapis.com/css?family=Roboto:500,400,100,300,700,900' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="../assets/mobile/css/mobile-angular-ui-base.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/mobile/css/animate.css">
  <link rel="stylesheet" type="text/css" href="../assets/mobile/css/app.css">

  <script type="text/javascript" src="../assets/mobile/js/angular.min.js"></script>
  <script type="text/javascript" src="../assets/mobile/js/angular-route.min.js"></script>
  <script type="text/javascript" src="../assets/mobile/js/mobile-angular-ui.js"></script>
  <script type="text/javascript" src="../assets/mobile/js/mobile-angular-ui.gestures.js"></script>
  <script type="text/javascript" src="../assets/mobile/js/angApp2.js"></script>
  
</head>
<body ng-app="freyoApp" ng-controller="freyoController">
  
  <!-- Sidebars -->
  <div ng-include="sidebarUrl()" class="sidebar sidebar-left"></div>

  <div class="app" 
         ui-swipe-right='Ui.turnOn("uiSidebarLeft")'
         ui-swipe-left='Ui.turnOff("uiSidebarLeft")'>
    <div class="freyo-header navbar navbar-app navbar-absolute-top">
      <div class="navbar-brand navbar-brand-center" ui-yield-to="title">
        WELCOME
      </div>
      <div class="btn-group pull-left">
        <div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle">
          <i class="fa fa-bars"></i>
        </div>
      </div>
      <div class="btn-group pull-right" ui-yield-to="navbarAction">
        <div class="btn">
          <i class="fa fa-search"></i>
        </div>
      </div>
    </div>

    <!-- App body -->

    <div class='app-body'>
      <div class='app-content'>
        <ng-view></ng-view>
      </div>
    </div>
  </div><!-- ~ .app -->

  <!-- Modals and Overlays -->
  <div ui-yield-to="modals"></div>

</body>
</html>