<?php 
if(is_blog()){
	return require(THEME_DIR . "/template_blog.php");
} ?>
<?php
$bgType         = wt_get_option('background','background_type');
$slide_bg_1     = wt_get_option('background','slide_bg_1');
$slide_bg_2     = wt_get_option('background','slide_bg_2');
$slide_bg_3     = wt_get_option('background','slide_bg_3');
$slide_bg_4     = wt_get_option('background','slide_bg_4');
$slide_bg_5     = wt_get_option('background','slide_bg_5');
$video_bg       = wt_get_option('background','video_link');
$video_controls = wt_get_option('background','video_controls');
$video_bg_img   = wt_get_option('background','video_mobile_bg');
$overlayType    = wt_get_option('general','overlay_type');
?>
<!DOCTYPE html>
<?php 
if(wt_get_option('general','enable_responsive')){ 
	$responsive = 'responsive ';
} else {
	$responsive = '';
}
$niceScroll   = wt_get_option('general', 'nice_scroll');
$smoothScroll = wt_get_option('general', 'smooth_scroll');
if($smoothScroll) {
	$niceScroll = false; // disable nicescroll if smoothScroll is enabled
}
if($video_controls) {
	$video_controls_out = "true";
} else {	
	$video_controls_out = "false";
}
?>
<!--[if lt IE 7]> <html class="<?php echo esc_attr( $responsive ); ?>no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="<?php echo esc_attr( $responsive ); ?>no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="<?php echo esc_attr( $responsive ); ?>no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<html class="<?php echo esc_attr( $responsive ); ?><?php if($niceScroll):?>wt-nice-scrolling <?php endif; ?><?php if($smoothScroll):?>wt-smooth-scrolling <?php endif; ?>no-js" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<?php if (isset($_SERVER['HTTP_USER_AGENT']) &&
    (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
        header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php if(wt_get_option('general','enable_responsive')){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php } ?>
<?php 
if($favicon = wt_get_option('general','favicon')) { ?>
<link rel="shortcut icon" href="<?php echo wt_get_image_src($favicon); ?>" />
<?php } 
if($favicon_57 = wt_get_option('general','favicon_57')) { ?>
<link rel="apple-touch-icon" href="<?php echo wt_get_image_src($favicon_57); ?>" />
<?php } 
if($favicon_72 = wt_get_option('general','favicon_72')) { ?>
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo wt_get_image_src($favicon_72); ?>" />
<?php } 
if($favicon_114 = wt_get_option('general','favicon_114')) { ?>
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo wt_get_image_src($favicon_114); ?>" />
<?php } 
if($favicon_144 = wt_get_option('general','favicon_144')) { ?>
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo wt_get_image_src($favicon_144); ?>" />
<?php } ?>
<!--[if lt IE 10]>
    <style type="text/css">
    html:not(.is_smallScreen) .wt_animations .wt_animate { visibility:visible;}
    </style>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php
if (is_blog()){
global $blog_page_id;
global $post;
$blog_page_id = wt_get_option('blog','blog_page');
$post->ID = get_object_id($blog_page_id,'page');
}
/* Site Alignement  */
if (get_post_meta($post->ID, '_site_alignment', true)) {
	$alignment = get_post_meta($post->ID, '_site_alignment', true);	
}	
else {
	$alignment = wt_get_option('general', 'site_alignment');
}

/* Sidebar Alignement  */
require_once (THEME_FILES . '/layout.php');

$type          = get_post_meta($post->ID, '_intro_type', true);
$stickyHeader  = wt_get_option('general', 'sticky_header');
$noStickyOnSS  = wt_get_option('general', 'no_sticky_on_ss');
$retinaLogo    = wt_get_option('general', 'logo_retina');
$enable_retina = wt_get_option('general', 'enable_retina');
$animations    = wt_get_option('general','enable_animation');
$pageLoader    = wt_get_option('general','page_loader');
$responsiveNav = wt_get_option('general', 'responsive_nav');
$menu_type     = 'top';
$bg            = wt_check_input(get_post_meta($post->ID, '_page_bg', true));
$bg_position   = get_post_meta($post->ID, '_page_position_x', true);
$bg_repeat     = get_post_meta($post->ID, '_page_repeat', true);

$color = get_post_meta($post->ID, '_page_bg_color', true);
if(!empty($color) && $color != "transparent"){
	$color = 'background-color:'.$color.';';
}else{
	$color = '';
}
if(!empty($bg)){
	$bg = 'background-image:url('.$bg.');background-position:top '.$bg_position.';background-repeat:'.$bg_repeat.'';
}else{
	$bg = '';
}
if ($stickyHeader) {
	wp_enqueue_script('jquery-sticky');
}
if($smoothScroll) {
	wp_enqueue_script('smooth-scroll');
}
if ($niceScroll) {
	wp_enqueue_script('nice-scroll');
}
?>
<body <?php if($alignment=='right'):?>id="right_alignment"<?php endif;?><?php if($alignment=='left'):?>id="left_alignment" <?php endif;?><?php body_class(); ?>  <?php if(!empty($color) || !empty($bg)){echo' style="'.$color.''.$bg.'"';} ?> <?php if($niceScroll):?> data-nice-scrolling="1"<?php endif; ?>>
<?php if($pageLoader){ ?>
    <div id="wt_loader"><div class="wt_loader_html"></div></div>
<?php } ?>
<?php if($bgType == 'pattern') :?>

	<?php if($overlayType =='pattern') :?>
        <div class="wt_pattern_overlay"></div>
    <?php endif; ?>
    <?php if($overlayType =='color') :?>
        <div class="wt_color_overlay"></div>
    <?php endif; ?>
  
<section id="wt_section_home" class="<?php if($animations):?> wt_animations <?php endif;?><?php if($noStickyOnSS):?>wt_noSticky_on_ss_home<?php endif; ?>">
<?php require_once (THEME_FILES . '/homeContent.php'); ?> 
    <div id="wt_home_content">
        <div class="container">
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php //wt_theme_generator('wt_custom_title',$post->ID); ?>
            	<?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?> 
        </div>
    </div>
</section>
<?php elseif($bgType == 'image_bg') :?>

	<?php if($overlayType =='pattern') :?>
        <div class="wt_pattern_overlay"></div>
    <?php endif; ?>
    <?php if($overlayType =='color') :?>
        <div class="wt_color_overlay"></div>
    <?php endif; ?>
  
<section id="wt_section_home" class="<?php if($animations):?>wt_animations <?php endif; ?><?php if($noStickyOnSS):?>wt_noSticky_on_ss_home<?php endif; ?>">
<?php require_once (THEME_FILES . '/homeContent.php'); ?>
  <div id="wt_home_content">
    <div class="container">
	    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        	<?php the_content(); ?>
        <?php endwhile; else: ?>
        <?php endif; ?>  
    </div>
  </div>
</section>
<?php elseif($bgType == 'video') :?>
<?php
wp_enqueue_script('jquery-YTPlayer');
wp_enqueue_style('YTPlayer');
?>
<section id="wt_section_home" class="<?php if($animations):?>wt_animations <?php endif; ?><?php if($noStickyOnSS):?>wt_noSticky_on_ss_home<?php endif; ?>">
<?php require_once (THEME_FILES . '/homeContent.php'); ?> 
  <div class="wt_bg_video_mobile" style="background-image: url(<?php echo esc_url( $video_bg_img ); ?>);"></div>
  <div class="wt_bg_video">
	<a id="bgndVideo_home" class="wt_youtube_player_home" data-property="{videoURL:'<?php echo esc_url( $video_bg ); ?>', containment:'body', showControls:<?php echo esc_attr( $video_controls_out ); ?>, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, ratio:'4/3', addRaster:false, quality:'default'}"></a>
  </div>
  
  <?php if($overlayType =='pattern') :?>
      <div class="wt_pattern_overlay"></div>
  <?php endif; ?>
   <?php if($overlayType =='color') :?>
      <div class="wt_color_overlay"></div>
  <?php endif; ?>
  
  <div id="wt_home_content">
    <div class="container">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        	<?php the_content(); ?>
        <?php endwhile; else: ?>
        <?php endif; ?>  
    </div>
  </div>
</section>
<?php elseif($bgType == 'slideshow') :?>
<?php
wp_enqueue_script( 'jquery-supersized');
wp_enqueue_script( 'jquery-supersized-shutter');
?>
<section id="wt_section_home" class="<?php if($animations):?>wt_animations<?php endif; ?><?php if($noStickyOnSS):?> wt_noSticky_on_ss_home<?php endif; ?>">

  <?php if($overlayType =='pattern') :?>
      <div class="wt_pattern_overlay"></div>
  <?php endif; ?>
   <?php if($overlayType =='color') :?>
      <div class="wt_color_overlay"></div>
  <?php endif; ?>
  
<?php require_once (THEME_FILES . '/homeContent.php'); ?> 
  <div class="wt_fullscreen_slider" data-images='["<?php if(!empty($slide_bg_1)) { echo esc_url( $slide_bg_1 ) .'"'; } ?><?php if(!empty($slide_bg_2)) { echo ', "' . esc_url( $slide_bg_2 ) .'"'; } ?><?php if(!empty($slide_bg_3)) { echo ', "' . esc_url( $slide_bg_3 ) .'"';  }?><?php if(!empty($slide_bg_4)) { echo ', "' . esc_url( $slide_bg_4 ) .'"'; } ?><?php if(!empty($slide_bg_5)) { echo ', "' . esc_url( $slide_bg_5 ) .'"'; } ?>]' data-autoplay="true" data-slideinterval="7000" data-transitionspeed="1500" data-transition="1">
    <div id="progress-back" class="load-item">
      <div id="progress-bar"></div>
    </div>
  </div>  
  
  <div id="wt_home_content">
    <div class="container">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        	<?php the_content(); ?>
        <?php endwhile; else: ?>
        <?php endif; ?> 
    </div>
  </div>
</section>
<?php endif; ?>
<script type="text/javascript">
/* <![CDATA[ */
var theme_uri="<?php echo THEME_URI;?>";
/* ]]> */
</script>
<?php
wt_scripts();
if(wt_get_option('general','analytics')){
	echo stripslashes(wt_get_option('general','analytics'));
}
?>
<?php
wp_footer();
?>
</body>
</html>