<?php get_header();
global $blog_page_id, $r;
$layout= wt_get_option('blog','layout');
?>
</div> <!-- End headerWrapper -->
<div id="wt_containerWrapper" class="clearfix">
    <?php wt_theme_generator('wt_breadcrumbs',$post->ID); ?>
	<?php //wt_theme_generator('wt_custom_header',$post->ID); ?>
    <?php wt_theme_generator('wt_containerWrapp',$post->ID);?>
        <div id="wt_container" class="clearfix">
            <?php wt_theme_generator('wt_content',$post->ID);?>
                <div class="container">
                    <div class="row">
                    <div class="wt_spacer_sc wt_clearboth" style="height:20px; line-height:20px;"></div>
					<?php if($layout == 'left') {
                        echo '<aside id="wt_sidebar" class="col-sm-3 col-md-3 col-lg-3 col-xs-12">';
                        get_sidebar(); 
                        echo '</aside> <!-- End wt_sidebar -->'; 
                    }?>
                    <?php if($layout != 'full') {
                        echo '<div id="wt_main" role="main" class="col-sm-9 col-md-9 col-lg-9 col-xs-12">'; 
                        echo '<div id="wt_mainInner">';
                    }?>
                        <?php 
                            get_template_part( 'loop','blog');
                        ?>
                    <?php if($layout != 'full') {
                        echo '</div> <!-- End wt_mainInner -->'; 
                        echo '</div> <!-- End wt_main -->'; 
                    }?>
                    <?php if($layout == 'right') {
                        echo '<aside id="wt_sidebar" class="col-sm-3 col-md-3 col-lg-3 col-xs-12">';
                        get_sidebar(); 
                        echo '</aside> <!-- End wt_sidebar -->'; 
                    }?>
                    </div> <!-- End row -->
                </div> <!-- End container -->
            </div> <!-- End wt_content -->
        </div> <!-- End wt_container -->
    </div> <!-- End wt_containerWrapp -->
</div> <!-- End wt_containerWrapper -->
<?php get_footer(); ?>