<div class="crf-main-form">
    <div class="crf-form-name-heading-Submissions">
      <h1>Search Submission</h1>
    </div>
    <div class="crf-form-setting">
  
     <form name="filter" id="filter" method="get" action="admin.php?page=crf_entries">
      <div class="crf-left-search-form">
     <div class="crf-form-setting-forms">
     <label><span class="dashicons dashicons-calendar-alt"></span>From Date</label>
	<input type="text" name="start_date" id="start_date" value="<?php if(isset($_GET['start_date'])) echo $_GET['start_date']; ?>" class="crf_date" />
    </div>
    <div class="crf-form-setting-forms">
    <label><span class="dashicons dashicons-calendar-alt"></span>To Date</label>
    <input type="text" name="end_date" id="end_date" value="<?php if(isset($_GET['end_date'])) echo $_GET['end_date']; ?>" class="crf_date" />
    </div>
    </div>
    <div class="crf-left-search-form">
    <div id="group_field">
    <div class="wrapper">
    <div class="crf-form-setting-forms">
    <label>Select Field</label>
    <select name="field_name" id="field_name">
     <option value="">Select Field</option>
    <?php
	
	 $qry1 = "select * from $crf_fields where Form_Id= '".$form_id."' and Type not in('heading','paragraph','DatePicker','file') order by ordering asc";
	  $reg1 = $wpdb->get_results($qry1);
	  if(!empty($reg1))
	  {
	   foreach($reg1 as $row1)
	   {
		  if(!empty($row1))
		  {
			  $Customfield = crf_get_field_key($row1)
			  ?>
              <option value="<?php echo $Customfield;?>" <?php if(isset($_GET['field_name']) && $Customfield==$_GET['field_name']) echo 'selected';?>><?php echo $row1->Name;?></option>
              <?php
		  }
	   }
	  }
	?>
   
    </select>
    </div>
    
    <div class="crf-form-setting-forms">
    
    <label>Enter Value</label>
    <input type="text" name="field_value" id="field_value" value="<?php if(isset($_GET['field_value'])) echo $_GET['field_value'];?>"/>
    </div>
    
    </div>
    </div>
    </div>
    <div class="crf-form-search-button">
    <input type="hidden" name="form_id" value="<?php echo $form_id;?>" />
    <input type="hidden" name="page" value="crf_entries" />
    <input type="reset" name="reset" onClick="clearForm('#filter')" />
    
    <input type="hidden" name="pagenum" value="1" />
    <input type="submit" name="crf_filter_result" />
    </div>
</form>
      
      
    </div>
  </div>