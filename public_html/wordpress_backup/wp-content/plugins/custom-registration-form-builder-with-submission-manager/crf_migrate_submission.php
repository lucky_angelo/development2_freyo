<?php
$path =  plugin_dir_url(__FILE__);
?>
<div class="crf_message">
  <div class="crf-form-heading">
  <div class="crf_message2">
  <h2 class="crf_message_heading">This will upgrade your old version content to new version</h2>
  <p class="crf_message_paragraph">It may take few minutes to complete. Please do not interrupt the
process once started.</p>
	</div>

  <div class="progressbar"></div>
  <div class="crf-form-button">
  <input type="button" name="migration" id="migration" onClick="start_migration()" value="Migrate" />
  </div>
  </div>
</div>
<script>
function start_migration()
{
	jQuery('.progressbar').html('<img src="<?php echo $path;?>images/ajax-loader.gif" />');
	jQuery('.crf_message2 h2.crf_message_heading').html('Upgrading in progress. Please wait.');
	jQuery('.crf_message2 p.crf_message_paragraph').html('Do not close or refresh the window.');
	jQuery('.crf_message .crf-form-button #migration').hide();
	
	jQuery.ajax({
                type: "POST",
                url: '<?php echo get_option('siteurl').'/wp-admin/admin-ajax.php';?>?action=crf_start_migrate_submission&cookie=encodeURIComponent(document.cookie)',
                success: function (serverResponse) {
                    if (serverResponse.trim() == "submission migrated successful") {
						jQuery('.crf_message2 h2.crf_message_heading').html('<div class="dashicons dashicons-yes"></div>');
						jQuery('.crf_message2 p.crf_message_paragraph').addClass('crf_black');
                        jQuery('.crf_message2 p.crf_message_paragraph').html('All Done! Thank you.');
						jQuery('.progressbar').html('');
						jQuery('.crf_message .crf-form-button').html('<a href="admin.php?page=crf_manage_forms">Continue</a>');
                    } else {
						jQuery('.crf_message2 p.crf_message_paragraph').addClass('crf_black');
						jQuery('.crf_message2 h2.crf_message_heading').html('<div class="dashicons dashicons-no-alt"></div>');
                        jQuery('.crf_message2 p.crf_message_paragraph').html('Sorry, the migration failed! Please try again.');
						jQuery('.progressbar').html('');
						jQuery('.crf_message .crf-form-button').html('<a href="admin.php?page=crf_migrate_submission">Try Again</a>');
						
                  }
                }
            })
}
</script>