<?php
/*Controls custom field creation in the dashboard area*/
global $wpdb;
$textdomain = 'custom-registration-form-pro-with-submission-manager';
$crf_forms =$wpdb->prefix."crf_forms";
$path =  plugin_dir_url(__FILE__); 
?>

<div id="support-feature">
<div class="support-top">
    <div class="hedding-boder">
    <h1 class="support-hedding-icon">Support, Feature Requests and Feedback</h1>
    </div>
</div>
<div class="support-available">
  <h3>Support is available through our forums. Here are the relevant links:</h3>
  <div class="link">
  <ul>
  <li><a href="http://cmshelplive.com/accounts/index.php?m=forum&action=viewboard&id=3" target="_blank">GENERAL QUESTIONS</a></li>
<li><a href="http://cmshelplive.com/accounts/index.php?m=forum&action=viewboard&id=6" target="_blank">INSTALLATION ISSUES</a></li>
<li><a href="http://cmshelplive.com/accounts/index.php?m=forum&action=viewboard&id=4" target="_blank">FEATURE REQUESTS</a></li>
<li><a href="http://cmshelplive.com/accounts/index.php?m=forum&action=viewboard&id=5" target="_blank">BUG REPORTS</a></li>
  </ul>
  </div>
  

</div>

</div>