<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'freyo_wpsite');

/** MySQL database username */
define('DB_USER', 'freyo_wpadmin');

/** MySQL database password */
define('DB_PASSWORD', 'fREyO1q2w');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E%`04&;uWV].Dad~~Y_|cmOTkvMr5D&$<PF0jeu+2VF5>/1isHx=m|8f2{GM6mKW');
define('SECURE_AUTH_KEY',  '5S5:_$1U>BdxNJ^p=+7<`FzWxC}+%uptB*}+JtVXf`O*Onji CF^Rbgkc@`$WGz<');
define('LOGGED_IN_KEY',    '_Yd)/w3-X>^=HA^-8=c P%uVVCRow6T$pq~Fi*<qx~/cq-jUo~YP^}8Ut(7R)Lx&');
define('NONCE_KEY',        'pZW$yR_Znl40VYl`.jlBHgU?d?9TcQgTm,30hdgfSDiLQND.j|+44v=hq$|XDW~y');
define('AUTH_SALT',        '%fVkRc`jl0ZPp2$6yHY6 Qel2W1BW9T-:|%s+{6wM >Z)0D8]AmFwv#-!(pA35{(');
define('SECURE_AUTH_SALT', '~^ )o87X&dRSc0_h87X4D&D&f|~|q,wP}U|7u9CY5R->7^/71HkvC~zAo|-BIh{!');
define('LOGGED_IN_SALT',   'w&?!Ts!-|PUg{fr;jv&KLs}_G~8m|+]MbQME}}(?8|jm~[T85G80E/whN!h;Jov~');
define('NONCE_SALT',       '{Qku!`b-8%P)78sv?m2;u&imHD+`|vR7BDBY:MUD$Sgu(@Ys) !rj1s2@FGm6hyM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fy_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
